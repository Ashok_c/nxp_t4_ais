################################################################################
# Automatically-generated file. Do not edit!
################################################################################

OBJ_SRCS := 
S_SRCS := 
ASM_SRCS := 
C_SRCS := 
S_UPPER_SRCS := 
O_SRCS := 
EXECUTABLES := 
OBJS := 
C_DEPS := 

# Every subdirectory with source files must be described here
SUBDIRS := \
amazon-freertos/FreeRTOS \
amazon-freertos/FreeRTOS/portable \
amazon-freertos/bufferpool \
amazon-freertos/crypto \
amazon-freertos/logging \
amazon-freertos/mqtt \
amazon-freertos/secure_sockets \
amazon-freertos/tls \
amazon-freertos/utils \
audio/amazon \
audio/amplifier \
audio/audio_processing \
audio/audio_samples \
audio/pdm_mic \
aws_ais/src \
board \
cjson/src \
codec \
common \
component/lists \
component/serial_manager \
component/serial_manager/usb_cdc_adapter \
component/uart \
device \
drivers \
lwip/port \
lwip/src/api \
lwip/src/core \
lwip/src/core/ipv4 \
lwip/src/core/ipv6 \
lwip/src/netif \
lwip/src/netif/ppp \
mbedtls/library \
mbedtls/port/ksdk \
osa \
sdmmc/port \
sdmmc/src \
source \
startup \
usb/device/source/ehci \
usb/device/source \
usb/phy \
utilities \
wiced_bt/imxrt_port \
wifi_wwd/FreeRTOS \
wifi_wwd/WWD/internal/bus_protocols/SDIO \
wifi_wwd/WWD/internal/bus_protocols \
wifi_wwd/WWD/internal/chips \
wifi_wwd/WWD/internal \
wifi_wwd/network \
wifi_wwd/platform \
wifi_wwd \
xip \

