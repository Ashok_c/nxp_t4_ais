


#ifndef AWS_USER_CREDENTIALS_H_
#define AWS_USER_CREDENTIALS_H_
//#include "flexspi_hyper_flash.h"
#include "fsl_common.h"
#include "sln_flash.h"
#include "stdint.h"

#define MQTT_BROKER_ENDPOINT_LEN 48
#define ALEXA_TOPIC_ROOT_LEN 16
#define ALEXA_ACCOUNT_ID_LEN 16
#define IOT_PRODUCT_NAME_LEN 16
#define ALEXA_USER_AWS_DETAILS   "aws_user_cred.dat"
#define ALEXA_USER_AWS_DETAILS_ADDR  (0x1D80000) // todo Needed to specify Actaul address of flash memory to store user aws data
#define TEST_PURPOSE 1

typedef struct
{
    uint8_t length;                  /**< aws endpoint  length */
    char data[MQTT_BROKER_ENDPOINT_LEN]; /**< actual aws data */
} mqtt_brk_endpoint_t;

typedef struct
{
    uint8_t length;                  /**< aws topic length */
    char data[ALEXA_TOPIC_ROOT_LEN]; /**< actual aws topic root value */
} alexa_topic_root_t;

typedef struct
{
    uint8_t length;                  /**< aws account id length */
    char value[ALEXA_ACCOUNT_ID_LEN]; /**< actual aws account value */
} alexa_account_id_t;

typedef struct
{
    uint8_t length;                  /**< aws iot_product_name length */
    char data[IOT_PRODUCT_NAME_LEN]; /**< actual aws iot_product_name data */
} iot_product_name_t;

typedef struct aws_user_aws_details
{
	mqtt_brk_endpoint_t mqtt_broker_endpoint;
	alexa_topic_root_t alexa_topic_root;
	alexa_account_id_t alexa_account_id;
	iot_product_name_t iot_product_name;
} aws_user_aws_details_t;

status_t aws_user_credentials_flash_get(aws_user_aws_details_t *cred);
status_t aws_user_credentials_flash_set(aws_user_aws_details_t *cred);
void aws_user_store_credentials_into_flash(void);

#endif /* AWS_USER_CREDENTIALS_H_ */
