/*
 * Copyright 2018 NXP.
 * This software is owned or controlled by NXP and may only be used strictly in accordance with the
 * license terms that accompany it. By expressly accepting such terms or by downloading, installing,
 * activating and/or otherwise using the software, you are agreeing that you have read, and that you
 * agree to comply with and are bound by, such license terms. If you do not agree to be bound by the
 * applicable license terms, then you may not retain, install, activate or otherwise use the software.
 */

#include "sln_flash.h"

#include "flexspi_hyper_flash.h"
#include "fsl_flexspi.h"

#ifdef __REDLIB__
size_t safe_strlen(const char *ptr, size_t max)
{
    char *p = (char *)ptr;

    while (max && *p)
    {
        p++;
        max--;
    }

    return (size_t)(p - ptr);
}
#endif

__attribute__((section(".data.$SRAM_DTC"))) static void sln_flash_memset(void *dst, uint8_t data, size_t len)
{
    uint8_t *ptr = (uint8_t *)dst;

    while (len)
    {
        *ptr++ = data;
        len--;
    }
}

__attribute__((section(".data.$SRAM_DTC"))) static void sln_flash_memcpy(void *dst, void *src, size_t len)
{
    uint8_t *ptrDst = (uint8_t *)dst;
    uint8_t *ptrSrc = (uint8_t *)src;

    while (len)
    {
        *ptrDst++ = *ptrSrc++;
        len--;
    }
}

__attribute__((section(".data.$SRAM_DTC"))) void SLN_Flash_Init(void)
{
    uint32_t irqState;

    irqState = DisableGlobalIRQ();

    SCB_DisableDCache();

    /* Update LUT table. */
    FLEXSPI_UpdateLUT(FLEXSPI, 0, customLUT, CUSTOM_LUT_LENGTH);

    /* Do software reset. */
    FLEXSPI_SoftwareReset(FLEXSPI);

    SCB_EnableDCache();

    EnableGlobalIRQ(irqState);
    /* Flush pipeline to allow pending interrupts take place
     * before starting next loop */
    __ISB();
}

__attribute__((section(".data.$SRAM_DTC"))) status_t SLN_Write_Flash_Page(uint32_t address, uint8_t *data, uint32_t len)
{
    status_t status = 0;
    uint32_t irqState;

    irqState = DisableGlobalIRQ();

    SCB_DisableDCache();

    /* Setup page size write buffer */
    uint8_t tempPage[FLASH_PAGE_SIZE];

    sln_flash_memset(tempPage, 0xFF, FLASH_PAGE_SIZE);

    sln_flash_memcpy(tempPage, data, len);

    /* Program page. */
    status = flexspi_nor_flash_page_program(FLEXSPI, address, (void *)tempPage);

    SCB_EnableDCache();

    EnableGlobalIRQ(irqState);
    /* Flush pipeline to allow pending interrupts take place
     * before starting next loop */
    __ISB();

    return status;
}

__attribute__((section(".data.$SRAM_DTC"))) status_t SLN_Erase_Sector(uint32_t address)
{
    status_t status = 0;
    uint32_t irqState;

    irqState = DisableGlobalIRQ();

    SCB_DisableDCache();

    /* Erase sectors. */
    status = flexspi_nor_flash_erase_sector(FLEXSPI, address);

    /* Do software reset. */
    FLEXSPI_SoftwareReset(FLEXSPI);

    SCB_EnableDCache();

    EnableGlobalIRQ(irqState);
    /* Flush pipeline to allow pending interrupts take place
     * before starting next loop */
    __ISB();

    return status;
}

/* NOTE: SLN_Erase_Sector must be called prior to writing pages in a sector
 *       Afterwards, multiple pages can be written in that sector */
__attribute__((section(".data.$SRAM_DTC"))) status_t SLN_Write_Flash_At_Address(uint32_t address, uint8_t *data)
{
    status_t status = 0;
    uint32_t irqState;

    irqState = DisableGlobalIRQ();

    SCB_DisableDCache();

    /* Do software reset. */
    FLEXSPI_SoftwareReset(FLEXSPI);

    /*Program page. */
    status = flexspi_nor_flash_page_program(FLEXSPI, address, (void *)data);

    SCB_EnableDCache();

    EnableGlobalIRQ(irqState);
    /* Flush pipeline to allow pending interrupts take place
     * before starting next loop */
    __ISB();

    return status;
}

__attribute__((section(".data.$SRAM_DTC")))
status_t SLN_Read_Flash_At_Address(uint32_t address, uint8_t *data, uint32_t size)
{
    memcpy(data, (void *)(FlexSPI_AMBA_BASE + address), size);

    return kStatus_Success;
}

__attribute__((section(".data.$SRAM_DTC"))) uint32_t SLN_Flash_Get_Read_Address(uint32_t address)
{
    return FlexSPI_AMBA_BASE + address;
}
