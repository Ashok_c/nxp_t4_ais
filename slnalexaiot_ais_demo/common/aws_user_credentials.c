


#include "aws_user_credentials.h"

#include "sln_flash_mgmt.h"
#include "FreeRTOSConfig.h"

status_t aws_user_credentials_flash_get(aws_user_aws_details_t *cred)
{

    uint32_t credLen = sizeof(aws_user_aws_details_t);
    SLN_FLASH_MGMT_Read(ALEXA_USER_AWS_DETAILS, (uint8_t *)cred, &credLen);

    return 0;
}

status_t aws_user_credentials_flash_set(aws_user_aws_details_t *cred)
{
    int32_t status = 0;

    status = SLN_FLASH_MGMT_Save(ALEXA_USER_AWS_DETAILS, (uint8_t *)cred, sizeof(aws_user_aws_details_t));

    if (status == SLN_FLASH_MGMT_EOVERFLOW || status == SLN_FLASH_MGMT_EOVERFLOW2)
    {
        status = SLN_FLASH_MGMT_Erase(ALEXA_USER_AWS_DETAILS);
        status = SLN_FLASH_MGMT_Save(ALEXA_USER_AWS_DETAILS, (uint8_t *)cred, sizeof(aws_user_aws_details_t));
    }

    return status;
}


#if TEST_PURPOSE
//aws_user_store_credentials_into_flash function is for testing purpose only.
 void aws_user_store_credentials_into_flash(void)
{
	 int32_t status = 0;

	 aws_user_aws_details_t cred;

	 cred.iot_product_name.length = 16;
	 cred.alexa_account_id.length = 16;
	 cred.mqtt_broker_endpoint.length = 48;
	 cred.alexa_topic_root.length = 16;
	 memcpy(cred.mqtt_broker_endpoint.data, "a39a4sbt5fpjbh.iot.us-west-2.amazonaws.com",  cred.mqtt_broker_endpoint.length);
	 memcpy(cred.alexa_account_id.value, "938289322244",  cred.alexa_account_id.length);
	 memcpy(cred.alexa_topic_root.data , "$aws/alexa",  cred.alexa_topic_root.length);
	 memcpy(cred.iot_product_name.data, "AIS_t4",  cred.iot_product_name.length );

	 configPRINTF(("........................Flashing data into flash meomery***********>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n"));
		configPRINTF(("MQTT_BROKER_ENDPOINT %s \r\n",cred.mqtt_broker_endpoint.data));
		configPRINTF(("ALEXA_TOPIC_ROOT %s \n",cred.alexa_topic_root.data));
		configPRINTF(("ALEXA_ACCOUNT_ID %s \n",cred.alexa_account_id.value));
		configPRINTF(("IOT_PRODUCT_NAME %s \n",cred.iot_product_name.data));
     configPRINTF(("........................***********>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n"));
	 status = aws_user_credentials_flash_set(&cred);

	 if(!status)
	 {
		 configPRINTF(("AWS: credentials saved in flash\r\n"));
	 }
	 else{
		 configPRINTF(("AWS: credentials failed saved in flash\r\n"));
		 }
}
#endif
