/*
 * Copyright 2018 NXP.
 * This software is owned or controlled by NXP and may only be used strictly in accordance with the
 * license terms that accompany it. By expressly accepting such terms or by downloading, installing,
 * activating and/or otherwise using the software, you are agreeing that you have read, and that you
 * agree to comply with and are bound by, such license terms. If you do not agree to be bound by the
 * applicable license terms, then you may not retain, install, activate or otherwise use the software.
 */

#include <stdint.h>

/* FreeRTOS kernel includes */
#include "FreeRTOS.h"
#include "task.h"

typedef enum __reconnect_state
{
    kStartState = (1U << 0U),
    kStreamerStop = (1U << 1U),
    kLinkLoss = (1U << 2U),
    kMqttReconnect = (1U << 3U),
    kAisDisconnect = (1U << 4U),
    kAisReconnect  = (1U << 5U),
    kOtaDisconnect = (1U << 6U),
    kOtaReconnect  = (1U << 7U),
    kLinkUp        = (1U << 8U),
    kSysReboot     = (1U << 9U)
} reconnectState_t;

typedef enum __reconnect_event
{
    kReconnectInvalidSequence = (1U << 0U),
    kReconnectAPIDeprecated = (1U << 1U),
    kReconnectEncryptionError = (1U << 2U),
    kReconnectGoingOffline = (1U << 3U),
    kReconnectNetworkLoss = (1U << 4U)
} reconnectEvent_t;

/*!
 * @brief Sets the reconnection task event to trigger reconnect state machine
 *
 * @param event reconnectEvent_t The event that needs to be set for reconnect
 *
 * @returns status of event set [not yet implemented]
 */
int32_t reconnection_task_set_event(reconnectEvent_t event);

/*!
 * @brief Get reconnection task current state
 *
 * @returns The reconnectState_t that the state machine is in
 */
reconnectState_t reconnection_task_get_state(void);

/*!
 * @brief Initializes the reconnection task and supporting timer tasks
 *
 * @param *handle Pointer to TashHandle_t that will be used by application
 */
int32_t reconnection_task_init(TaskHandle_t *handle);
