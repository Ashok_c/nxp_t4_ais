/*
 * Copyright 2018 NXP.
 * This software is owned or controlled by NXP and may only be used strictly in accordance with the
 * license terms that accompany it. By expressly accepting such terms or by downloading, installing,
 * activating and/or otherwise using the software, you are agreeing that you have read, and that you
 * agree to comply with and are bound by, such license terms. If you do not agree to be bound by the
 * applicable license terms, then you may not retain, install, activate or otherwise use the software.
 */

#ifndef _AISV2_APP_H_
#define _AISV2_APP_H_

#include "aisv2.h"

#define AIS_APP_TIMER_INTERVAL_MSEC                (30000U)   /* Update internal timer every 30 seconds */
#define AIS_APP_TIME_SYNC_INTERVAL_SEC             (30U)      /* Re-sync time every 30 sec */
#define AIS_APP_TIMER_SYNC_CLOCK_TIMEOUT_MSEC      (10000U)   /* Timeout for receiving sync clock directive is 10 seconds */
#define AIS_APP_MQTT_DISCONNECT_TIMEOUT_MSEC       (30000U)   /* Timeout for MQTT Disconnect is 30 seconds */
#define AIS_APP_MQTT_RECONNECT_TIMEOUT_MSEC        (90000U)   /* Timeout for MQTT Reconnect is 90 seconds */
#define AIS_APP_MQTT_AIS_CONNECT_TIMEOUT_MSEC      (60000U)   /* Timeout for AIS Connection events is 60 seconds */
#define AIS_APP_MQTT_OTA_DISCONNECT_TIMEOUT_MSEC   (30000U)   /* Timeout for OTA MQTT Disconnection is 30 seconds */
#define AIS_APP_MQTT_OTA_CONNECT_TIMEOUT_MSEC      (60000U)   /* Timeout for OTA MQTT Connection is 60 seconds */
#define AIS_APP_TIMER_INTERVAL_SEC                 (30U)
#define AIS_APP_THINK_STATE_TIMEOUT_MSEC           (10000U)

typedef struct
{
    ais_state_t state;
    ais_state_t prevState;

    int64_t speakerOffsetStart;
    int64_t speakerOffsetEnd;
    int64_t speakerOffsetWritten;
    echoMarker_t speakerMarker;
    bool speakerEOS;
    bool speakerOpen;

    ais_buffer_state_t prevSpeakerBufferState; /* used for better transition handling */
    ais_buffer_state_t speakerBufferState;
    uint32_t overrunSequence; /* holds record of last overrun sequence until received again */

    uint64_t prevTime;
    uint64_t currTime;
    uint64_t lastRefresh;

    bool syncClockReceived;

    /* Buffer to hold the microphone data to send to AIS. */
    uint32_t micIndex;
    TickType_t expectedTickTime;
    uint32_t volume;
} ais_app_data_t;

#endif
