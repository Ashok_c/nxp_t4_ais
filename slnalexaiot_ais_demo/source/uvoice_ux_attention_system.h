/*
 * Copyright 2018 NXP.
 * This software is owned or controlled by NXP and may only be used strictly in accordance with the
 * license terms that accompany it. By expressly accepting such terms or by downloading, installing,
 * activating and/or otherwise using the software, you are agreeing that you have read, and that you
 * agree to comply with and are bound by, such license terms. If you do not agree to be bound by the
 * applicable license terms, then you may not retain, install, activate or otherwise use the software.
 */

#include "FreeRTOS.h"
#include "task.h"
#include "aisv2_app.h"

typedef enum _ux_attention_states
{
    uxNull,
    uxIdle,
    uxListeningStart,
    uxListeningActive,
    uxListeningEnd,
    uxThinking,
    uxSpeaking,
    uxSpeakingEnd,
    uxMicOntoOff,
    uxMicOfftoOn,
    uxTimer,
    uxTimerShort,
    uxTimerEnd,
    uxAlarm,
    uxAlarmShort,
    uxAlarmEnd,
    uxReminder,
    uxReminderShort,
    uxReminderEnd,
    uxNotificationIncoming,
    uxNotificationQueued,
    uxNotificationCleared,
    uxReconnecting,
    uxConnected,
    uxBootUp,
    uxApMode,
    uxWiFiSetup,
    uxAccessPointFound,
    uxNoAccessPoint,
    uxInvalidWiFiCred,
    uxDeviceChange,
    uxDiscovery,
    uxAuthentication,
    uxDisconnected,
	uxBleProvision,
    uxError

} ux_attention_states_t;

#define UX_INTERACTION_MASK(x) \
    ((uxListeningStart == x) || (uxListeningActive == x) || (uxThinking == x) || (uxSpeaking == x))

void ux_attention_task_Init(TaskHandle_t *handle);
int32_t ux_attention_set_state(ux_attention_states_t uxState);
void ux_attention_resume_state(ais_state_t state);
ux_attention_states_t ux_attention_get_state(void);
void ux_attention_sys_fault(void);
