/*
 * Copyright 2018 NXP.
 * This software is owned or controlled by NXP and may only be used strictly in accordance with the
 * license terms that accompany it. By expressly accepting such terms or by downloading, installing,
 * activating and/or otherwise using the software, you are agreeing that you have read, and that you
 * agree to comply with and are bound by, such license terms. If you do not agree to be bound by the
 * applicable license terms, then you may not retain, install, activate or otherwise use the software.
 */

#include <string.h>
#include <time.h>

#include "ais_alerts.h"
#include "ais_streamer.h"
#include "aisv2_app.h"
#include "sln_flash.h"
#include "sln_flash_mgmt.h"
#include "streamer_pcm.h"

/*! @brief Values for flow control buffer states. */
const char *ais_buffer_state_str[] = {
    "AIS_BUFFER_STATE_GOOD,",     "AIS_BUFFER_STATE_OVERRUN,",          "AIS_BUFFER_STATE_OVERRUN_WARNING,",
    "AIS_BUFFER_STATE_UNDERRUN,", "AIS_BUFFER_STATE_UNDERRUN_WARNING,", "AIS_BUFFER_STATE_INVALID"};

const char *ais_state_str[] = {"AIS_STATE_IDLE", "AIS_STATE_THINKING", "AIS_STATE_SPEAKING", "AIS_STATE_ALERTING",
                               "AIS_STATE_INVALID"};

const uint64_t kEpochAdjust = 2208988800ULL;

extern ais_app_data_t appData;

#include "audio_processing_task.h"
#include "limits.h"
#include "reconnection_task.h"
#include "task.h"
#include "uvoice_ux_attention_system.h"

extern TaskHandle_t appTaskHandle;
extern TaskHandle_t xUXAttentionTaskHandle;

static uint8_t *s_recordData = NULL;
static uint32_t s_recordDataSize = 0U;

static uint8_t *pu8PreambleData;
static uint32_t u32PreambleIdx = 0;
static uint32_t u32PreambleSize = 0;

static TaskHandle_t s_alertsTask;
extern QueueHandle_t g_alertQueue;

TimerHandle_t s_thinkStateTimer = NULL;

extern uint32_t u32MallocCount;

/* Buffer to hold the microphone data to send to AIS. */
__attribute__((section(".ocram_non_cacheable_data"))) uint8_t micData[AIS_MIC_PACKET_SIZE];

static void vThinkStateTimerCallback(TimerHandle_t xTimer);

void AIS_AppInit(void)
{
    appData.speakerOffsetStart = -1;
    appData.speakerOffsetEnd = -1;
    appData.speakerOffsetWritten = 0;
    appData.overrunSequence = 0;
    appData.speakerEOS = false;
    appData.speakerOpen = false;
    appData.micIndex = 0;
    appData.state = AIS_STATE_IDLE;
    appData.prevState = AIS_STATE_IDLE;
    appData.currTime = 0;
    appData.prevTime = 0;
    appData.lastRefresh = 0;
    appData.speakerMarker.echoRequest = false;
    appData.speakerBufferState = AIS_BUFFER_STATE_GOOD;
    appData.volume = 100;

    s_alertsTask = AIS_Alerts_GetThisHandle();

    if (!s_thinkStateTimer)
    {
        s_thinkStateTimer = xTimerCreate("ThinkStateTimer",                               /* name */
                                         pdMS_TO_TICKS(AIS_APP_THINK_STATE_TIMEOUT_MSEC), /* period */
                                         pdFALSE,                                         /* self reload */
                                         (void *)0,                                       /* id */
                                         vThinkStateTimerCallback /* function to call when expiring */
        );

        if (!s_thinkStateTimer)
        {
            configPRINTF(("xTimerCreate failed\r\n"));
        }
    }
}

uint32_t AIS_AppCallback_Microphone(uint8_t **data, uint32_t *size)
{
    uint32_t ret = kStatus_NoTransferInProgress;

    if (kMicCloudWakeVerifier == audio_processing_get_state())
    {
        if (appData.expectedTickTime < xTaskGetTickCount())
        {
            ret = audio_processing_get_continuous_utterance(&pu8PreambleData, &u32PreambleSize, &u32PreambleIdx, size,
                                                            data);
            appData.expectedTickTime = xTaskGetTickCount() + (portTICK_PERIOD_MS * 50);
        }
    }
    else if (kMicRecording == audio_processing_get_state())
    {
        if (appData.expectedTickTime < xTaskGetTickCount())
        {
            /* Needs to be an if else or the last of the wake word will be overwritten */
            s_recordData = &(*data)[0];
            ret = audio_processing_get_output_buffer(&s_recordData, &s_recordDataSize);

            if (kStatus_Success == ret)
            {
                //*data = micData;
                *size = AIS_MIC_PACKET_SIZE;
            }
            appData.expectedTickTime = xTaskGetTickCount() + (portTICK_PERIOD_MS * 50);
        }
    }

    return ret;
}

static void _SpeakerEchoMarker(ais_handle_t *handle, uint32_t buffered)
{
    if (appData.speakerMarker.echoRequest)
    {
        uint64_t offsetReached;

        offsetReached = appData.speakerOffsetWritten - buffered;

        /* Send event when we have played back this data. */
        if (offsetReached >= appData.speakerMarker.offset)
        {
            AIS_EventSpeakerMarkerEncountered(handle, appData.speakerMarker.marker);
            appData.speakerMarker.echoRequest = false;
        }
    }
}

static void _SpeakerFlowControl(ais_handle_t *handle, uint32_t buffered)
{
    streamer_handle_t *streamer = (streamer_handle_t *)handle->audioPlayer;
    uint32_t lastSeq;
    bool dataComplete = false;

    if (appData.speakerOffsetEnd > 0 && (appData.speakerOffsetWritten >= appData.speakerOffsetEnd))
    {
        dataComplete = true;
    }

    if (STREAMER_IsPlaying(streamer))
    {
        lastSeq = handle->topicSequence[AIS_TOPIC_SPEAKER] - 1;

        /* NOTE: Hard overflow is signaled and sent from overflow callback! */
        if (appData.speakerBufferState == AIS_BUFFER_STATE_OVERRUN)
        {
            return;
        }

        /* CASE 1: above overrun threshold */
        if (buffered >= AWS_AUDIO_BUFFER_OVERRUN_THRESHOLD && !dataComplete)
        {
            if (appData.speakerBufferState == AIS_BUFFER_STATE_GOOD)
            {
                appData.prevSpeakerBufferState = appData.speakerBufferState;
                appData.speakerBufferState = AIS_BUFFER_STATE_OVERRUN_WARNING;
                AIS_EventBufferStateChanged(handle, appData.prevSpeakerBufferState, AIS_BUFFER_STATE_OVERRUN_WARNING,
                                            lastSeq);
            }
        }
        else
            /* CASE 2: below underrun threshold */
            if ((buffered <= AWS_AUDIO_BUFFER_UNDERRUN_THRESHOLD) && !dataComplete)
        {
            if (buffered == 0)
            {
                /* Hard underflow */
                if (appData.speakerBufferState == AIS_BUFFER_STATE_UNDERRUN_WARNING)
                {
                    appData.prevSpeakerBufferState = appData.speakerBufferState;
                    appData.speakerBufferState = AIS_BUFFER_STATE_UNDERRUN;
                    AIS_EventBufferStateChanged(handle, appData.prevSpeakerBufferState, AIS_BUFFER_STATE_UNDERRUN,
                                                lastSeq);
                }
            }
            else
            {
                /* Below underrun threshold, but not hard underflow */
                if (appData.speakerBufferState == AIS_BUFFER_STATE_GOOD ||
                    appData.speakerBufferState == AIS_BUFFER_STATE_UNDERRUN)
                {
                    appData.prevSpeakerBufferState = appData.speakerBufferState;
                    appData.speakerBufferState = AIS_BUFFER_STATE_UNDERRUN_WARNING;
                    AIS_EventBufferStateChanged(handle, appData.prevSpeakerBufferState,
                                                AIS_BUFFER_STATE_UNDERRUN_WARNING, lastSeq);
                }
            }
        }
        /* CASE 3: above underrun threshold, below overrun threshold */
        else
        {
            /* Buffer in good state. */
            if (appData.speakerBufferState != AIS_BUFFER_STATE_GOOD &&
                appData.speakerBufferState != AIS_BUFFER_STATE_OVERRUN)
            {
                configPRINTF(("Previous speaker buffer state: %s new state: %s buffered: %d\r\n",
                              ais_buffer_state_str[appData.speakerBufferState],
                              ais_buffer_state_str[AIS_BUFFER_STATE_GOOD], buffered));

                appData.prevSpeakerBufferState = appData.speakerBufferState;
                appData.speakerBufferState = AIS_BUFFER_STATE_GOOD;
            }
        }
    }
    else /* streamer not playing */
    {
        if (buffered >= AWS_AUDIO_BUFFER_OVERRUN_THRESHOLD)
        {
            /* Above overrun threshold */
            if (appData.speakerBufferState != AIS_BUFFER_STATE_GOOD)
            {
                /* If we are in barge-in state, pull the number of bytes and throw half of them away if overflow */
                if ((audio_processing_get_state() == kMicRecording))
                {
                    uint32_t queued = STREAMER_GetQueued(streamer);

                    uint32_t numberOfFrames = (queued / STREAMER_PCM_OPUS_FRAME_SIZE);
                    uint32_t bytesToDrop = ((numberOfFrames / 2) * STREAMER_PCM_OPUS_FRAME_SIZE);

                    STREAMER_Read(NULL, bytesToDrop);
                }
            }
        }
        else
        {
            /* Buffer in good state. */
            if (appData.speakerBufferState != AIS_BUFFER_STATE_GOOD)
            {
                configPRINTF(("Previous speaker buffer state: %s new state: %s buffered: %d\r\n",
                              ais_buffer_state_str[appData.speakerBufferState],
                              ais_buffer_state_str[AIS_BUFFER_STATE_GOOD], buffered));
                appData.prevSpeakerBufferState = appData.speakerBufferState;
                appData.speakerBufferState = AIS_BUFFER_STATE_GOOD;
            }
        }
    }
}

volatile uint32_t taskSpeakCount = 0U;
void AIS_AppCallback_SpeakerState(ais_handle_t *handle)
{
    streamer_handle_t *streamer = (streamer_handle_t *)handle->audioPlayer;
    uint32_t buffered, totalExpected = 0;

    STREAMER_MutexLock();

    buffered = STREAMER_GetQueued(streamer);

    /* Check marker for sending SpeakerMarkerEncountered. */
    _SpeakerEchoMarker(handle, buffered);

    /* Check for audio buffer over/underrun issues. */
    _SpeakerFlowControl(handle, buffered);

    /* Check for EOS from the streamer. */
    if (streamer->eos)
    {
        configPRINTF(("[AIS App] Stopping streamer playback %d\r\n", u32MallocCount));
        streamer->eos = false;
        STREAMER_Stop(streamer);

        appData.speakerEOS = true;

        /* Send SpeakerClosed event response if we have safely finished */
        if ((appData.speakerOffsetEnd > 0) && (appData.speakerOffsetStart != appData.speakerOffsetEnd) &&
            (appData.speakerOffsetWritten >= appData.speakerOffsetEnd))
        {
        	appData.speakerOpen = false;
            AIS_EventSpeakerClosed(handle, appData.speakerOffsetWritten);
        }
        // pdm_to_pcm_set_gain(3);
    }

    /* Calculate total amount of bytes received in this audio stream, based on
     * the start offset. */
    if (appData.speakerOffsetStart < 0)
    {
        buffered = 0;
        totalExpected = 0;
    }
    else if (appData.speakerOffsetEnd > 0)
    {
        totalExpected = appData.speakerOffsetEnd - appData.speakerOffsetStart;
    }

    /* Check for conditions to begin streamer playback. */
    if ((STREAMER_GetQueued(streamer) > 0) && (!STREAMER_IsPlaying(streamer)) &&
        (audio_processing_get_state() == kIdle))
    {
        /* Network buffer threshold has been exceeded, or all data collected. */
        if ((STREAMER_GetQueued(streamer) > AWS_AUDIO_START_THRESHOLD) || (totalExpected && buffered >= totalExpected))
        {
            appData.speakerEOS = false;
            configPRINTF(("[AIS App] Starting streamer playback %d\r\n", u32MallocCount));
            STREAMER_Start(streamer);
            STREAMER_SetVolume(appData.volume);
        }
        else if (appData.speakerEOS && (appData.speakerOffsetWritten >= appData.speakerOffsetEnd))
        {
            /* Flush streamer buffer if we are at the end of the rx audio. */
            /* Check if we have a new OpenSpeaker */
            if (appData.speakerOffsetStart < appData.speakerOffsetEnd)
            {
                appData.speakerEOS = false;

                configPRINTF(("[AIS App] Stopping streamer playback %d\r\n", u32MallocCount));
                STREAMER_Stop(streamer);
            }
        }
    }

    /* Check for end of playback, to exit speaker state. */
    if (appData.speakerOffsetEnd > 0 && !appData.speakerMarker.echoRequest)
    {
        if ((buffered == 0) && !STREAMER_IsPlaying(streamer) &&
            (appData.speakerOffsetWritten >= appData.speakerOffsetEnd))
        {
            /* TODO: make sure we have hit offset end, otherwise keep playing. */

            /* Check if we have a new OpenSpeaker */
            if (appData.speakerOffsetStart < appData.speakerOffsetEnd)
            {
                configPRINTF(("[AIS App] Exiting Speaker State\r\n"));

                AIS_ClearState(handle, AIS_TASK_STATE_SPEAKER);

                /* Reset offsets for the next audio stream. */
                appData.speakerOffsetStart = -1;
                appData.speakerOffsetEnd = -1;
                appData.speakerEOS = false;
                appData.speakerBufferState = AIS_BUFFER_STATE_GOOD;
            }
        }
    }

    STREAMER_MutexUnlock();
}

/* Return available audio buffer space. */
uint32_t AIS_AppCallback_SpeakerAvailableBuffer(ais_handle_t *handle)
{
    streamer_handle_t *streamer = (streamer_handle_t *)handle->audioPlayer;
    uint32_t buffered;

    buffered = STREAMER_GetQueued(streamer);

    return (AWS_AUDIO_BUFFER_SIZE - buffered);
}

void AIS_AppCallback_SpeakerOverflow(ais_handle_t *handle, uint32_t sequence)
{
    streamer_handle_t *streamer = (streamer_handle_t *)handle->audioPlayer;

    configPRINTF(("[AIS App] Speaker overflow detected, sequence: %d\r\n", sequence));

    STREAMER_MutexLock();

    appData.prevSpeakerBufferState = appData.speakerBufferState;
    appData.speakerBufferState = AIS_BUFFER_STATE_OVERRUN;
    appData.overrunSequence = sequence;

    /* send the overrun message directly from here */
    /* Need to send Overflow regardless of the previous state. This is a critical point */
    AIS_EventBufferStateChanged(handle, appData.prevSpeakerBufferState, AIS_BUFFER_STATE_OVERRUN,
                                appData.overrunSequence);

    if (!STREAMER_IsPlaying(streamer))
    {
        uint32_t queued = STREAMER_GetQueued(streamer);

        uint32_t numberOfFrames = (queued / STREAMER_PCM_OPUS_FRAME_SIZE);
        uint32_t bytesToDrop = ((numberOfFrames / 2) * STREAMER_PCM_OPUS_FRAME_SIZE);

        STREAMER_Read(NULL, bytesToDrop);
    }

    STREAMER_MutexUnlock();
}

// TODO unused
/* When this function is executed, there is enough space to buffer the data. */
void AIS_AppCallback_Overflow(ais_handle_t *handle)
{
    /* TODO: don't write data to the streamer if the offset is before the
     * OpenSpeaker value.  Need to buffer 3-5 message sequences for reorder and
     * throwaway detection. */

    streamer_handle_t *streamer = (streamer_handle_t *)handle->audioPlayer;

    /* Don't process this message since there isn't enough room to buffer
     * the audio data.  It will be added to the queue and checked again. */
    if (!STREAMER_IsPlaying(streamer))
    {
        uint32_t queued = STREAMER_GetQueued(streamer);

        uint32_t numberOfFrames = (queued / STREAMER_PCM_OPUS_FRAME_SIZE);
        uint32_t bytesToDrop = ((numberOfFrames / 2) * STREAMER_PCM_OPUS_FRAME_SIZE);
        // appData.speakerOffsetStart = (numberOfFrames / 2) * STREAMER_PCM_OPUS_DATA_SIZE;
        // appData.speakerOffsetEnd = (numberOfFrames / 2) * STREAMER_PCM_OPUS_DATA_SIZE;

        STREAMER_Read(NULL, bytesToDrop);
    }
}

/* When this function is executed, there is enough space to buffer the data. */
void AIS_AppCallback_Speaker(ais_handle_t *handle, uint8_t *data, uint32_t size, uint64_t offset)
{
    /* TODO: don't write data to the streamer if the offset is before the
     * OpenSpeaker value.  Need to buffer 3-5 message sequences for reorder and
     * throwaway detection. */

    if (handle->config->speakerDecoder == AIS_SPEAKER_DECODER_OPUS)
    {
        /* Write the size of the packet to the streamer. */
        STREAMER_Write((uint8_t *)&size, sizeof(uint32_t));
    }

    /* Write the actual data. */
    STREAMER_Write(data, size);

    appData.speakerOffsetWritten = offset + size;
}

void AIS_AppCallback_SpeakerMarker(ais_handle_t *handle, uint32_t marker)
{
    configPRINTF(("[AIS App] Speaker Marker Received: %d\r\n", marker));

    appData.speakerMarker.marker = marker;
    appData.speakerMarker.offset = appData.speakerOffsetWritten;
    appData.speakerMarker.echoRequest = true;
}

void AIS_AppCallback_OpenSpeaker(ais_handle_t *handle, uint64_t offset)
{
    uint32_t bytesToDrop = 0;
    /* Speaker data from AIS will start shortly.
     * Discard any data prior to 'offset'. */

    configPRINTF(("[AIS App] OpenSpeaker offset: %llu\r\n", offset));

	appData.speakerOpen = true;
	
    volatile uint32_t u32SpeakerStreamerOffset =
        ((offset / STREAMER_PCM_OPUS_DATA_SIZE) * STREAMER_PCM_OPUS_FRAME_SIZE);
    volatile uint32_t u32SpeakerStreamerEnd =
        ((appData.speakerOffsetEnd / STREAMER_PCM_OPUS_DATA_SIZE) * STREAMER_PCM_OPUS_FRAME_SIZE);

    bytesToDrop = u32SpeakerStreamerOffset - u32SpeakerStreamerEnd;

    /* Need to dump all the speaker data from the previous request for barge-in use case
     * If u32SpeakerStreamerEnd is 0 then the last request reached the end so we are not in the barge-in use case*/
    if (bytesToDrop > 0 && u32SpeakerStreamerEnd > 0)
    {
        configPRINTF(("[AIS App] Dropping extra packets %d\r\n", bytesToDrop));
        STREAMER_Read(NULL, bytesToDrop);
    }

    /* This offset value is high, it dosen't know when you interrupted
     * the last message so it contains data that has been dropped. But we
     * need it's value so we can determine when the interrupted message has ended */
    appData.speakerOffsetStart = offset;

    /* Only go to idle if we aren't in a wakeup or mic sending state. This is a guard against
     * Messages that have been sent from the client and server side and they are in flight (timing)
     */
    if (audio_processing_get_state() == kMicStopRecording || audio_processing_get_state() == kMicStop)
    {
        audio_processing_set_state(kIdle);
    }
}

void AIS_AppCallback_CloseSpeaker(ais_handle_t *handle, uint64_t offset, bool immediate)
{
    streamer_handle_t *streamer = (streamer_handle_t *)handle->audioPlayer;

    if (!immediate)
    {
        configPRINTF(("[AIS App] CloseSpeaker offset: %llu\r\n", offset));
    }
    else
    {
        configPRINTF(("[AIS App] CloseSpeaker (immediately)\r\n"));
    }

    STREAMER_MutexLock();
    /* Protect streamer updates */
    if (!immediate)
    {
        if (AIS_CheckState(handle, AIS_TASK_STATE_MICROPHONE))
        {
            /* The only scenario this should now hit is if wake word was uttered before the first speaker packet is
             * played */
            if (appData.speakerOffsetEnd != appData.speakerOffsetStart)
            {
                /* Set the end to the start as we never played anything out of the speaker */
                appData.speakerOffsetEnd = appData.speakerOffsetStart;

                if (appData.speakerOffsetStart != 0)
                {
                    appData.speakerOffsetWritten = appData.speakerOffsetStart;
    				appData.speakerOpen = false;
                    AIS_EventSpeakerClosed(handle, appData.speakerOffsetStart);
                }
                /* Needs to be guarded although this may need to be zero? */
                else
                {
    				appData.speakerOpen = false;
                    AIS_EventSpeakerClosed(handle, 1);
                }
            }
        }
        else
        {
            appData.speakerOffsetEnd = offset;
        }
    }
    else
    {
        if (STREAMER_IsPlaying(streamer))
        {
            /* If no offset passed in, stop playback immediately. */
            configPRINTF(("[AIS App] Stopping streamer playback %d\r\n", u32MallocCount));
            STREAMER_Stop(streamer);
        }
        else
        {
            /* We aren't in speaker state so send closed speaker with what was written */
            if ((appData.speakerOffsetEnd != appData.speakerOffsetStart))
            {
				appData.speakerOffsetEnd = appData.speakerOffsetStart;
				appData.speakerOffsetWritten = appData.speakerOffsetStart;

                /* Guarding against a speaker close before outputting the first speaker packet */
                if (appData.speakerOffsetWritten == 0)
                {
					appData.speakerOffsetWritten = 1;
                }
			}
    	}

        /* Need to send a close speaker to ensure the service knows we have stopped
         * Without this, the service will not change song/fire timers etc*/
    	if (appData.speakerOpen)
				{
    		appData.speakerOpen = false;
					AIS_EventSpeakerClosed(handle, appData.speakerOffsetWritten);
				}

        AIS_ClearState(handle, AIS_TASK_STATE_SPEAKER);
    }
    /* Finished streamer updates */
    STREAMER_MutexUnlock();
}

uint32_t idleCount = 0U, thinkCount = 0U, speakCount = 0U;
void AIS_AppCallback_SetAttentionState(ais_state_t state, uint64_t offset, bool immediate)
{
    /* App TODO: handle service state as part of Alexa UX attention system. */

    if (!immediate)
    {
        configPRINTF(("[AIS App] SetAttentionState: %d, offset: %llu\r\n", state, offset));
        /* TODO: transition when 'offset' is hit in the audio stream. */
    }
    else
    {
        configPRINTF(("[AIS App] SetAttentionState: %d (immediately)\r\n", state));
    }

    appData.prevState = appData.state;
    appData.state = state;

    if (AIS_STATE_IDLE == appData.state)
    {
        idleCount++;
        if (AIS_STATE_SPEAKING == appData.prevState)
        {
            if (!audio_processing_get_mute())
            {
                ux_attention_set_state(uxSpeakingEnd);
            }
            else
            {
                ux_attention_set_state(uxMicOntoOff);
            }
        }
        else
        {
            audio_processing_set_state(kIdle);

            if (!audio_processing_get_mute())
            {
                if (uxDeviceChange == ux_attention_get_state())
                {
                    ux_attention_set_state(uxDeviceChange);
                }
                else
                {
                    ux_attention_set_state(uxIdle);
                }
            }
            else
            {
                ux_attention_set_state(uxMicOntoOff);
            }
        }
    }
    else if (AIS_STATE_THINKING == appData.state)
    {
        thinkCount++;
        ux_attention_set_state(uxThinking);

        if (s_thinkStateTimer && (xTimerReset(s_thinkStateTimer, 0) != pdPASS))
        {
            configPRINTF(("xTimerReset failed\r\n"));
        }
    }
    else if (AIS_STATE_SPEAKING == appData.state)
    {
        speakCount++;
        ux_attention_set_state(uxSpeaking);
    }
    else if (AIS_STATE_ALERTING == appData.state)
    {
        // TODO: trigger alert callback
        configPRINTF(("[AIS] Alerting!\r\n"));
    }
    else if (AIS_STATE_INVALID == appData.state)
    {
        // TODO: handle invalid state
    }
}

void AIS_AppCallback_SetVolume(ais_handle_t *handle, uint32_t volume, uint64_t offset, bool immediate)
{
    /* App TODO: apply volume setting to speaker output. */

    if (!immediate)
    {
        configPRINTF(("[AIS App] SetVolume: %d, offset: %llu\r\n", volume, offset));

        /* TODO: transition when 'offset' is hit in the audios tream. */
    }
    else
    {
        configPRINTF(("[AIS App] SetVolume: %d (immediately)\r\n", volume));
    }

    if (appData.volume != volume)
    {
        ux_attention_set_state(uxDeviceChange);

        ux_attention_resume_state(appData.state);
    }

    STREAMER_SetVolume(volume);
    appData.volume = volume;
}

void AIS_AppCallback_SetAlertVolume(uint32_t volume)
{
    /* App TODO: apply alert volume setting. */

    configPRINTF(("[AIS App] SetAlertVolume: %d\r\n", volume));
}

void AIS_AppCallback_SetClock(uint64_t time)
{
    /* App TODO: store clock value and use for timer/alerts. */

    appData.prevTime = appData.currTime;
    appData.currTime = time;
    appData.lastRefresh = appData.currTime;
    appData.syncClockReceived = true;

    // configPRINTF(("[AIS App] SetClock: %llu\r\n", appData.currTime));

    // Adjust epoch to 1970 base year
    time_t epoch = appData.currTime - kEpochAdjust;
    struct tm *timeNow = (struct tm *)pvPortMalloc(sizeof(struct tm));
    configPRINTF(("[AIS App] SetClock: %s\r\n", asctime(gmtime(&epoch))));
    vPortFree(timeNow);
    timeNow = NULL;
}

void AIS_AppCallback_SetAlert(const char *token,
                              uint64_t scheduledTime,
                              uint32_t durationInMilliseconds,
                              ais_alert_type_t type)
{
    ais_alert_t alert = {0};
    memcpy(alert.token, token, safe_strlen(token, AIS_MAX_ALERT_TOKEN_LEN_BYTES));
    alert.scheduledTime = scheduledTime;
    alert.durationMs = durationInMilliseconds;
    alert.type = type;
    alert.valid = true;
    alert.idle = true;

    int32_t status = AIS_Alerts_SaveAlert(&alert);

    if (0 < status)
    {
        // Existing Alert has been triggered by AIS [status is alert index + 1]
        uint32_t alertIdx = status - 1;

        // Send notification to ux
        AIS_Alerts_Trigger(alertIdx, false);

        // Mark triggered in memory
        AIS_Alerts_MarkAsTriggered(alertIdx);

        configPRINTF(("[AIS App] Alert Triggered: %s\r\n", token));

        // Push token to queue to read from app task
        xQueueSend(g_alertQueue, token, (TickType_t)0);

        // Notify app task of online trigger
        xTaskNotify(appTaskHandle, kAlertOnlineTrigger, eSetBits);
    }
    else if (0 == status)
    {
        configPRINTF(("[AIS App] SetAlert process success: %s\r\n", token));

        // Push token to queue to read from app task
        xQueueSend(g_alertQueue, token, (TickType_t)0);

        // Notify alerts task to save to NVM
        xTaskNotify(s_alertsTask, kNewAlertSet, eSetBits);

        // Notify app task of success
        xTaskNotify(appTaskHandle, kNewAlertSet, eSetBits);
    }
    else
    {
        configPRINTF(("[AIS App] SetAlert process failure: %s\r\n", token));

        // Push token to queue to read from app task
        xQueueSend(g_alertQueue, token, (TickType_t)0);

        // Notify app task of failure
        xTaskNotify(appTaskHandle, kNewAlertFail, eSetBits);
    }
}

void AIS_AppCallback_DeleteAlert(const char *token)
{
    /* App TODO: Delete alert from local memory. */

    configPRINTF(("[AIS App] DeleteAlert: %s\r\n", token));

    size_t len = safe_strlen(token, AIS_MAX_ALERT_TOKEN_LEN_BYTES);
    int32_t status = AIS_Alerts_MarkForDelete(token, len);

    // Push token to queue to read from app task
    xQueueSend(g_alertQueue, token, (TickType_t)0);

    if (0 == status)
    {
        // Notify alerts task to remove from NVM
        xTaskNotify(s_alertsTask, kAlertDelete, eSetBits);

        // Notify app task of success
        xTaskNotify(appTaskHandle, kAlertDelete, eSetBits);
    }
    else
    {
        // Notify app task of failure
        xTaskNotify(appTaskHandle, kFailDelete, eSetBits);
    }
}

void AIS_AppCallback_OpenMicrophone(uint32_t timeout, const char *type, const char *token)
{
    /* App TODO: AIS requests mic input.
     * Respond with MicrophoneOpened within 'timeout' time, or send
     * OpenMicrophoneTimedOut. Pass the 'type' and 'token' parameters back with
     * the MicrophoneOpened request, if non-NULL. */
    audio_processing_set_state(kMicKeepOpen);

    // configPRINTF(("[AIS App] OpenMicrophone timeout: %d\r\n", timeout));
}

void AIS_AppCallback_CloseMicrophone(ais_handle_t *handle)
{
    /* App TODO: AIS requests close mic input. */

    /* XXX: need mutex around the changing of this in mic callback.
     * If we modify this here while state machine is updating, can become
     * non-zero before the start of another microphone send. */

    appData.micIndex = 0;

    audio_processing_set_state(kMicStopRecording);
}

void AIS_AppCallback_Exception(const char *code, const char *description)
{
    /* App TODO: handle exception error data. */

    configPRINTF(("[AIS App] Exception: %s: %s\r\n", code, description));
}

void AIS_AppError(ais_status_t error)
{
    /* App TODO: handle AIS error indication.
     * Microphone errors will transition AIS to IDLE state.  All others are
     * critical and require a restart of the service. */
}

void AIS_AppSaveSecret(uint8_t *secret)
{

    /* Write shared secret to hyperflash. */
    if (kStatus_Success == SLN_FLASH_MGMT_Save(AIS_SECRET_KEY_FILE_NAME, secret, AIS_SECRET_LENGTH))
    {
        configPRINTF(("[AIS App] Registration secret key saved to flash\r\n"));
    }
    else
    {
        configPRINTF(("[AIS App] Registration secret key could not be saved to flash\r\n"));
    }
}

void AIS_AppDisconnected(ais_handle_t *handle, ais_disconnect_code_t code, const char *description)
{
    /* Signal reconnection task that disconnect has occurred */
    switch (code)
    {
        case AIS_DISCONNECT_NONE:
            break;
        case AIS_DISCONNECT_INVALID_SEQUENCE:
            reconnection_task_set_event(kReconnectInvalidSequence);
            break;

        case AIS_DISCONNECT_API_DEPRECATED:
            reconnection_task_set_event(kReconnectAPIDeprecated);
            break;

        case AIS_DISCONNECT_ENCRYPTION_ERROR:
            reconnection_task_set_event(kReconnectEncryptionError);
            break;

        case AIS_DISCONNECT_GOING_OFFLINE:
        default:
            reconnection_task_set_event(kReconnectGoingOffline);
            break;
    }

    /* Signal audio processing task that we are offline */
    audio_processing_set_state(kReconnect);

    configPRINTF(("[AIS App] Disconnected from service: %s\r\n", description));
}

/**
 * @brief Function called when the registered timer's timeout is triggered.
 *
 * @param xTimer[in]    Pointer to the timer structure
 * @return              Void
 */
static void vThinkStateTimerCallback(TimerHandle_t xTimer)
{
    if (AIS_STATE_THINKING == appData.state)
    {
        /* still in thinking state when the timeout expired, trigger reconnect */
        audio_processing_set_state(kReconnect);
        configPRINTF(("Thinking state timeout, triggering a reconnection\r\n"));
    }
}
