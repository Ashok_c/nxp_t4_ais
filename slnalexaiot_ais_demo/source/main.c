/*
 * Copyright 2018 NXP.
 * This software is owned or controlled by NXP and may only be used strictly in accordance with the
 * license terms that accompany it. By expressly accepting such terms or by downloading, installing,
 * activating and/or otherwise using the software, you are agreeing that you have read, and that you
 * agree to comply with and are bound by, such license terms. If you do not agree to be bound by the
 * applicable license terms, then you may not retain, install, activate or otherwise use the software.
 */

#include <time.h>

/* Board includes */
#include "board.h"
#include "pin_mux.h"

/* FreeRTOS kernel includes */
#include "FreeRTOS.h"
#include "task.h"

/* Crypto includes */
#include "ksdk_mbedtls.h"

/* AWS includes */
#include "aws_system_init.h"
#include "aws_logging_task.h"
#include "aws_clientcredential.h"
#include "aws_ota_check.h"
#include "aws_user_credentials.h"

/* AIS includes */
#include "aisv2.h"
#include "aisv2_app.h"
#include "ais_streamer.h"

/* Driver includes */
#include "fsl_dmamux.h"
#include "fsl_edma.h"

/* Audio processing includes */
#include "pdm_to_pcm_task.h"
#include "audio_processing_task.h"
#include "sln_amplifier.h"

/* Network connection includes */
#include "udp_server.h"
#include "ais_authentication_task.h"
#include "network_connection.h"
#include "httpsclient.h"
#include "reconnection_task.h"

#include "ais_alerts.h"

/* Shell includes */
#include "sln_shell.h"

/* UX includes */
#include "sln_RT10xx_RGB_LED_driver.h"
#include "audio_samples.h"
#include "uvoice_ux_attention_system.h"

/* Button includes */
#include<switch.h>

/* Fault includes */
#include "fault_handlers.h"

#if PROVISIONING_METHOD == PROVISIONING_USE_BLE
/* Bluetooth includes */
#include "ble_connection.h"
#include "ble_server.h"
#endif

#define APP_INIT_NETWORK_CHECK_COUNT    30

#define LOGGING_STACK_SIZE 256
#define LOGGING_QUEUE_LENGTH 64

/* AIS Solution Default client ID matches given IoT Thing Name */
//#define alexaCLIENT_ID clientcredentialIOT_THING_NAME

char *alexaCLIENT_ID;

//#define alexaTOPIC_ROOT "$aws/alexa"

//#ifndef alexaACCOUNT_ID
//#define alexaACCOUNT_ID "938289322244"
//#endif

#ifndef alexaAUTH_CLIENT_ID
#define alexaAUTH_CLIENT_ID "Paste Auth Client ID here."
#endif

#ifndef alexaAUTH_TOKEN
#define alexaAUTH_TOKEN "Paste Auth Token here."
#endif

#include "sln_flash_mgmt.h"
#include "sln_file_table.h"

aws_user_aws_details_t flash_aws_usr_cred = {0};

const uint32_t kSyncClock = (1U << 31U);
const uint64_t kEpochDayZero = 3752179200ULL; // 11/26/2018 0:00:00

volatile uint32_t aisAlertCount = 0U;
__attribute__((section(".ocram_non_cacheable_data"))) ais_alert_t appAlerts[AIS_APP_MAX_ALERT_COUNT] = {0};
__attribute__((section(".ocram_non_cacheable_data"))) char *alertsDelete[AIS_APP_MAX_ALERT_COUNT] = {0};

__attribute__((section(".ocram_non_cacheable_data"))) ais_handle_t aisHandle;
__attribute__((section(".ocram_non_cacheable_data"))) ais_app_data_t appData;
__attribute__((section(".ocram_non_cacheable_data"))) ais_config_t aisConfig;
streamer_handle_t streamerHandle;

ota_handle_t otaHandle;

MQTTAgentHandle_t mqttHandle;

TaskHandle_t appTaskHandle = NULL;
TaskHandle_t appReconnectHandle = NULL;
TaskHandle_t appInitTaskHandle = NULL;
TaskHandle_t appInitDummyNullHandle = NULL;
TaskHandle_t aisAuthTaskHandle = NULL;
TaskHandle_t xAudioProcessingTaskHandle = NULL;
TaskHandle_t xPdmToPcmTaskHandle = NULL;
TaskHandle_t xUXAttentionTaskHandle = NULL;
TaskHandle_t xReconnectionTaskHandle = NULL;
TaskHandle_t xOfflineAudioTaskHandle = NULL;

EventGroupHandle_t s_offlineAudioEventGroup = NULL;

extern EventGroupHandle_t g_button_pressed;
BaseType_t xHigherPriorityTaskWoken = pdFALSE;



extern uint32_t u32MallocCount;

/* Clock sync timer handle */
TimerHandle_t aisClockSyncHandle;

/* Clock sync directive received check timer handle */
TimerHandle_t aisClockSyncCheckHandle;

QueueHandle_t g_alertQueue;

#define pdm_to_pcm_task_PRIORITY (configMAX_PRIORITIES - 2)
#define audio_processing_task_PRIORITY (configMAX_PRIORITIES - 1)

void offline_audio_task(void *arg);

#if 0
void vApplicationMallocFailedHook( void )
{
    __asm("BKPT #1");
}
#endif

void ais_app_clock_cb(TimerHandle_t xTimer)
{
    appData.prevTime = appData.currTime;
    appData.currTime += AIS_APP_TIMER_INTERVAL_SEC;

    uint64_t timeSinceSync = appData.currTime - appData.lastRefresh;

    if ( (AIS_APP_TIME_SYNC_INTERVAL_SEC <= timeSinceSync) && (reconnection_task_get_state() == kStartState) )
    {
        xTaskNotify( appTaskHandle, kSyncClock, eSetBits );
    }

    if (kStartState != reconnection_task_get_state())
    {
        // Begin check for timers that should have triggered (Offline use case)
        for (uint32_t idx = 0; idx < AIS_APP_MAX_ALERT_COUNT; idx++)
        {

            uint64_t alertTime = AIS_Alerts_GetScheduledTime(idx);

            // Any timer before this date is guaranteed to be invalid
            if ((kEpochDayZero < alertTime) && ((alertTime != UINT64_MAX)))
            {
                if (alertTime < appData.currTime)
                {

                    // Trigger alerts that aren't already triggered
                    if (true == AIS_Alerts_GetIdleState(idx))
                    {
                        // Trigger alert in UX system
                        AIS_Alerts_Trigger(idx, true);

                        // Mark this as triggered
                        AIS_Alerts_MarkAsTriggeredOffline(idx);
                    }

                }
            }

        }
    }
}

void ais_app_sync_clock_check_cb(TimerHandle_t xTimer)
{
    /* reconnect if sync clock directive not received in specified timeout */
    if ( (!appData.syncClockReceived) && (reconnection_task_get_state() == kStartState) )
    {
        configPRINTF(("Sync clock directive not received in specified timeout, triggering a reconnection\r\n"));
        reconnection_task_set_event(kReconnectGoingOffline);
        audio_processing_set_state(kReconnect);
    }
}

void APP_Wifi_UX_Callback(network_wifi_conn_state_t state)
{
    ux_attention_states_t uxState = uxError;

    switch (state)
    {
        case NETWORK_WIFI_CONN_WIFI_SETUP:
            uxState = uxWiFiSetup;
            break;
        case NETWORK_WIFI_CONN_INVALID_WIFI_CRED:
            uxState = uxInvalidWiFiCred;
            break;
        case NETWORK_WIFI_CONN_NO_ACCESS_POINT:
            if (uxNoAccessPoint != ux_attention_get_state())
            {
                uxState = uxNoAccessPoint;
            }
            break;
        case NETWORK_WIFI_CONN_ACCESS_POINT_FOUND:
            uxState = uxAccessPointFound;
            break;
        default:
            uxState = uxError;
            configPRINTF(("ERROR: Undefined wifi connection state, %d\r\n", state));
            break;
    };

    if (uxState != uxError)
    {
        ux_attention_set_state(uxState);
    }
}

BaseType_t APP_MQTT_Connect(MQTTAgentHandle_t *xMQTTHandle)
{
    MQTTAgentReturnCode_t xReturned;
    MQTTAgentConnectParams_t xConnectParameters;
    int attempts = 0;

    memset(&xConnectParameters, 0, sizeof(xConnectParameters));

    xConnectParameters.pcURL = flash_aws_usr_cred.mqtt_broker_endpoint.data;
    xConnectParameters.xFlags = mqttagentREQUIRE_TLS;
#if clientcredentialMQTT_BROKER_PORT == 443
    xConnectParameters.xFlags |= mqttagentUSE_AWS_IOT_ALPN_443;
#endif

    /* Deprecated - unused. */
    xConnectParameters.xURLIsIPAddress = pdFALSE;
    xConnectParameters.usPort = clientcredentialMQTT_BROKER_PORT;
    xConnectParameters.pucClientId = (const uint8_t *)alexaCLIENT_ID;
    xConnectParameters.usClientIdLength = (uint16_t)strlen(alexaCLIENT_ID);
    /* Deprecated - unused. */
    xConnectParameters.xSecuredConnection = pdFALSE;
    xConnectParameters.pvUserData = NULL;
    xConnectParameters.pxCallback = NULL;
    xConnectParameters.pcCertificate = NULL;
    xConnectParameters.ulCertificateSize = 0;

    xReturned = MQTT_AGENT_Create(xMQTTHandle);
    if (xReturned != eMQTTAgentSuccess)
    {
        return pdFAIL;
    }

    for (attempts = 0; attempts < 3; attempts++)
    {
        /* Connect to the broker. */
        configPRINTF(
                ("MQTT Alexa attempt %d to connect to %s.\r\n", attempts + 1, flash_aws_usr_cred.mqtt_broker_endpoint.data));
        xReturned = MQTT_AGENT_Connect(*xMQTTHandle, &xConnectParameters, pdMS_TO_TICKS(3000));
        if (xReturned != eMQTTAgentSuccess)
        {
            configPRINTF(("ERROR %d:  MQTT Alexa failed to connect.\r\n", xReturned));
#if USE_WIFI_CONNECTION
            if (WICED_FALSE == get_wifi_connect_state())
            {
                break;
            }
#endif
        }
        else
        {
            configPRINTF(("MQTT Alexa connected.\r\n"));
            return pdPASS;
        }
    }

    /* Could not connect, so delete the MQTT client. */
    MQTT_AGENT_Delete(*xMQTTHandle);

#if USE_WIFI_CONNECTION
    if (3 == attempts)
    {
#endif
        return pdFAIL;
#if USE_WIFI_CONNECTION
    }
    else
    {
        return 2;
    }
#endif
}

void APP_MQTT_Disconnect(MQTTAgentHandle_t *xMQTTHandle)
{
    MQTT_AGENT_Disconnect(*xMQTTHandle, pdMS_TO_TICKS(3000));
    MQTT_AGENT_Delete(*xMQTTHandle);
}

#if USE_WIFI_CONNECTION
void APP_wifi_connect_update_handler(wiced_bool_t event)
{
    switch (event)
    {
    case WICED_FALSE:
        if (WICED_FALSE != get_wifi_connect_state())
        {
            reconnection_task_set_event(kReconnectNetworkLoss);
			audio_processing_set_state(kReconnect);
        }
        break;
    default:
        break;
    }
}

/**
 * @brief Runs the AP mode job for wifi provisioning
 *
 * The job purpose is to get the wifi credentials from the companion app:
 * -The device starts the AP where the mobile will connect
 * -The udp_server_task is used for the communication between device and mobile
 * -The job waits for the ap_close_event bit which will be set after the wifi
 *  credentials were received and saved in flash.
 */
static void APP_WifiProvisioning_ApJob(void)
{
    char *serialNumber = NULL;
    wiced_ssid_t ap_ssid = {0};

    /* Create the SSID and Password */
    APP_GetUniqueID(&serialNumber);
    snprintf((char*)ap_ssid.value, sizeof(ap_ssid.value), "AlexaIotSer_%s", serialNumber);
    ap_ssid.length = strlen((char*)ap_ssid.value);

    if (APP_NETWORK_Wifi_StartAP(ap_ssid, serialNumber))
    {
        configPRINTF(("Could not start AP\r\n"));
    }
    else
    {
        udp_server_args_t udp_server_args = {NETWORK_MODE_AP, xEventGroupCreate()};
        ux_attention_set_state(uxApMode);
        configPRINTF(("AP started\r\n"));

        xTaskCreate(udp_server_task, "UDP_Server_Task", DEFAULT_THREAD_STACKSIZE, &udp_server_args, DEFAULT_THREAD_PRIO, NULL);

        xEventGroupWaitBits(udp_server_args.ap_close_event, 1, pdFALSE, pdFALSE, portMAX_DELAY);

        configPRINTF(("AP close and switch to Station\r\n"));
        vEventGroupDelete(udp_server_args.ap_close_event);
        APP_NETWORK_Wifi_StopAP();
        ux_attention_set_state(uxBootUp);
    }

    vPortFree(serialNumber);
}
#endif

void appInit(void *arg)
{
    status_t ret;
    BaseType_t xReturned;
    uint8_t *fileData = NULL;
    volatile bool registrationNeeded = true;
    uint8_t waitCount = 0;
    fault_status_t s_fault_log;
    status_t status = 0;
    cJSON_Hooks hooks;

    /* Check for empty flash */
    uint8_t validByteCount = 0U;

    //start attention task
    ux_attention_task_Init(&xUXAttentionTaskHandle);

    // start blue and cyan boot up
    ux_attention_set_state(uxBootUp);

    configPRINTF(("*** Starting Alexa Test application ***\r\n"));

    /* Initialize Amazon libraries */
    if (SYSTEM_Init() != pdPASS)
    {
        configPRINTF(("SYSTEM_Init failed\r\n"));
        goto error;
    }

    /* Check if the device was recovered from fault state */
    status = fault_log_flash_get(&s_fault_log);
    if (0 == status)
    {
        fault_check_status(s_fault_log);
    }
    /* At first boot SLN_FLASH_MGMT_ENOENTRY2 is returned because there is no log saved */
    else if (SLN_FLASH_MGMT_ENOENTRY2 != status)
    {
        configPRINTF(("[ERR] Failed to get fault log from flash\r\n"));
    }

    /* make the handle of the current task available in shell task */
    sln_shell_set_app_init_task_handle(&appInitTaskHandle);

    /* Initialize cJSON library to use FreeRTOS heap memory management. */
    hooks.malloc_fn = pvPortMalloc;
    hooks.free_fn = vPortFree;
    cJSON_InitHooks(&hooks);

    /* Initialize network */
#if USE_ETHERNET_CONNECTION
    APP_NETWORK_Init(true);
#elif USE_WIFI_CONNECTION
    APP_NETWORK_Init();

    /* set wifi connect / disconnect handler from here */
    APP_Wifi_Connect_Update_Handler_Set(&APP_wifi_connect_update_handler);

    /* Connect to WiFi network */
    while (1)
    {
        APP_Wifi_UX_Callback(NETWORK_WIFI_CONN_WIFI_SETUP);
        status = APP_NETWORK_Wifi_Connect(true);
        if (WIFI_CONNECT_SUCCESS == status)
        {
            break;
        }
        else
        {
            configPRINTF(("Failed to connect to wifi network, error %d\r\n", status));
            if (status == WIFI_CONNECT_WRONG_CRED)
            {
                /* Erase the wifi credentials if invalid credentials were provided */
                wifi_credentials_flash_reset();
            }
        }

#if PROVISIONING_METHOD == PROVISIONING_USE_WIFI
        configPRINTF(("Start AP job\r\n"));
        SLN_FLASH_MGMT_Erase(AIS_SECRET_KEY_FILE_NAME);
        APP_WifiProvisioning_ApJob();
#elif PROVISIONING_METHOD == PROVISIONING_USE_BLE
        configPRINTF(("Start BLE job\r\n"));
        ux_attention_set_state(uxBleProvision);
        BleProvisioning();
#else

        configPRINTF(("Insert credentials via USB CDC\r\n"));
        APP_Wifi_UX_Callback(NETWORK_WIFI_CONN_INVALID_WIFI_CRED);
        vTaskSuspend(NULL);
#endif
    }
#endif

    /* from this point on, if the appInit task gets suspended, the shell task should not resume it */
    sln_shell_set_app_init_task_handle(&appInitDummyNullHandle);

    /* Try read sharedKey */
    fileData = (uint8_t *)pvPortMalloc(AIS_SECRET_LENGTH);
    if (NULL != fileData)
    {
        //if (kStatus_Success == SLN_Read_Flash_At_Address(SECRET_ADDRESS, fileData, AIS_SECRET_LENGTH))
        uint32_t secLen = AIS_SECRET_LENGTH;
        if (kStatus_Success == SLN_FLASH_MGMT_Read(AIS_SECRET_KEY_FILE_NAME, fileData, &secLen))
        {
            for (uint32_t idx = 0; idx < AIS_SECRET_LENGTH; idx++)
            {
                if ((fileData[idx] != 0x00) && (fileData[idx] != 0xFF))
                {
                    validByteCount++;
                }
            }
        }
    }
    else
    {
        configPRINTF(("ERROR: Out of Memory!!\r\n"));
        goto error;
    }

    if (validByteCount) {
        registrationNeeded = false;
    }

    /* start registration task if the device was not registered */
    if (registrationNeeded)
    {
        configPRINTF(("Device not registered\r\n"));

#if USE_WIFI_CONNECTION
        /* Create ais authentication task, responsible for obtaining
         * the ais access tokens from the ais tokens endpoint */
        ais_authentication_set_app_init_task_handle(&appInitTaskHandle);
        xReturned = xTaskCreate(ais_authentication_task, "AIS_Auth_Task", 1024, &aisAuthTaskHandle, configMAX_PRIORITIES - 1, &aisAuthTaskHandle);
        assert(xReturned == pdPASS);
#endif

        /* Suspend until the tokens are obtained (will be awaken from the auth task) */
        vTaskSuspend(NULL);
    }
    else
    {
        configPRINTF(("Device already registered\r\n"));
    }

    /* Indicate to user we are now connecting */
    ux_attention_set_state(uxReconnecting);

    APP_GetUniqueID(&alexaCLIENT_ID);

    if (pdPASS == APP_MQTT_Connect(&mqttHandle))
    {
#if USE_WIFI_CONNECTION
        companion_app_status_set(MQTT_SUBSCRIBED);
#endif
    }
    else
    {
#if USE_WIFI_CONNECTION
        companion_app_status_set(CONN_ERROR);
#endif
        goto error;
    }

    STREAMER_Init();
    ret = STREAMER_Create(&streamerHandle, AIS_DECODER_OPUS);
    if (ret != kStatus_Success)
    {
        configPRINTF(("STREAMER_Create failed\r\n"));
        goto error;
    }


    /* Setup task for playing offline audio */
    xTaskCreate(offline_audio_task, "Offline_Audio_Task", 256, NULL, configMAX_PRIORITIES - 2, NULL);

    ret = AIS_Init(&aisHandle, &mqttHandle, (void *)&streamerHandle);
    if (ret != kStatus_Success)
    {
        configPRINTF(("AIS_Init failed\r\n"));
        goto error;
    }

    /* Configure the client information. */
    strcpy(aisConfig.awsPartnerRoot, flash_aws_usr_cred.alexa_topic_root.data);
    strcpy(aisConfig.awsClientId, alexaCLIENT_ID);
    strcpy(aisConfig.awsAccountId, flash_aws_usr_cred.alexa_account_id.value);
    strcpy(aisConfig.awsEndpoint, flash_aws_usr_cred.mqtt_broker_endpoint.data);
#if USE_WIFI_CONNECTION
    if (registrationNeeded)
    {
        strcpy(aisConfig.awsAuthClientId, get_lwa_auth_data()->client_id);
    }
#endif
    strcpy(aisConfig.awsAuthToken, alexaAUTH_TOKEN);
    strcpy(aisConfig.locale, "en-US");
    strcpy(aisConfig.firmwareVersion, "1");
    aisConfig.maxAlertCount = AIS_APP_MAX_ALERT_COUNT;
    aisConfig.speakerDecoder = AIS_SPEAKER_DECODER_OPUS;
    aisConfig.speakerChannels = 1;
    aisConfig.speakerBitrate = 64000;
    aisConfig.speakerBitrateType = AIS_SPEAKER_BITRATE_CONSTANT;

    AIS_SetConfig(&aisHandle, &aisConfig);

    /* Set shared secret */
    if (!registrationNeeded)
    {
        AIS_SetSecret(&aisHandle, fileData);
        registrationNeeded = false;
    }
    else
    {
        /* invalid sharedKey secret size */
        configPRINTF(("Invalid sharedKey secret\r\n"));
    }

    /* Free the file data obtained from the flash read */
    vPortFree(fileData);
    fileData = NULL;

    /* Register with AIS
     * Only necessary to be performed once per device. */
#if USE_WIFI_CONNECTION
    if (registrationNeeded)
    {
        char *auth_token = refresh_token_get();
        strcpy(aisConfig.awsAuthToken, auth_token);
        refresh_token_free();

        /* send status to the companion app */
        companion_app_status_set(REGISTERING_REQ);

        ret = AIS_Register(&aisHandle);
        if (ret != kStatus_Success)
        {
            configPRINTF(("AIS_Register failed\r\n"));
            companion_app_status_set(CONN_ERROR);
            goto error;
        }
        else
        {
            companion_app_status_set(REGISTERING_RSP);
        }
    }
#endif

    /* Initialize AIS Alerts module */
    ret = AIS_Alerts_Init();

    if (kStatus_Success != ret)
    {
        configPRINTF(("Alerts Management failed to initialize!\r\n"));
        goto error;
    }

    /* Set UX */
    ux_attention_set_state(uxConnected);

    /* Publish disconnect first otherwise if app is already connected the new
     * connection attempt can fail. */
    AIS_Disconnect(&aisHandle, AIS_DISCONNECT_GOING_OFFLINE);

    configPRINTF(("Wait 1 second\r\n"));
    osa_time_delay(1000);

    ret = AIS_Connect(&aisHandle);
    if (ret != kStatus_Success) {
        configPRINTF(("AIS_Connect failed\r\n"));
#if USE_WIFI_CONNECTION
        companion_app_status_set(CONN_ERROR);
#endif
        goto error;
    }

    /* Gather any alerts requires for deletion */
    alertTokenList_t deleteList;
    uint32_t deleteCount = 0;
    AIS_Alerts_GetDeletedList(&deleteList, &deleteCount);
    configPRINTF(("Found %d alerts ready to delete.\r\n", deleteCount));

    /* Send SynchronizeState to update on our status. */
    char *alertTokens[AIS_APP_MAX_ALERT_COUNT] = {0};
    for (uint32_t idx = 0; idx < deleteCount; idx++)
    {
        alertTokens[idx] = (char *)(&deleteList[idx]);
        configPRINTF(("Alert ready for delete: %s\r\n", alertTokens[idx]));
    }

    AIS_EventSynchronizeState(&aisHandle, appData.volume, (const char **)alertTokens, deleteCount);

#if USE_WIFI_CONNECTION
    companion_app_status_set(CONN_COMPLETE);
#endif

    aisClockSyncHandle = xTimerCreate( "AIS_Clock_Sync", AIS_APP_TIMER_INTERVAL_MSEC, pdTRUE, (void *)0, ais_app_clock_cb );

    xTimerStart( aisClockSyncHandle, 0);

    aisClockSyncCheckHandle = xTimerCreate( "AIS_Clock_Sync_Check", AIS_APP_TIMER_SYNC_CLOCK_TIMEOUT_MSEC, pdFALSE, (void *)0, ais_app_sync_clock_check_cb );

    OTA_Init(&otaHandle, &mqttHandle, (const char *)alexaCLIENT_ID);
    ret = OTA_Connect(&otaHandle);
    if (ret != kStatus_Success)
    {
        configPRINTF(("OTA_Connect failed\r\n"));
        goto error;
    }

    reconnection_task_init(&xReconnectionTaskHandle);

    vTaskResume(appTaskHandle);

    configPRINTF(("Exiting App Init\r\n"));
    vTaskDelete(NULL);

    return;

error:

    configPRINTF(("ERROR: Unable to initialize. Attempting to re-establish connection...\r\n"));

    do
    {
        configPRINTF(("...attempt %d...\r\n", waitCount++));
        vTaskDelay(portTICK_PERIOD_MS * 1000);
#if USE_WIFI_CONNECTION
        if (WICED_TRUE == get_wifi_connect_state())
        {
            configPRINTF(("SUCCESS: WiFi Available!\r\n"));
            break;
        }
#endif

    }
    while (APP_INIT_NETWORK_CHECK_COUNT > waitCount);

    if (APP_INIT_NETWORK_CHECK_COUNT == waitCount)
    {
        configPRINTF(("ERROR: Could not establish network connection! \r\nPlease check network connection and restart the device. \r\n"));
        vTaskDelete(NULL);
    }
    else
    {
        configPRINTF(("Network connection available. Restarting device... \r\n"));
    }

    ux_attention_sys_fault();

    vTaskDelay(portTICK_PERIOD_MS * 5000);

    NVIC_SystemReset();

}

void appTask(void *arg)
{
    ais_mic_open_t micOpen;

    //osa_time_delay(100);
    configPRINTF(("One second delay...\r\n"));
    osa_time_delay(1000);

    /* Queue needs to be initialized before AIS connect as this will cause a fault if a set or delete alert occurs */
    g_alertQueue = xQueueCreate( AIS_APP_MAX_ALERT_COUNT, AIS_MAX_ALERT_TOKEN_LEN_BYTES );

    vTaskSuspend(NULL);

    // Provide the audio_processing_task module with APP Task handle and AIS Task Handle
    audio_processing_set_app_task_handle(&appTaskHandle);
    audio_processing_set_ais_task_handle(&(aisHandle.mqttTask));

    int16_t *micBuf = pdm_to_pcm_get_pcm_output();
    audio_processing_set_mic_input_buffer(&micBuf);

    int16_t *ampBuf = pdm_to_pcm_get_amp_output();
    audio_processing_set_amp_input_buffer(&ampBuf);

    // Create audio processing task
    if (xTaskCreate(audio_processing_task, "Audio_processing_task", 1536U, NULL, audio_processing_task_PRIORITY, &xAudioProcessingTaskHandle) != pdPASS)
    {
        PRINTF("Task creation failed!.\r\n");
        while (1);
    }

    audio_processing_set_task_handle(&xAudioProcessingTaskHandle);

    pdm_to_pcm_set_audio_proces_task_handle(&xAudioProcessingTaskHandle);

    // Get loopback buffer address from AMP
    uint8_t *ampLoopBackBuf = SLN_AMP_GetLoopBackBuffer();
    pdm_to_pcm_set_amp_loopback_buffer(&ampLoopBackBuf);

    // Set loopback event bit for AMP
    EventBits_t loopBackEvent = pdm_to_pcm_get_amp_loopback_event();
    SLN_AMP_SetLoopBackEventBits(loopBackEvent);

    // Create pdm to pcm task
    if (xTaskCreate(pdm_to_pcm_task, "pdm_to_pcm_task", 512U, NULL, pdm_to_pcm_task_PRIORITY, &xPdmToPcmTaskHandle) != pdPASS)
    {
        PRINTF("Task creation failed!.\r\n");
        while (1);
    }

    pdm_to_pcm_set_task_handle(&xPdmToPcmTaskHandle);

    // Pass loopback event group to AMP
    EventGroupHandle_t ampLoopBackEventGroup = NULL;
    while(NULL == ampLoopBackEventGroup)
    {
        ampLoopBackEventGroup = pdm_to_pcm_get_event_group();
        vTaskDelay(10);
    }
    SLN_AMP_SetLoopBackEventGroup(&ampLoopBackEventGroup);

    SLN_AMP_WriteDefault();

    uint32_t taskNotification = 0;

    micOpen.asr_profile = AIS_ASR_FAR_FIELD;
    micOpen.initiator = AIS_INITIATOR_WAKEWORD;
    /* Initiator opaque type/token and wakeword indices not used for TAP. */
    micOpen.init_type = NULL;
    micOpen.init_token = NULL;
    micOpen.wwStart = 0;
    micOpen.wwEnd = 0;

    configPRINTF(("[App Task] Starting Available Heap: %d\r\n", xPortGetFreeHeapSize()));

    /* Give alerts the handlefor UX system*/
    AIS_Alerts_SetUxHandle(&xUXAttentionTaskHandle);
    AIS_Alerts_SetUxTimerBit(uxTimer);
    AIS_Alerts_SetUxAlarmBit(uxAlarm);
    AIS_Alerts_SetUxReminderBit(uxReminder);

    // Wait for a valid time
    while(0 == appData.currTime)
    {
        vTaskDelay(10);
    }

    // Re-sync alerts with service
    int32_t updateStatus = -1;

    updateStatus = AIS_Alerts_UpdateState(appData.currTime);

    if (1 == updateStatus)
    {
        /* Gather any alerts requires for deletion */
        alertTokenList_t deleteList;
        uint32_t deleteCount = 0;
        AIS_Alerts_GetDeletedList(&deleteList, &deleteCount);

        /* Send SynchronizeState to update on our status. */
        char *alertTokens[AIS_APP_MAX_ALERT_COUNT] = {0};

        for (uint32_t idx = 0; idx < AIS_APP_MAX_ALERT_COUNT; idx++)
        {
            alertTokens[idx] = (char *)(&deleteList[idx]);
        }

        AIS_EventSynchronizeState(&aisHandle, appData.volume, (const char **)alertTokens, deleteCount);
    }

    ux_attention_set_state(uxIdle);

    while(1)
    {

        xTaskNotifyWait( ULONG_MAX, ULONG_MAX, &taskNotification, portMAX_DELAY );

        if (kIdle & taskNotification)
        {
            // Set UX attention state
            ux_attention_set_state(uxIdle);

            taskNotification &= ~kIdle;
        }

        if (kWakeWordDetected & taskNotification)
        {
            if (reconnection_task_get_state() == kStartState)
            {
                streamer_handle_t *streamer = (streamer_handle_t*) aisHandle.audioPlayer;
                // Set UX attention state
                ux_attention_set_state(uxListeningStart);

                if (AIS_CheckState(&aisHandle, AIS_TASK_STATE_SPEAKER))
                {
                    STREAMER_MutexLock();

                    if (STREAMER_IsPlaying(streamer))
                    {
                        appData.speakerEOS = false;
                        configPRINTF(("[AIS App] Stopping streamer playback %d\r\n", u32MallocCount));
                        STREAMER_Stop(streamer);
                    }

                    appData.speakerOffsetStart = appData.speakerOffsetWritten;
                    appData.speakerOffsetEnd = appData.speakerOffsetWritten;

					appData.speakerOpen = false;
                    AIS_EventSpeakerClosed(&aisHandle, appData.speakerOffsetWritten);

                    STREAMER_MutexUnlock();

                    /* Need to make sure there is a 50ms gap between close speaker and mic open for spec compliance */
                    vTaskDelay(portTICK_PERIOD_MS * 50);
                }

                // Begin sending speech
                micOpen.wwStart = aisHandle.micStream.audio.offset + 16000;
                micOpen.wwEnd = aisHandle.micStream.audio.offset + audio_processing_get_wake_word_end();
                micOpen.initiator = AIS_INITIATOR_WAKEWORD;
                AIS_EventMicrophoneOpened(&aisHandle, &micOpen);

                // We are now recoding
                audio_processing_set_state(kWakeWordDetected);
            }
            else
            {
                ux_attention_set_state(uxDisconnected);
                audio_processing_set_state(kReconnect);
            }


            taskNotification &= ~kWakeWordDetected;
        }

        if (kMicKeepOpen & taskNotification)
        {
            if (reconnection_task_get_state() == kStartState)
            {
                // Set UX attention state
                ux_attention_set_state(uxListeningStart);

                // Being Sending speech
                micOpen.initiator = AIS_INITIATOR_TAP;
                AIS_EventMicrophoneOpened(&aisHandle, &micOpen);
            }
            else
            {
                ux_attention_set_state(uxDisconnected);
                audio_processing_set_state(kReconnect);
            }

            taskNotification &= ~kMicKeepOpen;
        }

        if (kMicRecording & taskNotification)
        {
            // Set UX attention state
            ux_attention_set_state(uxListeningActive);

            taskNotification &= ~kMicRecording;
        }

        if (kMicStop & taskNotification)
        {
            // Set UX attention state
            ux_attention_set_state(uxListeningEnd);
            taskNotification &= ~kMicStop;
        }

        if (kNewAlertSet & taskNotification)
        {
            char token[AIS_MAX_ALERT_TOKEN_LEN_BYTES] = {0};
            if( xQueueReceive( g_alertQueue, token, ( TickType_t ) 5 ) )
            {
                AIS_EventSetAlertSucceeded(&aisHandle, token);
            }

            taskNotification &= ~kNewAlertSet;
        }

        if (kNewAlertFail & taskNotification)
        {
            char token[AIS_MAX_ALERT_TOKEN_LEN_BYTES] = {0};
            if( xQueueReceive( g_alertQueue, token, ( TickType_t ) 5 ) )
            {
                AIS_EventSetAlertFailed(&aisHandle, token);
            }

            taskNotification &= ~kNewAlertFail;
        }

        if (kAlertDelete & taskNotification)
        {
            char token[AIS_MAX_ALERT_TOKEN_LEN_BYTES] = {0};
            if( xQueueReceive( g_alertQueue, token, ( TickType_t ) 5 ) )
            {
                AIS_EventDeleteAlertSucceeded(&aisHandle, token);
            }

            taskNotification &= ~kAlertDelete;
        }

        if (kFailDelete & taskNotification)
        {
            char token[AIS_MAX_ALERT_TOKEN_LEN_BYTES] = {0};
            if( xQueueReceive( g_alertQueue, token, ( TickType_t ) 5 ) )
            {
                AIS_EventDeleteAlertFailed(&aisHandle, token);
            }

            taskNotification &= ~kFailDelete;
        }

        if (kAlertOnlineTrigger & taskNotification)
        {
            char token[AIS_MAX_ALERT_TOKEN_LEN_BYTES] = {0};
            if( xQueueReceive( g_alertQueue, token, ( TickType_t ) 5 ) )
            {
                AIS_EventSetAlertSucceeded(&aisHandle, token);
            }

            taskNotification &= ~kAlertOnlineTrigger;
        }

        if (kSyncClock & taskNotification)
        {
            /* reset directive received check timer */
            appData.syncClockReceived = false;
            if (xTimerReset(aisClockSyncCheckHandle, 0) != pdPASS)
            {
                configPRINTF(("xTimerReset failed\r\n"));
            }

            AIS_EventSynchronizeClock(&aisHandle);

            taskNotification &= ~kSyncClock;
        }

        taskNotification = 0;
    }

    configPRINTF(("Error occurred: exiting\r\n"));
    configPRINTF(("Exiting demo\r\n"));
    vTaskDelete(NULL);
}

void button_callback(int button_nr, int state)
{
    if(button_nr == 1)
    {
        if(state == PRESSED)
        {
            xEventGroupSetBitsFromISR(g_button_pressed, BIT_PRESS_1, &xHigherPriorityTaskWoken);
        }
        else
        {
            xEventGroupSetBitsFromISR(g_button_pressed, BIT_RELEASE_1, &xHigherPriorityTaskWoken);
        }
    }
    else if (button_nr == 2)
    {
        if(state == PRESSED)
        {
            xEventGroupSetBitsFromISR(g_button_pressed, BIT_PRESS_2, &xHigherPriorityTaskWoken);
        }
        else
        {
            xEventGroupSetBitsFromISR(g_button_pressed, BIT_RELEASE_2, &xHigherPriorityTaskWoken);
        }
    }
}

void button_task(void *arg)
{
    EventBits_t bits;

    volatile uint32_t sec_1 = 0;
    OsaTimeval pressed_time_1 = {0};
    OsaTimeval released_time_1 = {0};

    volatile uint32_t sec_2 = 0;
    OsaTimeval pressed_time_2 = {0};
    OsaTimeval released_time_2 = {0};

    while(1)
    {
        bits = xEventGroupWaitBits(g_button_pressed, BIT_PRESS_1 | BIT_RELEASE_1 | BIT_PRESS_2 | BIT_RELEASE_2  , pdTRUE, pdFALSE, portMAX_DELAY);

        if( bits & BIT_PRESS_1 )
        {
            osa_time_get(&pressed_time_1);
        }

        if( bits & BIT_RELEASE_1 )
        {
            osa_time_get(&released_time_1);
            sec_1 = (released_time_1.tv_usec - pressed_time_1.tv_usec) / 1000000;

            if(sec_1 >= 10)
            {
                configPRINTF(("Starting factory reset ...\r\n"));

                RGB_LED_SetColor(LED_COLOR_ORANGE);

                /* Factory reset stuff */
                SLN_FLASH_MGMT_Erase(AIS_SECRET_KEY_FILE_NAME);
                SLN_FLASH_MGMT_Erase(AIS_ALERT_FILE_NAME);
#if USE_WIFI_CONNECTION
                SLN_FLASH_MGMT_Erase(WIFI_CRED_FILE_NAME);
#endif

                /* a bit of delay to let everyone admire the beautiful orange LED */
                osa_time_delay(2000);

                NVIC_SystemReset();
            }
            if((sec_1 <= 3) && (sec_1 >= 1))
            {
                if (audio_processing_get_mute())
                {
                    configPRINTF(("Finish mute mode ...\r\n"));
                    audio_processing_unmute();
                    /* Set the UX state to the current one */
                    ux_attention_resume_state(appData.state);
                }
                else
                {
                    configPRINTF(("Switch to mute mode ...\r\n"));
                    audio_processing_mute();
                    ux_attention_set_state(uxMicOntoOff);
                }
            }
            /* Perform a wifi credentials reset if SW_1 is pressed between 5 to 10 secs */
            else if((sec_1 >= 5) && (sec_1 < 10))
            {
                configPRINTF(("Reset the wifi credentials and the board\r\n"));
                wifi_credentials_flash_reset();
                vTaskDelay(portTICK_PERIOD_MS * 500);
                NVIC_SystemReset();
            }
        }

        if( bits & BIT_PRESS_2 )
        {
            osa_time_get(&pressed_time_2);
        }

        if( bits & BIT_RELEASE_2 )
        {
            osa_time_get(&released_time_2);
            sec_2 = (released_time_2.tv_usec - pressed_time_2.tv_usec) / 1000000;

            if (sec_2 < 3)
            {

                // Make sure we are offline
                reconnectState_t currState = reconnection_task_get_state();

                if ((kStartState != currState) && (kLinkUp != currState))
                {
                    int32_t ret = 0;

                    // Mark all that are not idle as deleted
                    for (uint32_t idx = 0; idx < AIS_APP_MAX_ALERT_COUNT; idx++)
                    {
                        if (false == AIS_Alerts_GetIdleState(idx))
                        {
                            ret = AIS_Alerts_MarkForDeleteOffline(idx);
                            if (0 != ret)
                            {
                                configPRINTF(("Unable to mark alert for delete: %d\r\n", ret));
                            }
                        }
                    }

                    ux_attention_states_t currUxState = ux_attention_get_state();

                    switch(currUxState)
                    {
                        case uxTimer:
                            ux_attention_set_state(uxTimerEnd);
                            break;
                        case uxAlarm:
                            ux_attention_set_state(uxAlarmEnd);
                            break;
                        case uxReminder:
                            ux_attention_set_state(uxReminderEnd);
                            break;
                        default:
                            break;
                    };

                    if (NULL != s_offlineAudioEventGroup)
                    {
                        xEventGroupSetBits(s_offlineAudioEventGroup, OFFLINE_AUDIO_ABORT);
                    }
                }
            }
        }

    }

    vTaskDelete(NULL);
}

void offline_audio_task(void *arg)
{
    // Play offline alert audio if needed

    EventBits_t offlineAudioEventBits;

    s_offlineAudioEventGroup = xEventGroupCreate();

    if( s_offlineAudioEventGroup != NULL )
    {
        while(1)
        {
            offlineAudioEventBits = xEventGroupWaitBits(s_offlineAudioEventGroup, OFFLINE_AUDIO_EVENT_MASK, pdTRUE, pdFALSE, portMAX_DELAY );

            // Choose what audio to play
            if (OFFLINE_AUDIO_TIMER & offlineAudioEventBits)
            {
                configPRINTF(("Starting offline timer audio %d...\r\n",appData.volume));
                SLN_AMP_SetVolume(appData.volume);
                SLN_AMP_WriteLoop((uint8_t *)med_system_alerts_melodic_02_mono_wav, MED_SYSTEM_ALERTS_MELODIC_02_MONO_WAV_LEN);
            }
            else if (OFFLINE_AUDIO_ALARM & offlineAudioEventBits)
            {
                configPRINTF(("Starting offline alarm audio %d...\r\n",appData.volume));
                SLN_AMP_SetVolume(appData.volume);
                SLN_AMP_WriteLoop((uint8_t *)med_system_alerts_melodic_01_mono_wav, MED_SYSTEM_ALERTS_MELODIC_01_MONO_WAV_LEN);
            }
            else if (OFFLINE_AUDIO_RMNDR & offlineAudioEventBits)
            {
                configPRINTF(("Starting offline reminder audio %d...\r\n",appData.volume));
                SLN_AMP_SetVolume(appData.volume);
                SLN_AMP_WriteLoop((uint8_t *)med_alerts_notification_01_mono_wav, MED_ALERTS_NOTIFICATION_01_MONO_WAV_LEN);
            }
            else if (OFFLINE_AUDIO_ABORT & offlineAudioEventBits)
            {
                configPRINTF(("...offline audio stopped %d!\r\n",appData.volume));
                SLN_AMP_SetVolume(0);
                SLN_AMP_AbortWrite();
            }
        }
    }
}

void main(void)
{
	status_t status = 0;
    /* Enable additional fault handlers */
    SCB->SHCSR |= (SCB_SHCSR_BUSFAULTENA_Msk | /*SCB_SHCSR_USGFAULTENA_Msk |*/ SCB_SHCSR_MEMFAULTENA_Msk);

    /* Init board hardware. */
    BOARD_ConfigMPU();
    BOARD_InitBootPins();
    BOARD_BootClockRUN();

    CRYPTO_InitHardware();

    /* Initialize Flash to allow writing */
    SLN_Flash_Init();

    /* Initialize flash management */
    SLN_FLASH_MGMT_Init((sln_flash_entry_t *)g_fileTable , false);

    // XXX: For Test
    //SLN_FLASH_MGMT_Erase(AIS_ALERT_FILE_NAME);

    /*
     * AUDIO PLL setting: Frequency = Fref * (DIV_SELECT + NUM / DENOM)
     *                              = 24 * (32 + 77/100)
     *                              = 786.48 MHz
     */
    const clock_audio_pll_config_t audioPllConfig = {
            .loopDivider = 32,  /* PLL loop divider. Valid range for DIV_SELECT divider value: 27~54. */
            .postDivider = 1,   /* Divider after the PLL, should only be 1, 2, 4, 8, 16. */
            .numerator = 77,    /* 30 bit numerator of fractional loop divider. */
            .denominator = 100, /* 30 bit denominator of fractional loop divider */
    };

    CLOCK_InitAudioPll(&audioPllConfig);

    CLOCK_SetMux(kCLOCK_Sai1Mux, BOARD_PDM_SAI_CLOCK_SOURCE_SELECT);
    CLOCK_SetDiv(kCLOCK_Sai1PreDiv, BOARD_PDM_SAI_CLOCK_SOURCE_PRE_DIVIDER);
    CLOCK_SetDiv(kCLOCK_Sai1Div, BOARD_PDM_SAI_CLOCK_SOURCE_DIVIDER);
    CLOCK_EnableClock(kCLOCK_Sai1);

    CLOCK_SetMux(kCLOCK_Sai2Mux, BOARD_PDM_SAI_CLOCK_SOURCE_SELECT);
    CLOCK_SetDiv(kCLOCK_Sai2PreDiv, BOARD_PDM_SAI_CLOCK_SOURCE_PRE_DIVIDER);
    CLOCK_SetDiv(kCLOCK_Sai2Div, BOARD_PDM_SAI_CLOCK_SOURCE_DIVIDER);
    CLOCK_EnableClock(kCLOCK_Sai2);

    edma_config_t dmaConfig = {0};

    /* Create EDMA handle */
    /*
     * dmaConfig.enableRoundRobinArbitration = false;
     * dmaConfig.enableHaltOnError = true;
     * dmaConfig.enableContinuousLinkMode = false;
     * dmaConfig.enableDebugMode = false;
     */
    EDMA_GetDefaultConfig(&dmaConfig);
    EDMA_Init(DMA0, &dmaConfig);

    DMAMUX_Init(DMAMUX);

    RGB_LED_Init();

    switchInit();

    sln_shell_init();

    xLoggingTaskInitialize(LOGGING_STACK_SIZE, tskIDLE_PRIORITY + 1, LOGGING_QUEUE_LENGTH);

#if TEST_PURPOSE //For testing purpose
    aws_user_store_credentials_into_flash();
    configPRINTF(("aws_user_store_credentials_into_flash done!!\r\n"));
#endif


    status =  aws_user_credentials_flash_get(&flash_aws_usr_cred);

   if (status)
   {
	   configPRINTF(("There is no aws_user_credentials details in flash\r\n"));
   }
   else{
	   configPRINTF(("MQTT_BROKER_ENDPOINT %s \r\n",flash_aws_usr_cred.mqtt_broker_endpoint.data));
	   configPRINTF(("ALEXA_TOPIC_ROOT %s \n",flash_aws_usr_cred.alexa_topic_root.data));
	   configPRINTF(("ALEXA_ACCOUNT_ID %s \n",flash_aws_usr_cred.alexa_account_id.value));
	   configPRINTF(("IOT_PRODUCT_NAME %s \n",flash_aws_usr_cred.iot_product_name.data));

   }


    xTaskCreate(appInit, "APP_Init", 1536, NULL, configMAX_PRIORITIES - 1, &appInitTaskHandle);

    xTaskCreate(appTask, "APP_Task", 1024, NULL, configTIMER_TASK_PRIORITY - 1, &appTaskHandle);

    xTaskCreate(sln_shell_task, "Shell_Task", 512, NULL, tskIDLE_PRIORITY + 1, NULL);

    xTaskCreate(button_task, "Button_Task", 256, NULL, configMAX_PRIORITIES - 1, NULL);

    xTaskCreate(ota_task, "OTA_Task", 384, NULL, tskIDLE_PRIORITY + 1, NULL);

    /* Run RTOS */
    vTaskStartScheduler();

    /* Exit */
    ux_attention_sys_fault();
    AIS_Deinit(&aisHandle);
    APP_MQTT_Disconnect(&mqttHandle);
    APP_NETWORK_Uninit();

    /* Should not reach this statement */
    while (1)
    {
    }
}
