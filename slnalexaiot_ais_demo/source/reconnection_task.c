/*
 * Copyright 2018 NXP.
 * This software is owned or controlled by NXP and may only be used strictly in accordance with the
 * license terms that accompany it. By expressly accepting such terms or by downloading, installing,
 * activating and/or otherwise using the software, you are agreeing that you have read, and that you
 * agree to comply with and are bound by, such license terms. If you do not agree to be bound by the
 * applicable license terms, then you may not retain, install, activate or otherwise use the software.
 */

#include "reconnection_task.h"

/* Board includes */
#include "board.h"
#include "pin_mux.h"

/* FreeRTOS kernel includes */
#include "FreeRTOS.h"
#include "task.h"

/* AIS includes */
#include "aisv2.h"
#include "aisv2_app.h"

/* UX includes */
#include "ais_alerts.h"
#include "ais_streamer.h"
#include "audio_processing_task.h"
#include "network_connection.h"
#include "uvoice_ux_attention_system.h"

/* OTA includes */
#include "aws_ota_check.h"

#define RECONNECTION_TASK_NAME      "reconnection_task"
#define RECONNECTION_TASK_STACK     1024U
#define RECONNECTION_TASK_PRIORITY  (configMAX_PRIORITIES - 3)
#define RECONNECTION_EVENT_MASK     (kReconnectInvalidSequence | kReconnectAPIDeprecated | kReconnectEncryptionError | kReconnectGoingOffline | kReconnectNetworkLoss)

// TODO: This is a bit of a hack to push the reconnection out of a waiting state (see aws_mqtt_agent.c for what these
// numbers are)
#define RECONNECTION_MQTT_TIMEOUT_MASK \
    (0xFFFF0000UL | 32 |               \
     0x00000001UL) // (mqttMESSAGE_IDENTIFIER_MASK | eMQTTOperationTimedOut | mqttNOTIFICATION_STATUS_MASK)

// TODO: Clean up this extern pile, it does not "spark joy"
extern ais_handle_t aisHandle;
extern ota_handle_t otaHandle;
extern ais_app_data_t appData;
extern MQTTAgentHandle_t mqttHandle;
extern BaseType_t APP_MQTT_Connect(MQTTAgentHandle_t *xMQTTHandle);
extern void APP_MQTT_Disconnect(MQTTAgentHandle_t *xMQTTHandle);
extern EventGroupHandle_t s_offlineAudioEventGroup;

static TaskHandle_t *s_thisTask = NULL;

static uint32_t s_reconnectState = kStartState;

static ais_disconnect_code_t s_aisDisconnectCode = AIS_DISCONNECT_GOING_OFFLINE;

static EventGroupHandle_t s_reconnectionEventGroup;

/* Disconnect timeout timer handle */
static TimerHandle_t s_disconnectTimeoutHandle;

/* Reconnect timeout timer handle */
static TimerHandle_t s_reconnectTimeoutHandle;

/* AIS Connection timeout timer handle */
static TimerHandle_t s_aisConnectionTimeoutHandle;

/* AIS Disconnection timeout timer handle */
static TimerHandle_t s_aisDisconnectionTimeoutHandle;

/* OTA Connection timeout timer handle */
static TimerHandle_t s_otaConnectionTimeoutHandle;

/* OTA Disconnection timeout timer handle */
static TimerHandle_t s_otaDisconnectionTimeoutHandle;

void ais_app_disconnect_timeout_cb(TimerHandle_t xTimer)
{
    /* Mitigation for MQTT Disconnect deadlock */
    if (s_reconnectState == kLinkLoss)
    {
        configPRINTF(("[ERROR] MQTT Disconnect timeout... resetting...\r\n"));
        NVIC_SystemReset(); /* Good Bye */
    }
}

void ais_app_reconnect_timeout_cb(TimerHandle_t xTimer)
{
    /* Mitigation for MQTT Disconnect deadlock */
    if (s_reconnectState == kMqttReconnect)
    {
        configPRINTF(("[ERROR] MQTT Connect timeout... resetting...\r\n"));
        NVIC_SystemReset(); /* Good Bye */
    }
}

void ais_app_ais_connect_timeout_cb(TimerHandle_t xTimer)
{
    /* Mitigation for AIS Connection deadlock */
    if (s_reconnectState == kAisReconnect)
    {
        configPRINTF(("[ERROR] AIS Connect timeout... resetting...\r\n"));
        NVIC_SystemReset(); /* Good Bye */
    }
}

void ais_app_ais_disconnect_timeout_cb(TimerHandle_t xTimer)
{
    /* Mitigation for AIS Disconnect deadlock */
    if (s_reconnectState == kAisDisconnect)
    {
        configPRINTF(("[ERROR] AIS Disconnect timeout... resetting...\r\n"));
        NVIC_SystemReset(); /* Good Bye */
    }
}

void ais_app_ota_connect_timeout_cb(TimerHandle_t xTimer)
{
    /* Mitigation for OTA Connect deadlock */
    if (s_reconnectState == kOtaReconnect)
    {
        configPRINTF(("[ERROR] OTA Connect timeout... resetting...\r\n"));
        NVIC_SystemReset(); /* Good Bye */
    }
}

void ais_app_ota_disconnect_timeout_cb(TimerHandle_t xTimer)
{
    /* Mitigation for OTA Disconnect deadlock */
    if (s_reconnectState == kOtaDisconnect)
    {
        configPRINTF(("[ERROR] OTA Disconnect timeout... resetting...\r\n"));
        NVIC_SystemReset(); /* Good Bye */
    }
}

static void reconnect_interaction_ux(void)
{
    ux_attention_states_t currUxState;

    currUxState = ux_attention_get_state();

    if (UX_INTERACTION_MASK(currUxState))
    {
        ux_attention_set_state(uxDisconnected);
    }
}

static void reconnect_state_machine(uint32_t *reconnectState)
{
    status_t connectStatus = kStatus_Fail;
    status_t disconnectStatus = kStatus_Fail;

    uint32_t attempts = 3;

    switch (*reconnectState)
    {
        case kStreamerStop:
            reconnect_interaction_ux();

            streamer_handle_t *streamer = (streamer_handle_t *)aisHandle.audioPlayer;

            /* Stop the streamer as we aren't going to get more data */
            configPRINTF(("Stopping streamer playback!\r\n"));
            STREAMER_Stop(streamer);

            *reconnectState = kAisDisconnect;

            break;

        case kLinkLoss:

            reconnect_interaction_ux();

            if (xTimerStart(s_disconnectTimeoutHandle, 0) != pdPASS)
            {
                configPRINTF(("xTimerStart failed\r\n"));
            }

            APP_MQTT_Disconnect(&mqttHandle);

            configPRINTF(("Disconnected from MQTT...\r\n"));

            *reconnectState = kMqttReconnect;
            break;

        case kMqttReconnect:

#if USE_WIFI_CONNECTION
            configPRINTF(("Waiting for link to re-establish...\r\n"));
            do
            {
                reconnect_interaction_ux();

                vTaskDelay(portTICK_PERIOD_MS * 1000);
            } while (WICED_FALSE == get_wifi_connect_state());
#endif

            configPRINTF(("Reconnecting to MQTT...\r\n"));

            vTaskDelay(portTICK_PERIOD_MS * 1000);

            if (xTimerStart(s_reconnectTimeoutHandle, 0) != pdPASS)
            {
                configPRINTF(("xTimerStart failed\r\n"));
            }

            BaseType_t ret = APP_MQTT_Connect(&mqttHandle);
            if (pdFAIL == ret)
            {
                *reconnectState = kSysReboot;
            }
            else if (2 == ret)
            {
                *reconnectState = kLinkLoss;
            }
            else
            {
                *reconnectState = kAisDisconnect;
            }

            break;

        case kAisDisconnect:

            if ((true == aisHandle.aisConnected) && (false == aisHandle.firstConnect))
            {
                vTaskDelay(portTICK_PERIOD_MS * 1000);

                if (xTimerStart(s_aisDisconnectionTimeoutHandle, 0) != pdPASS)
                {
                    configPRINTF(("xTimerStart failed\r\n"));
                }

                disconnectStatus = AIS_Disconnect(&aisHandle, s_aisDisconnectCode);
            }

            // Timeout will have changed our state, best not to ignore
            if (kAisDisconnect == *reconnectState)
            {
                if (kStatus_Success == disconnectStatus)
                {
                    *reconnectState = kAisReconnect;
                }
                else
                {
                    configPRINTF(("Fail to disconnect from AIS service, disconnecting from MQTT\r\n"));
                    *reconnectState = kLinkLoss;
                }
            }
            break;

        case kAisReconnect:

            connectStatus = kStatus_Fail;

            vTaskDelay(portTICK_PERIOD_MS * 1000);

            /* Connect to the service again */
            do
            {
                vTaskDelay(portTICK_PERIOD_MS * 1000);

                if (xTimerStart(s_aisConnectionTimeoutHandle, 0) != pdPASS)
                {
                    configPRINTF(("xTimerStart failed\r\n"));
                }

                connectStatus = AIS_Connect(&aisHandle);
                attempts--;
            } while ((attempts > 0) && (kStatus_Success != connectStatus));

            if (kStatus_Success == connectStatus)
            {
                /* Stop any offline audio */
                if (NULL != s_offlineAudioEventGroup)
                {
                    xEventGroupSetBits(s_offlineAudioEventGroup, OFFLINE_AUDIO_ABORT);
                }

                /* Gather any alerts requires for deletion */
                alertTokenList_t deleteList;
                uint32_t deleteCount = 0;
                AIS_Alerts_GetDeletedList(&deleteList, &deleteCount);
                configPRINTF(("Found %d alerts ready to delete.\r\n", deleteCount));

                /* Send SynchronizeState to update on our status. */
                char *alertTokens[AIS_APP_MAX_ALERT_COUNT] = {0};
                for (uint32_t idx = 0; idx < deleteCount; idx++)
                {
                    alertTokens[idx] = (char *)(&deleteList[idx]);
                    configPRINTF(("Alert ready for delete: %s\r\n", alertTokens[idx]));
                }

                /* Send synchronize event */
                connectStatus =
                    AIS_EventSynchronizeState(&aisHandle, appData.volume, (const char **)alertTokens, deleteCount);

                if (kStatus_Success != connectStatus)
                {
                    configPRINTF(("[Reconnect] Error sending SynchronizeState: %d\r\n", connectStatus));
                }

                /* Set the ux_state to idle to clear the state set before the disconnect */
                ux_attention_set_state(uxIdle);

                *reconnectState = kOtaDisconnect;
            }
            else
            {
                *reconnectState = kLinkLoss;
            }
            break;

        case kOtaDisconnect:
            configPRINTF(("Disconnect from OTA service\r\n"));

            if (xTimerStart(s_otaDisconnectionTimeoutHandle, 0) != pdPASS)
            {
                configPRINTF(("xTimerStart failed\r\n"));
            }
            disconnectStatus = OTA_Disconnect( &otaHandle );

            if (disconnectStatus == kStatus_Success)
            {
                *reconnectState = kOtaReconnect;
            }
            else
            {
                *reconnectState = kLinkLoss;
                configPRINTF(("Fail to disconnect from OTA service\r\n"));
            }
            break;

        case kOtaReconnect:
            do
            {
                configPRINTF(("Reconnect to OTA service, countdown = %d\r\n", attempts));

                if (xTimerStart(s_otaConnectionTimeoutHandle, 0) != pdPASS)
                {
                    configPRINTF(("xTimerStart failed\r\n"));
                }
                connectStatus = OTA_Connect(&otaHandle);

                if (connectStatus == kStatus_Success)
                {
                    *reconnectState = kLinkUp;
                }
                else
                {
                    attempts--;
                    if (attempts == 0)
                    {
                        *reconnectState = kLinkLoss;
                        configPRINTF(("Fail to reconnect to OTA service, countdown = %d, move on...\r\n", attempts));
                    }
                }
            }
            while ((attempts > 0) && (connectStatus != kStatus_Success));
            break;

        case kLinkUp:
            ux_attention_set_state(uxDeviceChange);
            audio_processing_set_state(kIdle);
            if (!audio_processing_get_mute())
            {
                ux_attention_set_state(uxIdle);
            }
            else
            {
                ux_attention_set_state(uxMicOntoOff);
            }
            *reconnectState = kStartState;

            // Stop all the timeouts (we don't need them anymore)
            if (xTimerStop(s_disconnectTimeoutHandle, 0) != pdPASS)
            {
                configPRINTF(("xTimerStop failed\r\n"));
            }

            if (xTimerStop(s_reconnectTimeoutHandle, 0) != pdPASS)
            {
                configPRINTF(("xTimerStop failed\r\n"));
            }

            if (xTimerStop(s_aisConnectionTimeoutHandle, 0) != pdPASS)
            {
                configPRINTF(("xTimerStop failed\r\n"));
            }

            if (xTimerStop(s_aisDisconnectionTimeoutHandle, 0) != pdPASS)
            {
                configPRINTF(("xTimerStop failed\r\n"));
            }

            if (xTimerStop(s_otaConnectionTimeoutHandle, 0) != pdPASS)
            {
                configPRINTF(("xTimerStop failed\r\n"));
            }

            if (xTimerStop(s_otaDisconnectionTimeoutHandle, 0) != pdPASS)
            {
                configPRINTF(("xTimerStop failed\r\n"));
            }

            break;

        case kSysReboot:

            ux_attention_sys_fault();

            vTaskDelay(portTICK_PERIOD_MS * 10000);

            NVIC_SystemReset(); /* Something has gone wrong */
            break;

        default:
            break;
    }
}

void reconnectionTask(void *arg)
{
    EventBits_t reconnectionEventBits;

    s_reconnectionEventGroup = xEventGroupCreate();

    if (s_reconnectionEventGroup != NULL)
    {
        // Stop all the timeouts (we don't need them yet)
        if (xTimerStop(s_disconnectTimeoutHandle, 0) != pdPASS)
        {
            configPRINTF(("xTimerStop failed\r\n"));
        }

        if (xTimerStop(s_reconnectTimeoutHandle, 0) != pdPASS)
        {
            configPRINTF(("xTimerStop failed\r\n"));
        }

        if (xTimerStop(s_aisConnectionTimeoutHandle, 0) != pdPASS)
        {
            configPRINTF(("xTimerStop failed\r\n"));
        }

        if (xTimerStop(s_aisDisconnectionTimeoutHandle, 0) != pdPASS)
        {
            configPRINTF(("xTimerStop failed\r\n"));
        }

        if (xTimerStop(s_otaConnectionTimeoutHandle, 0) != pdPASS)
        {
            configPRINTF(("xTimerStop failed\r\n"));
        }

        if (xTimerStop(s_otaDisconnectionTimeoutHandle, 0) != pdPASS)
        {
            configPRINTF(("xTimerStop failed\r\n"));
        }

        while (1)
        {
            reconnectionEventBits =
                xEventGroupWaitBits(s_reconnectionEventGroup, RECONNECTION_EVENT_MASK, pdTRUE, pdFALSE, portMAX_DELAY);

            if (kReconnectInvalidSequence & reconnectionEventBits)
            {
                /* Set reconnect state machine entry state */
                s_reconnectState = kAisDisconnect;

                /* Set code for disconnect publish message */
                s_aisDisconnectCode = AIS_DISCONNECT_INVALID_SEQUENCE;
            }
            else if (kReconnectAPIDeprecated & reconnectionEventBits)
            {
                /* Set reconnect state machine entry state */
                s_reconnectState = kAisDisconnect;

                /* Set code for disconnect publish message */
                s_aisDisconnectCode = AIS_DISCONNECT_API_DEPRECATED;
            }
            else if (kReconnectEncryptionError & reconnectionEventBits)
            {
                /* Set reconnect state machine entry state */
                s_reconnectState = kAisDisconnect;

                /* Set code for disconnect publish message */
                s_aisDisconnectCode = AIS_DISCONNECT_ENCRYPTION_ERROR;
            }
            else if (kReconnectGoingOffline & reconnectionEventBits)
            {
                /* Set reconnect state machine entry state */
                s_reconnectState = kStreamerStop;

                /* Set code for disconnect publish message */
                s_aisDisconnectCode = AIS_DISCONNECT_GOING_OFFLINE;
            }
            else if (kReconnectNetworkLoss & reconnectionEventBits)
            {
                /* Set reconnect state machine entry state */
                s_reconnectState = kLinkLoss;

                /* Set code for disconnect publish message */
                s_aisDisconnectCode = AIS_DISCONNECT_GOING_OFFLINE;
            }

            /* Run reconnect state machine to regain connection */
            while ((kStartState != s_reconnectState))
            {
                reconnect_state_machine(&s_reconnectState);
            }
        }
    }
}

int32_t reconnection_task_set_event(reconnectEvent_t event)
{
    if (NULL != s_reconnectionEventGroup)
    {
        xEventGroupSetBits(s_reconnectionEventGroup, event);
    }

    // TODO: Handle the return code
    return 0;
}

reconnectState_t reconnection_task_get_state(void)
{
    return s_reconnectState;
}

int32_t reconnection_task_init(TaskHandle_t *handle)
{
    s_disconnectTimeoutHandle = xTimerCreate("AIS_MQTT_Timeout", AIS_APP_MQTT_DISCONNECT_TIMEOUT_MSEC, pdFALSE,
                                             (void *)0, ais_app_disconnect_timeout_cb);

    if (NULL == s_disconnectTimeoutHandle)
    {
        return 1;
    }

    s_reconnectTimeoutHandle = xTimerCreate("AIS_MQTT_Timeout2", AIS_APP_MQTT_RECONNECT_TIMEOUT_MSEC, pdFALSE,
                                            (void *)0, ais_app_reconnect_timeout_cb);

    if (NULL == s_reconnectTimeoutHandle)
    {
        return 2;
    }

    s_aisConnectionTimeoutHandle = xTimerCreate("AIS_MQTT_Timeout3", AIS_APP_MQTT_AIS_CONNECT_TIMEOUT_MSEC, pdFALSE,
                                                (void *)0, ais_app_ais_connect_timeout_cb);

    if (NULL == s_aisConnectionTimeoutHandle)
    {
        return 3;
    }

    s_aisDisconnectionTimeoutHandle = xTimerCreate("AIS_MQTT_Timeout4", AIS_APP_MQTT_DISCONNECT_TIMEOUT_MSEC, pdFALSE,
                                                   (void *)0, ais_app_ais_disconnect_timeout_cb);

    if (NULL == s_aisDisconnectionTimeoutHandle)
    {
        return 4;
    }

    s_otaConnectionTimeoutHandle = xTimerCreate( "AIS_MQTT_Timeout5", AIS_APP_MQTT_OTA_CONNECT_TIMEOUT_MSEC, pdFALSE, (void *)0, ais_app_ota_connect_timeout_cb );

    if (NULL == s_otaConnectionTimeoutHandle)
    {
        return 5;
    }

    s_otaDisconnectionTimeoutHandle = xTimerCreate( "AIS_MQTT_Timeout6", AIS_APP_MQTT_OTA_DISCONNECT_TIMEOUT_MSEC, pdFALSE, (void *)0, ais_app_ota_disconnect_timeout_cb );

    if (NULL == s_otaDisconnectionTimeoutHandle)
    {
        return 6;
    }

    if (pdPASS != xTaskCreate(reconnectionTask, RECONNECTION_TASK_NAME, RECONNECTION_TASK_STACK, NULL,
                              RECONNECTION_TASK_PRIORITY, (void *)handle))
    {
        configPRINTF(("[RECONN TASK] Task Creation Failed\r\n"));
        return 7;
    }
    else
    {
        s_thisTask = handle;
    }

    return 0;
}
