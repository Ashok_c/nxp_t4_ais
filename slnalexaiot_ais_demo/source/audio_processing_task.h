/*
 * Copyright 2018 NXP.
 * This software is owned or controlled by NXP and may only be used strictly in accordance with the
 * license terms that accompany it. By expressly accepting such terms or by downloading, installing,
 * activating and/or otherwise using the software, you are agreeing that you have read, and that you
 * agree to comply with and are bound by, such license terms. If you do not agree to be bound by the
 * applicable license terms, then you may not retain, install, activate or otherwise use the software.
 */

#ifndef _AUDIO_PROCESSING_TASK_H_
#define _AUDIO_PROCESSING_TASK_H_

#include <stdint.h>
#include "FreeRTOS.h"
#include "task.h"

/*!
 * @addtogroup
 * @{
 */

/*******************************************************************************
 * Definitions
 ******************************************************************************/

typedef enum __audio_processing_states
{
    kIdle = (1U << 0U),
    kWakeWordDetected = (1U << 1U),
    kMicRecording = (1U << 2U),
    kMicStopRecording = (1U << 3U),
    kMicKeepOpen = (1U << 4U),
    kMicStop = (1U << 5U),
    kMicCloudWakeVerifier = (1U << 6U),
    kReconnect = (1U << 7U)
} audio_processing_states_t;

#define AUDIO_PROCESSING_NOT_LISTENING_MASK (kIdle | kReconnect | kMicStopRecording | kMicStop)
#define AUDIO_PROCESSING_IS_LISTENING_MASK (kWakeWordDetected | kMicRecording | kMicKeepOpen | kMicCloudWakeVerifier)

/*******************************************************************************
 * API
 ******************************************************************************/
#if defined(__cplusplus)
extern "C"
{
#endif

    /*!
     * @brief
     */
    void audio_processing_task(void *pvParameters);

    /*!
     * @brief
     */
    void audio_processing_set_mic_input_buffer(int16_t **buf);

    /*!
     * @brief
     */
    void audio_processing_set_amp_input_buffer(int16_t **buf);

    /*!
     * @brief
     */
    uint32_t audio_processing_get_continuous_utterance(
        uint8_t **outBuf, uint32_t *outLen, uint32_t *index, uint32_t *size, uint8_t **data);

    /*!
     * @brief
     */
    uint32_t audio_processing_get_output_buffer(uint8_t **outBuf, uint32_t *outLen);

    /*!
     * @brief
     */
    void audio_processing_set_task_handle(TaskHandle_t *handle);

    /*!
     * @brief
     */
    TaskHandle_t audio_processing_get_task_handle(void);

    /*!
     * @brief
     */
    void audio_processing_set_app_task_handle(TaskHandle_t *handle);

    /*!
     * @brief
     */
    void audio_processing_set_ais_task_handle(TaskHandle_t *handle);

    /*!
     * @brief
     */
    void audio_processing_set_state(audio_processing_states_t state);

    /*!
     * @brief
     */
    audio_processing_states_t audio_processing_get_state(void);

    /*!
     * @brief
     */
    uint32_t audio_processing_get_wake_word_end(void);

    /*!
     * @brief Gets the mute mode
     * @returns mute mode (0 / 1)
     */
    uint8_t audio_processing_get_mute(void);

    /*!
     * @brief Sets the mute mode
     * @returns Void
     */
    void audio_processing_mute(void);

    /*!
     * @brief Sets unmute mode
     * @returns Void
     */
    void audio_processing_unmute(void);

#if defined(__cplusplus)
}
#endif

/*! @} */

#endif /* _AUDIO_PROCESSING_TASK_H_ */

/*******************************************************************************
 * API
 ******************************************************************************/
