/*
 * Copyright 2018 NXP.
 * This software is owned or controlled by NXP and may only be used strictly in accordance with the
 * license terms that accompany it. By expressly accepting such terms or by downloading, installing,
 * activating and/or otherwise using the software, you are agreeing that you have read, and that you
 * agree to comply with and are bound by, such license terms. If you do not agree to be bound by the
 * applicable license terms, then you may not retain, install, activate or otherwise use the software.
 */

#ifndef _SLN_RT10XX_LED_H_
#define _SLN_RT10XX_LED_H_

#include "fsl_common.h"

#define BLINK_RATE 50

/*! @brief LED Brightness */
typedef enum {
    LED_BRIGHT_OFF    = 0,      /*!< LED Off */
    LED_BRIGHT_LOW    = 1,      /*!< LED Low Brightness */
    LED_BRIGHT_MEDIUM = 2,      /*!< LED Medium Brightness */
    LED_BRIGHT_HIGH   = 3       /*!< LED High Brightness */
} rgb_led_brightness_t;

typedef enum _rgb_led_color
{
    LED_COLOR_RED,
    LED_COLOR_ORANGE,
    LED_COLOR_YELLOW,
    LED_COLOR_GREEN,
    LED_COLOR_BLUE,
    LED_COLOR_PURPLE,
    LED_COLOR_CYAN,
    LED_COLOR_WHITE,
    LED_COLOR_OFF,
}
rgbLedColor_t; 

extern void RGB_LED_SetColor(rgbLedColor_t color);
extern void RGB_LED_SetBrightnessColor(rgb_led_brightness_t brightness, rgbLedColor_t color);
extern void RGB_LED_Blink(uint8_t brightness, uint8_t color, uint32_t blinkrate, uint32_t *blinkcount, bool *blinktoggle);
extern void RapidBlinkLED();
extern status_t RGB_LED_Init(void);

#endif


