################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../amazon-freertos/third_party/jsmn/jsmn.c 

OBJS += \
./amazon-freertos/third_party/jsmn/jsmn.o 

C_DEPS += \
./amazon-freertos/third_party/jsmn/jsmn.d 


# Each subdirectory must supply rules for building sources it contributes
amazon-freertos/third_party/jsmn/%.o: ../amazon-freertos/third_party/jsmn/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -std=gnu99 -DSDK_DEBUGCONSOLE=1 -DSCANF_FLOAT_ENABLE=0 -DPRINTF_ADVANCED_ENABLE=0 -DSCANF_ADVANCED_ENABLE=0 -DXIP_EXTERNAL_FLASH=1 -DXIP_BOOT_HEADER_ENABLE=1 -DXIP_BOOT_HEADER_DCD_ENABLE=1 -D__SEMIHOST_HARDFAULT_DISABLE=1 -DDEBUG_CONSOLE_TRANSFER_NON_BLOCKING=1 -DUSB_STACK_FREERTOS_HEAP_SIZE=65536 -DUSB_STACK_FREERTOS -DARM_MATH_CM7 -DUSE_RTOS=1 -DFSL_RTOS_FREE_RTOS -DLWIP_DNS=1 -DLWIP_DHCP=1 -DMBEDTLS_CONFIG_FILE='"aws_mbedtls_config.h"' -DCPU_MIMXRT106ADVL6A -DCPU_MIMXRT106ADVL6A_cm7 -DPRINTF_FLOAT_ENABLE=0 -DCR_INTEGER_PRINTF -D__MCUXPRESSO -D__USE_CMSIS -DDEBUG -D__REDLIB__ -I../board -I../source -I../ -I../drivers -I../utilities -I../component/serial_manager -I../device -I../CMSIS -I../amazon-freertos/include -I../amazon-freertos/FreeRTOS/portable -I../lwip/port/arch -I../lwip/src/include/compat/posix -I../lwip/src/include/compat/posix/arpa -I../lwip/src/include/compat/posix/net -I../lwip/src/include/compat/posix/sys -I../lwip/src/include/compat/stdc -I../lwip/src/include/lwip -I../lwip/src/include/lwip/priv -I../lwip/src/include/lwip/prot -I../lwip/src/include/netif -I../lwip/src/include/netif/ppp -I../lwip/src/include/netif/ppp/polarssl -I../wifi_wwd/FreeRTOS -I../wifi_wwd/network -I../wifi_wwd/platform -I../wifi_wwd/WWD/include/network -I../wifi_wwd/WWD/include/platform -I../wifi_wwd/WWD/include/RTOS -I../wifi_wwd/WWD/include -I../wifi_wwd/WWD/internal/bus_protocols/SDIO -I../wifi_wwd/WWD/internal/bus_protocols -I../wifi_wwd/WWD/internal/chips -I../wifi_wwd/WWD/internal -I../wifi_wwd/WWD -I../wifi_wwd -I../sdmmc/inc -I../sdmmc/port -I../amazon-freertos/tinycbor -I../amazon-freertos/third_party/pkcs11 -I../mbedtls/include/mbedtls -I../mbedtls/port/ksdk -I../amazon-freertos/third_party/jsmn -I../component/uart -I../component/lists -I../xip -I../source/bl_drv -I../osa -I../usb/device/class/cdc -I../usb/device/class/msc -I../usb/device/class -I../usb/device/include -I../usb/device/source/ehci -I../usb/device/source -I../usb/include -I../usb/phy -I../lwip/port -I../lwip/src -I../lwip/src/include -I../amazon-freertos/third_party -I../mbedtls/include -O0 -fno-common -g -Wall -c  -ffunction-sections  -fdata-sections  -ffreestanding  -fno-builtin  -fomit-frame-pointer -mcpu=cortex-m7 -mfpu=fpv5-d16 -mfloat-abi=hard -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


