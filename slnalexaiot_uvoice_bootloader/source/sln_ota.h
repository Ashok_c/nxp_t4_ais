/*
 * Copyright 2019 NXP.
 * This software is owned or controlled by NXP and may only be used strictly in accordance with the
 * license terms that accompany it. By expressly accepting such terms or by downloading, installing,
 * activating and/or otherwise using the software, you are agreeing that you have read, and that you
 * agree to comply with and are bound by, such license terms. If you do not agree to be bound by the
 * applicable license terms, then you may not retain, install, activate or otherwise use the software.
 */

#ifndef SLN_OTA_H_
#define SLN_OTA_H_

/* Board includes */
#include "board.h"
#include "pin_mux.h"
#include "fsl_debug_console.h"

/* FreeRTOS kernel includes */
#include "FreeRTOS.h"
#include "task.h"

/* Crypto includes */
#include "ksdk_mbedtls.h"
#include "base64.h"

/* AWS includes */
#include "aws_system_init.h"
#include "aws_logging_task.h"
#include "aws_clientcredential.h"
#include "aws_mqtt_agent.h"
#include "aws_ota_agent.h"
#include "aws_application_version.h"

/* USB includes */
#include "serial_manager.h"
#include "usb_device_board_config.h"
#include "usb_phy.h"
#include "usb.h"

#include "network_connection.h"
#include "sln_flash_mgmt.h"
#include "wifi_credentials.h"
#include "flash_ica_driver.h"
#include "sln_flash.h"
#include "aws_ota_pal.h"
#include "bootloader.h"
#include "limits.h"

typedef enum __ota_states
{
    kSelfTestPassed   = 	(1U << 0U),
	kOtaFailed        = 	(1U << 1U)
} ota_state_t;

void otaAppInitTask(void *arg);
void ota_pal_ota_done_task_set(TaskHandle_t handle);

#endif /* SLN_OTA_H_ */
