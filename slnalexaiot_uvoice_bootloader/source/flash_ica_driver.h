/*
 * Copyright 2019 NXP
 * All rights reserved.
 *
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef _FICA_DRIVER_H_
#define _FICA_DRIVER_H_


//! @addtogroup flash_ica
//! @{

////////////////////////////////////////////////////////////////////////////////
// Definitions
////////////////////////////////////////////////////////////////////////////////

// #ifdef PRODUCTION_VERSION // Production version will jump to Factory Image instead of Application A

#include "stdbool.h"


/*******************************************************************************
 * ICA (Image Configuration Area) Definitions
 ******************************************************************************/
#define FICA_VER 2

// ICA Descriptor, used to determine if the ICA area has been initialized or not
#define FICA_ICA_DESC 0xA5A5A5A5

// Start address of ICA area in flash
#define FICA_START_ADDR  0x01FC0000 // with 256KB Erase Block
#define FICA_TABLE_SIZE  0x00040000 // with 256KB Erase Block
//#define FICA_START_ADDR  0x01FFF000 // with 4KB Erase Block

// ICA field size
#define FICA_FIELD_SIZE 4

// ICA Offsets from FICA_START_ADDR
#define FICA_OFFSET_ICA_DESC            0
#define FICA_OFFSET_ICA_VER             4
#define FICA_OFFSET_ICA_COMM            8
#define FICA_OFFSET_ICA_CUR_TYPE       12 // New App Status
#define FICA_OFFSET_ICA_NEW_APP_TYPE   16 // New App Type

// Size of the ICA definitions (summary of ICA Offsets right above)
#define FICA_ICA_DEFINITION_SIZE 20

#define CHECK_APP_SIGN    1
#define APP_SIGN_SIZE  2048


/*******************************************************************************
 * Image Field Definitions
 * Defaults and choices for each of the ICA Record fields
 * Each offset is a word (4 bytes)
 ******************************************************************************/

// Image Record Start Descriptor
#define FICA_REC_START_ID 0x5A5A5A5A

// Image Record offsets from start of record
#define FICA_OFFSET_IMG_DESC 0
#define FICA_OFFSET_IMG_TYPE 4
#define FICA_OFFSET_IMG_ICA_RSV1 8
#define FICA_OFFSET_IMG_ICA_RSV2 12
#define FICA_OFFSET_IMG_ICA_RSV3 16
#define FICA_OFFSET_IMG_ICA_RSV4 20
#define FICA_OFFSET_IMG_ICA_RSV5 24
#define FICA_OFFSET_IMG_ICA_RSV6 28
#define FICA_OFFSET_IMG_START 32
#define FICA_OFFSET_IMG_SIZE 36
#define FICA_OFFSET_IMG_FMT 40
#define FICA_OFFSET_IMG_HASH_TYPE 44
#define FICA_OFFSET_IMG_HASH_LOC 48
#define FICA_OFFSET_IMG_ENC_TYPE 52
#define FICA_OFFSET_IMG_PKI_TYPE 56
#define FICA_OFFSET_IMG_PKI_LOC 60
#define FICA_OFFSET_IMG_FAC_BOOT 64
#define FICA_OFFSET_IMG_RSV2 68
#define FICA_OFFSET_IMG_RSV3 72
#define FICA_OFFSET_IMG_RSV4 76
#define FICA_OFFSET_IMG_RSV5 80
#define FICA_OFFSET_IMG_RSV6 84
#define FICA_OFFSET_IMG_RSV7 88
#define FICA_OFFSET_IMG_RSV8 92
#define FICA_OFFSET_IMG_RSV9 96

// Image Record Field Default values and choices
// Offset 0 - Descriptor ID (start id)
#define FICA_IMG_DESC_ID 0x5A5A5A5A

// Offset 1 - Image Types
#define FICA_IMG_TYPE_NONE    0 // Default
#define FICA_IMG_TYPE_APP_A   1 // Application Factory Image
#define FICA_IMG_TYPE_APP_B   2 // Application A Image
#define FICA_IMG_TYPE_APP_FAC 3 // Application B Image
//#define FICA_IMG_TYPE_BL_FAC  4 // Bootloader Factory Image


// Total Number of Images Defined in this Version
#define FICA_NUM_IMG_TYPES 3

#define ICA_COMM_BOOT_NONE    0x00000000
#define ICA_COMM_AIS_OTA_BIT  0x00000010
#define ICA_COMM_AIS_NAV_BIT  0x00000100
#define ICA_COMM_AIS_NAP_BIT  0x00000200
#define ICA_COMM_AIS_NAI_BIT  0x00000400

// Image Flash Start Addresses for this flash (IS26KL256S)
#define FICA_IMG_ICA_DEF_ADDR   FICA_START_ADDR + FICA_ICA_DEFINITION_SIZE

#define FICA_IMG_BL_FAC_ADDR    0x00002000
#define FICA_IMG_RESERVED_ADDR  0x00200000 // (SLN Toolboxes)
#define FICA_IMG_APP_A_ADDR     0x00300000
#define FICA_IMG_APP_B_ADDR     0x00D00000
#define FICA_IMG_APP_FAC_ADDR   0x01700000

#define FICA_IMG_APP_A_SIZE     0x00A00000 // 10Mb
#define FICA_IMG_APP_B_SIZE     0x00A00000 // 10Mb
#define FICA_IMG_APP_FAC_SIZE   0x00400000 // 4Mb

// Image Size
#define FICA_IMG_SIZE_ZERO 0 // default

// Image Format
#define FICA_IMG_FMT_NONE 0 // default
#define FICA_IMG_FMT_BIN 1
#define FICA_IMG_FMT_SREC 2
#define FICA_IMG_FMT_AES_128 3

// Image CRC
#define FICA_IMG_CRC_NONE 0 // default

// Image Key
#define FICA_IMG_HASH_NONE 0 // default

// Offset 7 - Reserved
#define FICA_IMG_RSV 0 // default

// ICA Record Structure
typedef struct ICA_Record
{
    uint32_t descriptor;
    uint32_t imgtype;
    uint32_t ica_rsv1;
    uint32_t ica_rsv2;
    uint32_t ica_rsv3;
    uint32_t ica_rsv4;
    uint32_t ica_rsv5;
    uint32_t ica_rsv6;
    uint32_t imgaddr;
    uint32_t imglen;
    uint32_t imgfmt;
    uint32_t imghashtype;
    uint32_t imghashloc;
    uint32_t imgenctype;
    uint32_t imgpkitype;
    uint32_t imgpkiloc;
    uint32_t rsv1;
    uint32_t rsv2;
    uint32_t rsv3;
    uint32_t rsv4;
    uint32_t rsv5;
    uint32_t rsv6;
    uint32_t rsv7;
    uint32_t rsv8;
    uint32_t rsv9;
} FICA_Record;

// Uncomment to Blink progress on LEDs
#define BLINK_PROGRESS 1

#define TYPE_APP false
#define TYPE_IMG true

#define IMG_EXT_NO_ERROR 0
#define IMG_EXT_ERROR 1

#define EXT_FLASH_PROGRAM_PAGE   0x200 // 512B
#define EXT_FLASH_ERASE_PAGE   0x40000 // 256KB

#define SLN_FLASH_NO_ERROR 0
#define SLN_FLASH_ERROR -1

////////////////////////////////////////////////////////////////////////////////
// Externals
////////////////////////////////////////////////////////////////////////////////

extern uint8_t ProgExtAppImgBuf[];
extern uint8_t tdata[];


////////////////////////////////////////////////////////////////////////////////
// Prototypes
////////////////////////////////////////////////////////////////////////////////

#if defined(__cplusplus)
extern "C" {
#endif


int32_t FICA_app_program_ext_init(uint32_t newimgtype);
int32_t FICA_app_program_ext_abs(uint32_t offset, uint8_t *pbuf, uint32_t len);
int32_t FICA_app_program_ext_finalize(uint8_t *msgsig);
int32_t FICA_app_program_ext_set_reset_vector();
int32_t FICA_app_program_ext_calculate_crc(uint32_t imgtype, uint32_t *pcrc);
int32_t FICA_app_program_ext_program_crc(uint32_t imgtype, uint32_t crc);
int32_t FICA_clear_buf(uint8_t *pbuf, uint8_t initval);
int32_t FICA_read_db();
int32_t FICA_write_db();
int32_t FICA_write_buf(uint32_t offset, uint32_t len, void *buf);
int32_t FICA_read_buf(uint32_t offset, uint32_t len, void *buf);
int32_t FICA_get_field_addr(uint32_t imgtype, uint32_t fieldoffset, uint32_t *pfieldaddr);
int32_t FICA_write_field(uint32_t imgtype, uint32_t fieldoffset, uint32_t val);
int32_t FICA_write_field_no_save(uint32_t imgtype, uint32_t fieldoffset, uint32_t val);
int32_t FICA_read_field(uint32_t imgtype, uint32_t field_offset, uint32_t *pfieldval);
int32_t FICA_get_app_img_start_addr(uint32_t imgtype, uint32_t *startaddr);
int32_t FICA_get_app_img_max_size(uint32_t imgtype, uint32_t *maximgsize);
int32_t FICA_get_app_img_len(uint32_t imgtype, uint32_t *plen);
int32_t FICA_write_image_info(uint32_t imgtype, uint32_t imgfmt, uint32_t len);
int32_t FICA_read_record(uint32_t imgtype, FICA_Record *pimgrec);
int32_t FICA_write_record(uint32_t imgtype, FICA_Record *pimgrec);
bool is_FICA_initialized(void);
int32_t FICA_initialize(void);
int32_t FICA_verify_ext_flash(void);
uint16_t FICA_compute_chunk_CRC(uint8_t *pData, uint16_t lenData, uint16_t crcValueOld);
int32_t FICA_get_comm_flag(uint32_t comflag, bool *flagstate);
int32_t FICA_set_comm_flag(uint32_t comflag, bool flagstate);
void reset_mcu(void);
int32_t FICA_read_for_debug(uint32_t addr, uint32_t len);
int32_t FICA_GetCurBootStartAddr(uint32_t *paddr);
int32_t FICA_GetCurAppStartType(uint32_t *pimgtype);
int32_t FICA_GetNewAppStartAddr(uint32_t *paddr);
int32_t FICA_SetCurAppStartType(uint32_t imgtype);
int32_t FICA_read_hyper_flash_id(uint8_t *pid);
int32_t FICA_GetImgTypeFromAddr(uint32_t appaddr, uint32_t *imgtype);
int32_t FICA_Verify_Signature(uint8_t *msgsig, uint32_t msglen);
bool FICA_is_OTA_FlashBitCleared();
int32_t FICA_Set_OTA_FlashBit();
int32_t FICA_Clear_OTA_FlashBit();
int32_t FICA_Erase_Bank(uint32_t startaddr, uint32_t banksize);
int32_t FICA_app_program_ext_set_reset_vector();

#if defined(__cplusplus)
}
#endif

//! @}

#endif /* _FICA_DRIVER_H_ */
////////////////////////////////////////////////////////////////////////////////
// EOF
////////////////////////////////////////////////////////////////////////////////
