/*
 * Copyright 2018 NXP.
 * This software is owned or controlled by NXP and may only be used strictly in accordance with the
 * license terms that accompany it. By expressly accepting such terms or by downloading, installing,
 * activating and/or otherwise using the software, you are agreeing that you have read, and that you
 * agree to comply with and are bound by, such license terms. If you do not agree to be bound by the
 * applicable license terms, then you may not retain, install, activate or otherwise use the software.
 */

#ifndef NETWORK_CONNECTION_H_
#define NETWORK_CONNECTION_H_

#include "assert.h"
#include "stdbool.h"
#include "stdint.h"

#include "board.h"

/* Provisioning Method (for WiFi SSID and password) */
#define PROVISIONING_USE_USB_CDC    0
#define PROVISIONING_USE_WIFI       1
#define PROVISIONING_USE_BLE        2

#ifndef PROVISIONING_METHOD
#define PROVISIONING_METHOD         PROVISIONING_USE_WIFI
#endif

/* choose your weapon */
#ifndef USE_ETHERNET_CONNECTION
#define USE_ETHERNET_CONNECTION 0
#endif

#ifndef USE_WIFI_CONNECTION
#define USE_WIFI_CONNECTION 1
#endif

#if USE_WIFI_CONNECTION
#include "wwd_structures.h"
#endif

/* IP address configuration. */
#define configIP_ADDR0 192
#define configIP_ADDR1 168
#define configIP_ADDR2 1
#define configIP_ADDR3 222

/* Netmask configuration. */
#define configNET_MASK0 255
#define configNET_MASK1 255
#define configNET_MASK2 255
#define configNET_MASK3 0

/* Gateway address configuration. */
#define configGW_ADDR0 192
#define configGW_ADDR1 168
#define configGW_ADDR2 1
#define configGW_ADDR3 1

#if USE_WIFI_CONNECTION
/* Max password length */
#define WSEC_MAX_PSK_LEN 64

typedef enum network_wifi_conn_state
{
    NETWORK_WIFI_CONN_WIFI_SETUP,
    NETWORK_WIFI_CONN_INVALID_WIFI_CRED,
    NETWORK_WIFI_CONN_NO_ACCESS_POINT,
    NETWORK_WIFI_CONN_ACCESS_POINT_FOUND
} network_wifi_conn_state_t;

typedef enum network_mode
{
    NETWORK_MODE_AP,     /* Device is configured as AP (when wifi is used)*/
    NETWORK_MODE_STATION /* Device is configured as Station (when either wifi or ethernet is used) */
} network_mode_t;

typedef struct
{
    uint8_t length;                  /**< psk length */
    uint8_t value[WSEC_MAX_PSK_LEN]; /**< actual psk value */
} wiced_psk_t;

typedef struct wifi_cred
{
    wiced_ssid_t ssid;
    wiced_psk_t password;
} wifi_cred_t;

typedef enum wifi_connect_state
{
    WIFI_CONNECT_SUCCESS = 0,       /* Successfully connected to wifi network */
    WIFI_CONNECT_NO_CRED,           /* No wifi credentials found in flash */
    WIFI_CONNECT_WRONG_CRED,        /* Wrong wifi credentials found in flash */
    WIFI_CONNECT_FAILED             /* Failed to connect to wifi network because of other error */
} wifi_connect_state_t;

wiced_bool_t get_wifi_connect_state(void);

void APP_NETWORK_Init(void);
status_t APP_NETWORK_Wifi_Connect(bool use_dhcp);
status_t APP_NETWORK_Wifi_StartAP(wiced_ssid_t ap_ssid, char *ap_passwd);
status_t APP_NETWORK_Wifi_StopAP(void);
status_t APP_NETWORK_Wifi_CheckCred(void);
void APP_Wifi_Connect_Update_Handler_Set(void (*func)(wiced_bool_t event));
void APP_Wifi_UX_Callback(network_wifi_conn_state_t state);

#elif USE_ETHERNET_CONNECTION
void APP_NETWORK_Init(bool use_dhcp);
#endif

void APP_NETWORK_Uninit(void);

#endif /* NETWORK_CONNECTION_H_ */
