/*
 * Amazon FreeRTOS OTA PAL V1.0.0
 * Copyright (C) 2018 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of
 * this software and associated documentation files (the "Software"), to deal in
 * the Software without restriction, including without limitation the rights to
 * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
 * the Software, and to permit persons to whom the Software is furnished to do so,
 * subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
 * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
 * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
 * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 * http://aws.amazon.com/freertos
 * http://www.FreeRTOS.org
 */

/* C Runtime includes. */
#include <stdio.h>
#include <stdlib.h>

/* Amazon FreeRTOS include. */
#include "FreeRTOS.h"
#include "aws_ota_pal.h"
#include "aws_ota_agent_internal.h"

/* Board specific includes */
#include "board.h"
#include "flash_ica_driver.h"
#include "sln_ota.h"

/* Definitions */
#define IMG_TYPE_APP_A_PATH "AppA"
#define IMG_TYPE_APP_B_PATH "AppB"

static OTA_PAL_ImageState_t platformState = eOTA_PAL_ImageState_Unknown;
static TaskHandle_t s_otaDoneTaskHandle = NULL;

/* Specify the OTA signature algorithm we support on this platform. */
const char cOTA_JSON_FileSignatureKey[ OTA_FILE_SIG_KEY_STR_MAX_LENGTH ] = "sig-sha256-rsa";

OTA_Err_t prvPAL_CreateFileForRx( OTA_FileContext_t * const C )
{
    DEFINE_OTA_METHOD_NAME( "prvPAL_CreateFileForRx" );

    OTA_Err_t ret = kOTA_Err_None;
    uint8_t img_type = FICA_IMG_TYPE_NONE;

    /* Decide what image type we have based on the file path */
    if (!strcmp((const char *)C->pucFilePath, IMG_TYPE_APP_A_PATH))
    {
    	img_type = FICA_IMG_TYPE_APP_A;
    } 
	else if (!strcmp((const char *)C->pucFilePath, IMG_TYPE_APP_B_PATH))
	{
		img_type = FICA_IMG_TYPE_APP_B;
	}

    /* Init FICA to be ready for the new application */
    if((kOTA_Err_None == ret) && (FICA_app_program_ext_init(img_type) != SLN_FLASH_NO_ERROR))
    {
    	OTA_LOG_L1("[%s] FICA_app_program_ext_init failed.\r\n", OTA_METHOD_NAME);
		ret = kOTA_Err_RxFileCreateFailed;
    }

    if (kOTA_Err_None == ret)
    {
    	/* We don't need yet anything here, but this field
		 * must not remain NULL after this function returns */
    	C->pucFile = (void *)0x1;
    	OTA_LOG_L1("[%s] OK.\r\n", OTA_METHOD_NAME);
    }
    else
    {
    	OTA_LOG_L1("[%s] Failed.\r\n", OTA_METHOD_NAME);
    }

    return ret;
}
/*-----------------------------------------------------------*/

OTA_Err_t prvPAL_Abort( OTA_FileContext_t * const C )
{
    DEFINE_OTA_METHOD_NAME( "prvPAL_Abort" );

    OTA_Err_t eResult = kOTA_Err_None;

    /* reset OTA flash bit and then the board if:
     * - abort was called before receiving the whole file
     *   and
     * - abort is not called during agent shutting down phase
     *   (agent is shut down when wifi link is lost)
     */
    if (C->ulBlocksRemaining != 0 &&
    	OTA_GetAgentState() != eOTA_AgentState_ShuttingDown)
    {
    	OTA_LOG_L1( "[%s] Aborting OTA, did not receive all file blocks.\r\n", OTA_METHOD_NAME );

    	/* Must set the OTA bit to not start OTA after next reset */
		if (FICA_Set_OTA_FlashBit() != SLN_FLASH_NO_ERROR)
		{
			OTA_LOG_L1( "[%s] FICA_Set_OTA_FlashBit failed.\r\n", OTA_METHOD_NAME );
			eResult = kOTA_Err_AbortFailed;
		}

		prvPAL_ResetDevice();
    }
    else
    {
    	OTA_LOG_L1( "[%s] Abort.\r\n", OTA_METHOD_NAME );
    }

    return eResult;
}
/*-----------------------------------------------------------*/

/* Write a block of data to the specified file. */
int16_t prvPAL_WriteBlock( OTA_FileContext_t * const C,
                           uint32_t ulOffset,
                           uint8_t * const pacData,
                           uint32_t ulBlockSize )
{
    DEFINE_OTA_METHOD_NAME( "prvPAL_WriteBlock" );

    if (FICA_app_program_ext_abs(ulOffset, (void *)pacData, ulBlockSize) != SLN_FLASH_NO_ERROR)
	{
		OTA_LOG_L1( "[%s] FICA_app_program_ext_abs failed.\r\n", OTA_METHOD_NAME );
		return -1;
	}

    return ulBlockSize;
}
/*-----------------------------------------------------------*/

OTA_Err_t prvPAL_CloseFile( OTA_FileContext_t * const C )
{
    DEFINE_OTA_METHOD_NAME( "prvPAL_CloseFile" );

    if(FICA_app_program_ext_finalize(C->pxSignature->ucData) != SLN_FLASH_NO_ERROR)
	{
    	OTA_LOG_L1("[%s] FICA_app_program_ext_finalize failed.\r\n", OTA_METHOD_NAME);
    	return kOTA_Err_FileClose;
	}

    OTA_LOG_L1("[%s] OK.\r\n", OTA_METHOD_NAME);
    C->pucFile = NULL;

    return kOTA_Err_None;
}
/*-----------------------------------------------------------*/

OTA_Err_t prvPAL_ResetDevice( void )
{
    DEFINE_OTA_METHOD_NAME("prvPAL_ResetDevice");

    OTA_LOG_L1( "[%s] Resetting the device.\r\n", OTA_METHOD_NAME );

    /* give some time for last logs printing */
    vTaskDelay(2000);

    /* good bye, cruel world! */
    NVIC_SystemReset();

    return kOTA_Err_None;
}
/*-----------------------------------------------------------*/

OTA_Err_t prvPAL_ActivateNewImage(void)
{
    DEFINE_OTA_METHOD_NAME("prvPAL_ActivateNewImage");

    OTA_LOG_L1( "[%s] Activating the new MCU image.\r\n", OTA_METHOD_NAME );
	return prvPAL_ResetDevice( );
}
/*-----------------------------------------------------------*/

OTA_Err_t prvPAL_SetPlatformImageState( OTA_ImageState_t eState )
{
    DEFINE_OTA_METHOD_NAME( "prvPAL_SetPlatformImageState" );
    OTA_Err_t eResult = kOTA_Err_Uninitialized;

	if( eState == eOTA_ImageState_Accepted )
	{
		OTA_LOG_L1( "[%s] Accepted image.\r\n", OTA_METHOD_NAME );

		/* If previous state was PendingCommit, we are now in Self Test mode,
		 * so we should signal the ota done task to check when the OTA Agent
		 * goes back into ready state and update the Reset Vector, set FICA OTA flag, etc */
		if (eOTA_PAL_ImageState_PendingCommit == platformState)
		{
			xTaskNotify(s_otaDoneTaskHandle, kSelfTestPassed, eSetBits);
		}

		eResult = kOTA_Err_None;
	}
	else if ( eState == eOTA_ImageState_Rejected  )
	{
		OTA_LOG_L1( "[%s] Rejected image.\r\n", OTA_METHOD_NAME );

		/* signal ota done task, it'll reset in order to not get stuck in the bootloader  */
		xTaskNotify(s_otaDoneTaskHandle, kOtaFailed, eSetBits);
	}
	else if ( eState == eOTA_ImageState_Aborted )
	{
		OTA_LOG_L1( "[%s] Aborted image.\r\n", OTA_METHOD_NAME );

		/* signal ota done task, it'll reset in order to not get stuck in the bootloader  */
		xTaskNotify(s_otaDoneTaskHandle, kOtaFailed, eSetBits);
	}
	else if ( eState == eOTA_ImageState_Testing )
	{
		OTA_LOG_L1( "[%s] Image state testing.\r\n", OTA_METHOD_NAME );
		platformState = eOTA_PAL_ImageState_PendingCommit;
		eResult = kOTA_Err_None;
	}
	else
	{
		eResult = kOTA_Err_BadImageState;
	}

	return eResult;
}
/*-----------------------------------------------------------*/

OTA_PAL_ImageState_t prvPAL_GetPlatformImageState( void )
{
    DEFINE_OTA_METHOD_NAME( "prvPAL_GetPlatformImageState" );

    /* we are not really using self test mode for other than checking version got upgraded,
     * no need for extra code here */
    return platformState;
}
/*-----------------------------------------------------------*/

void ota_pal_ota_done_task_set(TaskHandle_t handle)
{
	s_otaDoneTaskHandle = handle;
}

/* Provide access to private members for testing. */
#ifdef AMAZON_FREERTOS_ENABLE_UNIT_TESTS
#include "aws_ota_pal_test_access_define.h"
#endif
