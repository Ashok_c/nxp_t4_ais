/*
 * Copyright 2018 NXP. 
 * This software is owned or controlled by NXP and may only be used strictly in accordance with the
 * license terms that accompany it. By expressly accepting such terms or by downloading, installing,
 * activating and/or otherwise using the software, you are agreeing that you have read, and that you
 * agree to comply with and are bound by, such license terms. If you do not agree to be bound by the
 * applicable license terms, then you may not retain, install, activate or otherwise use the software.
 */

/* Board includes */
#include "board.h"
#include "pin_mux.h"
#include "fsl_debug_console.h"

/* FreeRTOS kernel includes */
#include "FreeRTOS.h"
#include "task.h"

/* Crypto includes */
#include "ksdk_mbedtls.h"
#include "base64.h"

/* AWS includes */
#include "aws_logging_task.h"

/* USB includes */
#include "serial_manager.h"
#include "usb_device_board_config.h"
#include "usb_phy.h"
#include "usb.h"

#include "sln_flash_mgmt.h"
#include "wifi_credentials.h"
#include "sln_flash.h"
#include "sln_ota.h"
#include "bootloader.h"
#include "sln_ota_sec.h"
#include "sln_file_table.h"

/*******************************************************************************
* Definitions
******************************************************************************/

/*******************************************************************************
* Variables
******************************************************************************/

/*******************************************************************************
* Code
******************************************************************************/

int main(void)
{
    __asm("cpsid i");

    BOARD_ConfigMPU();

    /* Enable additional fault handlers */
	SCB->SHCSR |= (SCB_SHCSR_BUSFAULTENA_Msk | /*SCB_SHCSR_USGFAULTENA_Msk |*/ SCB_SHCSR_MEMFAULTENA_Msk);

	/* Init board hardware. */
	BOARD_InitBootPins();
	BOARD_BootClockRUN();

	USB_DeviceClockInit();
	BOARD_InitDebugConsole();
	CRYPTO_InitHardware();

	/* Initialize Flash to allow writing */
	SLN_Flash_Init();
	/* Initialize flash management */
	SLN_FLASH_MGMT_Init((sln_flash_entry_t *)g_fileTable , false);

	BootloaderMain();

	/* Should not reach this statement */
	while (1)
	{
	}
}
