/*
 * Copyright 2019 NXP.
 * This software is owned or controlled by NXP and may only be used strictly in accordance with the
 * license terms that accompany it. By expressly accepting such terms or by downloading, installing,
 * activating and/or otherwise using the software, you are agreeing that you have read, and that you
 * agree to comply with and are bound by, such license terms. If you do not agree to be bound by the
 * applicable license terms, then you may not retain, install, activate or otherwise use the software.
 */

#include "sln_ota.h"

/* Declare the firmware version structure for all to see. */
AppVersion32_t xAppFirmwareVersion = {
   .u.x.ucMajor = 0,
   .u.x.ucMinor = 0,
   .u.x.usBuild = 0,
};

char *alexaCLIENT_ID = NULL;
MQTTAgentHandle_t mqttHandle;
TaskHandle_t otaDoneTaskHandle = NULL;
static EventGroupHandle_t s_reconnectionEventGroup;

typedef enum __reconnect_event
{
    kReconnectNetworkLoss     = (1U << 0U),
	kReconnectNetworkUp       = (1U << 1U)
}reconnectEvent_t;

BaseType_t APP_MQTT_Connect(MQTTAgentHandle_t *xMQTTHandle)
{
    MQTTAgentReturnCode_t xReturned;
    MQTTAgentConnectParams_t xConnectParameters;
    int attempts = 0;

    memset(&xConnectParameters, 0, sizeof(xConnectParameters));

    xConnectParameters.pcURL = clientcredentialMQTT_BROKER_ENDPOINT;
    xConnectParameters.xFlags = mqttagentREQUIRE_TLS;
#if clientcredentialMQTT_BROKER_PORT == 443
    xConnectParameters.xFlags |= mqttagentUSE_AWS_IOT_ALPN_443;
#endif

    /* Deprecated - unused. */
    xConnectParameters.xURLIsIPAddress = pdFALSE;
    xConnectParameters.usPort = clientcredentialMQTT_BROKER_PORT;
    xConnectParameters.pucClientId = (const uint8_t *)alexaCLIENT_ID;
    xConnectParameters.usClientIdLength = (uint16_t)strlen(alexaCLIENT_ID);
    /* Deprecated - unused. */
    xConnectParameters.xSecuredConnection = pdFALSE;
    xConnectParameters.pvUserData = NULL;
    xConnectParameters.pxCallback = NULL;
    xConnectParameters.pcCertificate = NULL;
    xConnectParameters.ulCertificateSize = 0;

    xReturned = MQTT_AGENT_Create(xMQTTHandle);
    if (xReturned != eMQTTAgentSuccess)
    {
        return pdFAIL;
    }

    for (attempts = 0; attempts < 3; attempts++)
    {
        /* Connect to the broker. */
        configPRINTF(
                ("MQTT Alexa attempt %d to connect to %s.\r\n", attempts + 1, clientcredentialMQTT_BROKER_ENDPOINT));
        xReturned = MQTT_AGENT_Connect(*xMQTTHandle, &xConnectParameters, pdMS_TO_TICKS(3000));
        if (xReturned != eMQTTAgentSuccess)
        {
            configPRINTF(("ERROR %d:  MQTT Alexa failed to connect.\r\n", xReturned));
#if USE_WIFI_CONNECTION
            if (WICED_FALSE == get_wifi_connect_state())
            {
                break;
            }
#endif
        }
        else
        {
            configPRINTF(("MQTT Alexa connected.\r\n"));
            return pdPASS;
        }
    }

    /* Could not connect, so delete the MQTT client. */
    MQTT_AGENT_Delete(*xMQTTHandle);

#if USE_WIFI_CONNECTION
    if (3 == attempts)
    {
#endif
    	return pdFAIL;
#if USE_WIFI_CONNECTION
    }
    else
    {
    	return 2;
    }
#endif
}

void APP_MQTT_Disconnect(MQTTAgentHandle_t *xMQTTHandle)
{
    MQTT_AGENT_Disconnect(*xMQTTHandle, pdMS_TO_TICKS(3000));
    MQTT_AGENT_Delete(*xMQTTHandle);
}

/* Need to generate a unique ID for the client ID */
void APP_GetUniqueID(char **uniqueID)
{
    uint64_t u64UniqueIDRaw = (uint64_t)((uint64_t)OCOTP->CFG1 << 32ULL) | OCOTP->CFG0;

    uint32_t cIdLen = 0;
    mbedtls_base64_encode( NULL, 0, &cIdLen, (const unsigned char *)&u64UniqueIDRaw, sizeof(uint64_t));
    *uniqueID = (char *)pvPortMalloc(cIdLen + 1);
    memset(*uniqueID, 0, cIdLen + 1);
    uint32_t outputLen = 0;
    mbedtls_base64_encode((unsigned char *)*uniqueID, cIdLen, &outputLen, (const unsigned char *)&u64UniqueIDRaw, sizeof(uint64_t));
}

/* Remove the trailing '=' sign from the thing name */
static void remove_equal_sign(char *my_string)
{
    const char equal = '='; // character to search
    char *found;
    found = strchr(my_string, equal);
    if (found)
    {
    	/* replace '=' with null character '\0' */
        *found = '\0';
    }
}

void reconnectTask(void *arg)
{
	EventBits_t reconnectionEventBits;
	s_reconnectionEventGroup = xEventGroupCreate();

	if ( s_reconnectionEventGroup == NULL)
	{
		configPRINTF(("xEventGroupCreate failed\r\n"));
		goto exit;
	}

	while(1)
	{
		reconnectionEventBits = xEventGroupWaitBits(s_reconnectionEventGroup, kReconnectNetworkLoss | kReconnectNetworkUp, pdTRUE, pdFALSE, portMAX_DELAY );

		if (kReconnectNetworkLoss & reconnectionEventBits)
		{
			/* Turn off ota agent now */
			OTA_AgentShutdown(10);

			/* Turn off mqtt agent now */
			APP_MQTT_Disconnect(&mqttHandle);
		} else
		if (kReconnectNetworkUp & reconnectionEventBits)
		{
			vTaskDelay(2000);

			/* Connect to MQTT broker */
			BaseType_t ret = APP_MQTT_Connect(&mqttHandle);
			if (pdFAIL == ret)
			{
				configPRINTF(("APP_MQTT_Connect failed, resetting the board.\r\n"));
				vTaskDelay(2000);
				NVIC_SystemReset();
			}
			else if (2 == ret)
			{
				/* link loss, stop current iteration here, wait for a new connection */
				configPRINTF(("APP_MQTT_Connect failed, link loss during connection.\r\n"));
				continue;
			}
			else
			{
				/* mqtt connect succeeded */
			}

			/* OTA agent initialization */
			OTA_State_t ota_ret = OTA_AgentInit(mqttHandle,
												(const uint8_t *)alexaCLIENT_ID,
												NULL,		/* NULL uses the default callback handler. */
												( TickType_t ) ~0 );

			if (ota_ret != eOTA_AgentState_Ready)
			{
				if (WICED_FALSE != get_wifi_connect_state())
				{
					configPRINTF(("OTA_AgentInit failure, resetting the board.\r\n"));
					vTaskDelay(2000);
					NVIC_SystemReset();
				}
				else
				{
					/* link loss during OTA_AgentInit */
					configPRINTF(("OTA_AgentInit failed, link loss during connection.\r\n"));
				}
			}
		}
	}

exit:
	vTaskDelete(NULL);
}

void otaDoneTask(void *arg)
{
	uint32_t taskNotification = 0;

	/* suspend the task here, wait for the OTA PAL to wake it */
	xTaskNotifyWait( ULONG_MAX, ULONG_MAX, &taskNotification, portMAX_DELAY );

	/* extra delay, precaution measure, give time to OTA agent
	 * to send status message to cloud */
	vTaskDelay(2000);

	if (kSelfTestPassed & taskNotification)
	{
		/* wait for max 10 seconds to have the agent in ready state */
		int retry = 10;
		OTA_State_t eState;
		while (retry--)
		{
			if ((eState = OTA_GetAgentState()) != eOTA_AgentState_Ready)
			{
				vTaskDelay(1000);
			}
		}

		if (FICA_app_program_ext_set_reset_vector() != SLN_FLASH_NO_ERROR)
		{
			configPRINTF(("FICA_app_program_ext_set_reset_vector failed\r\n"));
		}
		else
		{
			configPRINTF(("FICA_app_program_ext_set_reset_vector succeeded, using updated app after reset\r\n"));
		}
		prvPAL_ActivateNewImage();
	}

	if (kOtaFailed & taskNotification)
	{
		/* Must set the OTA bit to not start OTA after next reset */
		if (FICA_Set_OTA_FlashBit() != SLN_FLASH_NO_ERROR)
		{
			configPRINTF(("FICA_Set_OTA_FlashBit failed.\r\n"));
		}

		/* Must reset NAP bit, otherwise we'll read wrong version at next OTA */
		if(FICA_set_comm_flag(ICA_COMM_AIS_NAP_BIT, false)!=SLN_FLASH_NO_ERROR)
		{
			configPRINTF(("Failed to reset ICA_COMM_AIS_NAP_BIT.\r\n"));
		}

		configPRINTF(("OTA failed or got canceled. Resetting ... \r\n"));
		prvPAL_ResetDevice();
	}

	/* prvPAL_ActivateNewImage should reset the board, but, hey, this is still a FreeRTOS task */
	vTaskDelete(NULL);
}

static uint32_t mainAppVersionGet()
{
	bool commflag = false;
	uint32_t imgtype = FICA_IMG_TYPE_APP_FAC;
	uint32_t appaddr = FICA_IMG_APP_FAC_ADDR;

	/* Just in case ... */
	FICA_initialize();

	/* Determine if we are in self test mode. We can do that by verifying ICA_COMM_AIS_NAP_BIT */
	if (FICA_get_comm_flag(ICA_COMM_AIS_NAP_BIT, &commflag)!=SLN_FLASH_NO_ERROR)
		return SLN_FLASH_ERROR;

	if (commflag)
	{
		/* self test mode, get the address of the pending commit main app */
		if (FICA_read_field(FICA_IMG_TYPE_NONE, FICA_OFFSET_ICA_NEW_APP_TYPE, &imgtype) != SLN_FLASH_NO_ERROR)
			return SLN_FLASH_ERROR;
	}
	else
	{
		/* not self test mode, get the address of the currently running main app */
		if (FICA_GetCurAppStartType(&imgtype) != SLN_FLASH_NO_ERROR)
			return SLN_FLASH_ERROR;
	}

	if (imgtype == 0 || imgtype > FICA_NUM_IMG_TYPES)
		return SLN_FLASH_ERROR;

	if (FICA_get_app_img_start_addr(imgtype, &appaddr) != SLN_FLASH_NO_ERROR)
		return SLN_FLASH_ERROR;

	xAppFirmwareVersion.u.ulVersion32 = (uint32_t)*((uint32_t *)(SLN_Flash_Get_Read_Address(appaddr + 0x3C0)));

	return 0;
}

#if USE_WIFI_CONNECTION
void APP_wifi_connect_update_handler(wiced_bool_t event)
{
    switch (event)
    {
    case WICED_FALSE:
        if (WICED_FALSE != get_wifi_connect_state())
        {
        	/* signal reconnection task about the event */
        	if (NULL != s_reconnectionEventGroup)
			{
				xEventGroupSetBits(s_reconnectionEventGroup, kReconnectNetworkLoss );
			}
        }
        break;
    case WICED_TRUE:
    	if (WICED_TRUE != get_wifi_connect_state())
		{
    		/* signal reconnection task about the event */
    		if (NULL != s_reconnectionEventGroup)
			{
				xEventGroupSetBits(s_reconnectionEventGroup, kReconnectNetworkUp );
			}
		}
		break;
    default:
        break;
    }
}
#endif

void otaAppInitTask(void *arg)
{
	/* give time for usb logging init */
	vTaskDelay(2000);

	/* Get the main app version */
	if (mainAppVersionGet())
	{
		configPRINTF(("mainAppVersionGet failed\r\n"));
	}

#if USE_WIFI_CONNECTION
    APP_Wifi_Connect_Update_Handler_Set(&APP_wifi_connect_update_handler);
	xTaskCreate(reconnectTask, "Reconnect_task", 1024, NULL, configMAX_PRIORITIES - 1, NULL);
#endif

	/* Initialize Amazon libraries */
	if (SYSTEM_Init() != pdPASS)
	{
		configPRINTF(("SYSTEM_Init failed\r\n"));
		goto exit;
	}

    /* Initialize network */
#if USE_ETHERNET_CONNECTION
    APP_NETWORK_Init(true);
#elif USE_WIFI_CONNECTION
    APP_NETWORK_Init();
    APP_NETWORK_Wifi_Connect(true);
#endif

    APP_GetUniqueID(&alexaCLIENT_ID);
	remove_equal_sign(alexaCLIENT_ID);

    /* Connect to MQTT broker */
    if (pdFAIL == APP_MQTT_Connect(&mqttHandle))
	{
    	configPRINTF(("APP_MQTT_Connect failed\r\n"));
		goto exit;
	}

    /* OTA agent initialization */
    OTA_AgentInit(mqttHandle,
    		      (const uint8_t *)alexaCLIENT_ID,
				  NULL,		/* NULL uses the default callback handler. */
				  ( TickType_t ) ~0 );

    /* Create a task for getting signaled when OTA finishes and a reset is needed */
    xTaskCreate(otaDoneTask, "ota_done_task", 1024, NULL, tskIDLE_PRIORITY + 1, &otaDoneTaskHandle);

    /* Inform the AWS OTA PAL about the task handle */
    ota_pal_ota_done_task_set(otaDoneTaskHandle);

    OTA_State_t eState;
    for (;;)
    {
    	while( ( eState = OTA_GetAgentState() ) != eOTA_AgentState_NotReady )
		{
			/* Wait forever for OTA traffic but allow other tasks to run
			   and output statistics only once per second. */

			vTaskDelay( 5000 );
			configPRINTF( ( "State: %d  Received: %u   Queued: %u   Processed: %u   Dropped: %u\r\n",
				OTA_GetAgentState(),
				OTA_GetPacketsReceived(),
				OTA_GetPacketsQueued(),
				OTA_GetPacketsProcessed(),
				OTA_GetPacketsDropped() ) );
		}

		/* ... Handle MQTT disconnect per your application needs ... */
    	vTaskDelay(5000);
    }

exit:
    vTaskDelete(NULL);
}
