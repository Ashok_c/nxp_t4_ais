/*
 * Copyright 2019 NXP
 * All rights reserved.
 *
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "fsl_gpio.h"

// Uncomment to force the FICA table to initialize
//#define FORCE_FICA_TABLE_INIT   1

#include "flash_ica_driver.h"
#include "pin_mux.h"
#include "board.h"

#include "FreeRTOS.h"

// HYPERFLASH Includes
#include "sln_flash.h"
#include "fsl_flexspi.h"
#include "flexspi_hyper_flash.h"
#include "sln_flash_mgmt.h"
#include "sln_ota_sec.h"

// Encrypted XIP Includes
#include "nor_encrypt_bee.h"

//! @addtogroup flash_ica
//! @{

/*******************************************************************************
 * Definitions
 ******************************************************************************/

////////////////////////////////////////////////////////////////////////////////
// Prototypes
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
// Variables
////////////////////////////////////////////////////////////////////////////////

uint32_t FICA_RECORD_SIZE = sizeof(FICA_Record);
uint32_t ProgExtAppImgType = 0;
uint32_t ProgExtAppImgStartAddr = 0;
uint8_t *pProgExtAppImgStartAddr = (uint8_t *)&ProgExtAppImgStartAddr;
uint32_t ProgExtAppImgMaxSize = 0;
uint32_t ProgExtAppImgCurLen = 0;
uint32_t ProgExtAppImgCurPageLen = 0;
uint32_t ProgExtAppImgCurPageOffset = 0;
uint8_t *pProgExtAppImgBuf=ProgExtAppImgBuf;
uint8_t *pProgExtAppImgBufRun=ProgExtAppImgBuf;
uint8_t ProgExtAppImgCert[APP_SIGN_SIZE] = {0};
uint8_t *pProgExtAppImgCert = &ProgExtAppImgCert[0];
FICA_Record ProgExtAppInfo = {0};
volatile uint32_t ErrorCnt = 0;
//bool ProgExtAppImg1stPage = true;
static bool s_isEncryptedXIP = false;


////////////////////////////////////////////////////////////////////////////////
// Code
////////////////////////////////////////////////////////////////////////////////

/*
 * FICA - Flash ICA (Image Configuration Area) Driver
 *
 */

//! @brief Initialize the Application Program External Flash Interface
__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE")))
int32_t FICA_app_program_ext_init(uint32_t newimgtype)
{
	status_t status;
	uint32_t curimgtype = FICA_IMG_TYPE_APP_FAC;

	ProgExtAppImgType = newimgtype;

	// Initialize the Flash ICA (Image Configuration Area)
	if(FICA_initialize()!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

    if(FICA_GetCurAppStartType(&curimgtype)!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

    if(newimgtype==curimgtype)
		return(SLN_FLASH_ERROR);

	// Get the base address for the new application based on the image type
	if(FICA_get_app_img_start_addr(ProgExtAppImgType, &ProgExtAppImgStartAddr)!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	// Get the application image max size
	if(FICA_get_app_img_max_size(ProgExtAppImgType, &ProgExtAppImgMaxSize)!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	ProgExtAppImgCurLen = 0;
	ProgExtAppImgCurPageLen = 0;
	ProgExtAppImgCurPageOffset = 0;
	FICA_clear_buf(pProgExtAppImgBuf,0xFF);
	pProgExtAppImgBufRun=ProgExtAppImgBuf;

	// Read the new application information back into the Info structure
	if(FICA_read_record(newimgtype, &ProgExtAppInfo)!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	// Erase all pages in this App bank
	if(FICA_Erase_Bank(ProgExtAppImgStartAddr, ProgExtAppImgMaxSize)!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	if(FICA_set_comm_flag(ICA_COMM_AIS_NAI_BIT, true)!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	s_isEncryptedXIP = bl_nor_encrypt_has_encrypted_region();

	return(SLN_FLASH_NO_ERROR);
}


//! @brief Blocking image program to external flash
// This is part of a blocking image program, but the actual small buffer flash write is blocking
// Assumes all calls have imgaddr that is PAGE ALIGNED
__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE")))
int32_t FICA_app_program_ext_abs(uint32_t offset, uint8_t *bufptr, uint32_t writelen)
{
	// Write the image buffer to the external flash
	// len should be the flash page size until the last call, then the remainder
	// Erase page before writing

	uint32_t curextlen = 0; // current length of the write, accumulates to total image write length (writelen)
	uint32_t curwritelen = 0; // will be EXT_FLASH_ERASE_PAGE size until the last page
	uint32_t curextpage = 0; // Increments up each loop to index the page that is to be written
	uint32_t curwriteaddr = 0; // address to erase page and write the current buffer data, buffer reindexes on each loop

	uint32_t pagenumrem = offset % EXT_FLASH_PROGRAM_PAGE;
	uint32_t pagestartaddr = (offset / EXT_FLASH_PROGRAM_PAGE) * EXT_FLASH_PROGRAM_PAGE;
	uint32_t prestartlen = offset - pagestartaddr;
	uint32_t tempaddr = 0;

	bool commflag = false;

	// check if some newbie passed in a null pointer for a buffer
	if(bufptr==NULL || writelen==0)
		return(SLN_FLASH_ERROR);

	// length can not be greater than the total flash size allocated for images
	if((offset + writelen) > ProgExtAppImgMaxSize)
		return(SLN_FLASH_ERROR);

	// Verify the init passed before calling this abs function
	if(FICA_get_comm_flag(ICA_COMM_AIS_NAI_BIT, &commflag)!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	if(!commflag)
		return(SLN_FLASH_ERROR);

	FICA_clear_buf(pProgExtAppImgBuf,0xFF);

	int32_t templen = offset + writelen;
	if(templen>(int32_t)ProgExtAppImgCurLen)
		ProgExtAppImgCurLen = (uint32_t)templen;

	// If its not on a flash page boundary, need to read flash before erasing the page
	if(pagenumrem > 0)
	{
		FICA_read_for_debug(ProgExtAppImgStartAddr + pagestartaddr, EXT_FLASH_PROGRAM_PAGE);

		// the image address doesn't start on a page boundary, read the flash from the start of the page
		if(SLN_Read_Flash_At_Address(ProgExtAppImgStartAddr + pagestartaddr, (uint8_t *)pProgExtAppImgBuf, prestartlen)!=SLN_FLASH_NO_ERROR)
			return(SLN_FLASH_ERROR);
	}

	// Erase External Flash and Program it from the buffer, by external flash page
	while(curextlen < writelen)
	{
		// Write either a page or less if only less than a page is left
		curwritelen = prestartlen + (writelen - curextlen) >= EXT_FLASH_PROGRAM_PAGE ? EXT_FLASH_PROGRAM_PAGE - prestartlen : writelen - curextlen;
		curwriteaddr = pagestartaddr + (curextpage * EXT_FLASH_PROGRAM_PAGE);

		// See if there is data after the data we need to write, that needs to be saved as well
		if(prestartlen+curwritelen<EXT_FLASH_PROGRAM_PAGE)
		{
			uint32_t remainpagelen = EXT_FLASH_PROGRAM_PAGE - prestartlen - curwritelen;
			tempaddr = curwriteaddr + prestartlen + curwritelen;

			if(SLN_Read_Flash_At_Address(ProgExtAppImgStartAddr + tempaddr, (uint8_t *)&pProgExtAppImgBuf[prestartlen+curwritelen], remainpagelen)!=SLN_FLASH_NO_ERROR)
				return(SLN_FLASH_ERROR);
		}

		// Copy the data from the passed buffer to the Program Image Buffer
		memcpy(&pProgExtAppImgBuf[prestartlen], bufptr, curwritelen);

		tempaddr = curwriteaddr;

		if (s_isEncryptedXIP && bl_nor_in_encrypted_region(ProgExtAppImgStartAddr + tempaddr, EXT_FLASH_PROGRAM_PAGE))
		{
            if (kStatus_Success != bl_nor_encrypt_data(ProgExtAppImgStartAddr + tempaddr, EXT_FLASH_PROGRAM_PAGE, (uint32_t *)pProgExtAppImgBuf))
            {
                return(SLN_FLASH_ERROR);
            }
		}

		if(SLN_Write_Flash_At_Address(ProgExtAppImgStartAddr + tempaddr, (uint8_t *)pProgExtAppImgBuf)!=SLN_FLASH_NO_ERROR)
			return(SLN_FLASH_ERROR);

		curextlen += curwritelen;
		bufptr += curwritelen;
		curextpage++;
		prestartlen = 0;
	}
	return(IMG_EXT_NO_ERROR);
}



//! @brief  Finalize the program by verifying the Signature and writing image info
__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE")))
int32_t FICA_app_program_ext_finalize(uint8_t *msgsig)
{

	if(FICA_write_image_info(ProgExtAppImgType, FICA_IMG_FMT_BIN, ProgExtAppImgCurLen)!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	if(FICA_set_comm_flag(ICA_COMM_AIS_NAV_BIT, true)!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	if(msgsig!=NULL)
	{
		if(FICA_Verify_Signature(msgsig, ProgExtAppImgCurLen)!=SLN_FLASH_NO_ERROR)
			return(SLN_FLASH_ERROR);
	}

	if(FICA_write_field(FICA_IMG_TYPE_NONE, FICA_OFFSET_ICA_NEW_APP_TYPE, ProgExtAppImgType)!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	if(FICA_set_comm_flag(ICA_COMM_AIS_NAP_BIT, true)!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	if(FICA_set_comm_flag(ICA_COMM_AIS_NAI_BIT, false)!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	s_isEncryptedXIP = false;

	return(SLN_FLASH_NO_ERROR);
}


//! @brief  Sets the reset vector to the new application and sets the OTA bit indicating its done
__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE")))
int32_t FICA_app_program_ext_set_reset_vector()
{
	bool commflag = false;

	FICA_initialize();

	if(FICA_get_comm_flag(ICA_COMM_AIS_NAP_BIT, &commflag)!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	if(commflag)
	{
		if(FICA_read_field(FICA_IMG_TYPE_NONE, FICA_OFFSET_ICA_NEW_APP_TYPE, &ProgExtAppImgType)!=SLN_FLASH_NO_ERROR)
			return(SLN_FLASH_ERROR);

		if(ProgExtAppImgType == 0 || ProgExtAppImgType > FICA_NUM_IMG_TYPES)
			return(SLN_FLASH_ERROR);

		// Write the new application address as the current application address
		if(FICA_SetCurAppStartType(ProgExtAppImgType)!=SLN_FLASH_NO_ERROR)
			return(SLN_FLASH_ERROR);

		if(FICA_set_comm_flag(ICA_COMM_AIS_NAV_BIT, false)!=SLN_FLASH_NO_ERROR)
			return(SLN_FLASH_ERROR);

		if(FICA_set_comm_flag(ICA_COMM_AIS_NAP_BIT, false)!=SLN_FLASH_NO_ERROR)
			return(SLN_FLASH_ERROR);

		// Setting the OTA flash bit clears the request for OTA update
		if(FICA_Set_OTA_FlashBit()!=SLN_FLASH_NO_ERROR)
			return(SLN_FLASH_ERROR);
	}
	else
	{
		return(SLN_FLASH_ERROR);
	}

	return(SLN_FLASH_NO_ERROR);
}


__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE")))
int32_t FICA_Erase_Bank(uint32_t startaddr, uint32_t banksize)
{
	// Erase all sectors in this bank
	for(uint32_t runaddr = startaddr; runaddr < (startaddr + banksize); runaddr+=EXT_FLASH_ERASE_PAGE)
	{
		if(SLN_Erase_Sector(runaddr)!=kStatus_Success)
			return(SLN_FLASH_ERROR);
	}
	return(SLN_FLASH_NO_ERROR);
}


//! @brief  Flush the program buffer.  Write any remaining bytes out to the flash
__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE")))
int32_t FICA_Verify_Signature(uint8_t *msgsig, uint32_t msglen)
{
	uint32_t imgType;
	int32_t ret = 0;
	uint8_t *caRootPem = NULL;
	uint32_t caRootLen = 0;

	uint32_t msgAddr = SLN_Flash_Get_Read_Address(ProgExtAppImgStartAddr);

	// Calculate the start of the signature
	uint32_t offset = ProgExtAppImgStartAddr + msglen - APP_SIGN_SIZE;
	uint32_t imageLen = msglen - APP_SIGN_SIZE;

	// Read the Cert from flash
	if(SLN_Read_Flash_At_Address(offset, pProgExtAppImgCert, APP_SIGN_SIZE)!=SLN_FLASH_NO_ERROR)
	{
		ret = SLN_FLASH_ERROR;
		goto exit;
	}

	ret = SLN_FLASH_MGMT_Read(ROOT_CA_CERT, NULL, &caRootLen);

	if (SLN_FLASH_MGMT_OK == ret)
	{
		// Found a file
		caRootPem = (uint8_t *)pvPortMalloc(caRootLen);

		if (NULL == caRootPem)
		{
			return SLN_FLASH_MGMT_ENOMEM;
		}

		ret = SLN_FLASH_MGMT_Read(ROOT_CA_CERT, caRootPem, &caRootLen);
	}
	else
	{
		goto exit;
	}

	if(SLN_OTA_SEC_Verify_Cert((uint8_t *)caRootPem, pProgExtAppImgCert)!=SLN_FLASH_NO_ERROR)
	{
		ret = SLN_FLASH_ERROR;
		goto exit;
	}

	if(SLN_OTA_SEC_Verify_Signature(pProgExtAppImgCert, (uint8_t *)msgAddr, imageLen, msgsig)!=SLN_FLASH_NO_ERROR)
	{
		ret = SLN_FLASH_ERROR;
		goto exit;
	}

	ret = FICA_GetImgTypeFromAddr(ProgExtAppImgStartAddr, &imgType);

	if (SLN_FLASH_NO_ERROR == ret)
	{
		if (FICA_IMG_TYPE_APP_A == imgType)
		{
			ret = SLN_FLASH_MGMT_Save(APP_A_SIGNING_CERT, pProgExtAppImgCert, safe_strlen((char *)pProgExtAppImgCert, APP_SIGN_SIZE));
		}
		else if (FICA_IMG_TYPE_APP_B == imgType)
		{
			ret = SLN_FLASH_MGMT_Save(APP_B_SIGNING_CERT, pProgExtAppImgCert, safe_strlen((char *)pProgExtAppImgCert, APP_SIGN_SIZE));
		}
	}

exit:
	vPortFree(caRootPem);
	caRootPem = NULL;

	return ret;
}

//! @brief Clears the buffer with the passed initialization value
__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE"))) int32_t FICA_clear_buf(uint8_t *pbuf, uint8_t initval)
{
    // Read an Erase Page size (4K for this flash)
    for (int i = 0; i < EXT_FLASH_ERASE_PAGE; i++)
        *pbuf++ = initval;

    return (SLN_FLASH_NO_ERROR);
}

//! @brief Read the FICA database into a buffer
// Usually done just prior to modify, write
__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE")))
int32_t FICA_read_db()
{
	uint8_t *bufptr = ProgExtAppImgBuf;

	for(uint32_t runaddr = FICA_START_ADDR; runaddr < (FICA_START_ADDR + FICA_TABLE_SIZE); runaddr+=EXT_FLASH_PROGRAM_PAGE, bufptr+=EXT_FLASH_PROGRAM_PAGE)
	{
		if(SLN_Read_Flash_At_Address(runaddr, bufptr, EXT_FLASH_PROGRAM_PAGE)!=SLN_FLASH_NO_ERROR)
			return(SLN_FLASH_ERROR);
	}
	return(SLN_FLASH_NO_ERROR);
}

//! @brief Write the FICA database from the buffer
__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE")))
int32_t FICA_write_db()
{
	uint8_t *bufptr = ProgExtAppImgBuf;

	// Erase the 1st sector of the FICA ICA area
	if(SLN_Erase_Sector(FICA_START_ADDR)!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	for(uint32_t runaddr = FICA_START_ADDR; runaddr < (FICA_START_ADDR + FICA_TABLE_SIZE); runaddr+=EXT_FLASH_PROGRAM_PAGE, bufptr+=EXT_FLASH_PROGRAM_PAGE)
	{
		// Write the entire FICA buffer
		if(SLN_Write_Flash_At_Address(runaddr, (uint8_t *)bufptr)!=SLN_FLASH_NO_ERROR)
			return(SLN_FLASH_ERROR);
	}

	// Read it back into the Info Structure so FICA is using what was written
	if(FICA_read_db() != SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	return(SLN_FLASH_NO_ERROR);
}

//! @brief Writes the passed buffer to external flash starting at address passed by offset
__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE"))) int32_t FICA_write_buf(uint32_t offset, uint32_t len, void *buf)
{
    uint8_t *pbuf = (uint8_t *)buf;

    offset -= FICA_START_ADDR;

    if (pbuf == NULL)
        return (SLN_FLASH_ERROR);
    for (int i = 0; i < len; i++)
    {
        if ((offset + i) >= EXT_FLASH_ERASE_PAGE)
            return (SLN_FLASH_ERROR);
        ProgExtAppImgBuf[offset + i] = *pbuf++;
    }
    return (SLN_FLASH_NO_ERROR);
}


//! @brief Get the field flash address based on the image type and field offset
__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE")))
int32_t FICA_write_full_page(uint32_t offset, uint32_t progpagesize, uint32_t erasepagersize, uint32_t *pdatabuf)
{
	uint32_t run_offset = 0;

	FICA_read_for_debug(offset, EXT_FLASH_ERASE_PAGE);

	// Erase page before writing
	if(SLN_Erase_Sector(offset)!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	FICA_read_for_debug(offset, EXT_FLASH_ERASE_PAGE);

	while(run_offset < EXT_FLASH_ERASE_PAGE)
	{
		if(SLN_Write_Flash_At_Address(offset + run_offset, (uint8_t *)pdatabuf)!=SLN_FLASH_NO_ERROR)
			return(SLN_FLASH_ERROR);

		pdatabuf += EXT_FLASH_PROGRAM_PAGE;
		run_offset += EXT_FLASH_PROGRAM_PAGE;

	}
	FICA_read_for_debug(offset, EXT_FLASH_ERASE_PAGE);

	return(SLN_FLASH_NO_ERROR);

}

//! @brief Reads the external flash to the passed buffer starting at address passed by offset
__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE"))) int32_t FICA_read_buf(uint32_t offset, uint32_t len, void *buf)
{
    uint8_t *pbuf = (uint8_t *)buf;

    if (pbuf == NULL)
        return (SLN_FLASH_ERROR);
    for (int i = 0; i < len; i++)
    {
        if ((offset + i) >= EXT_FLASH_ERASE_PAGE)
            return (SLN_FLASH_ERROR);
        *pbuf++ = ProgExtAppImgBuf[offset + i];
    }
    return (SLN_FLASH_NO_ERROR);
}

//! @brief Get the field flash address based on the image type and field offset
__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE"))) int32_t FICA_get_field_addr(uint32_t imgtype,
                                                                       uint32_t fieldoffset,
                                                                       uint32_t *pfieldaddr)
{
    uint32_t recnum = 0;

    // Check if the image type is valid or no image type was passed in
    if (imgtype == FICA_IMG_TYPE_NONE)
    {
        // No image type passed in, desired field is at the start of the ICA
        *pfieldaddr = FICA_START_ADDR + fieldoffset;
    }
    else
    {
        // Get the record number based on the image type. Image type starts at 1, recnum starts at zero
        if (imgtype > 0)
            recnum = imgtype - 1;

        // Valid image type, calculate the field address
        *pfieldaddr = FICA_START_ADDR + FICA_ICA_DEFINITION_SIZE + (recnum * FICA_RECORD_SIZE) + fieldoffset;
    }
    return (SLN_FLASH_NO_ERROR);
}


//! @brief Write a value to a specific image field, write it to the external flash
__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE")))
int32_t FICA_write_field(uint32_t imgtype, uint32_t fieldoffset, uint32_t val)
{

	// Check if its a valid image type
	if(imgtype>FICA_NUM_IMG_TYPES)
		return(SLN_FLASH_ERROR);

	if(FICA_read_db()!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	FICA_write_field_no_save(imgtype, fieldoffset, val);

	if(FICA_write_db()!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

//#define FICA_VALIDATE_WRITE 1

#ifdef FICA_VALIDATE_WRITE

    uint32_t readval = 0;

    if (FICA_read_field(imgtype, fieldoffset, &readval) != SLN_FLASH_NO_ERROR)
        return (SLN_FLASH_ERROR);

    if (val != readval)
        return (SLN_FLASH_ERROR);
#endif

    return (SLN_FLASH_NO_ERROR);
}


//! @brief Write a value to a specific image field, writes to buffer only, not external flash
__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE")))
int32_t FICA_write_field_no_save(uint32_t imgtype, uint32_t fieldoffset, uint32_t val)
{
	uint32_t fieldaddr = 0;
	uint32_t *pfieldaddr = &fieldaddr;

	uint8_t valbuf[100]={0};
	uint8_t *pvalbuf=valbuf;

	uint8_t *pval = (uint8_t *)&val;

	valbuf[0] = *pval++;
	valbuf[1] = *pval++;
	valbuf[2] = *pval++;
	valbuf[3] = *pval++;

	// Check if its a valid image type
	if(imgtype>FICA_NUM_IMG_TYPES) return(SLN_FLASH_ERROR);

	// Get the image record offset based on the image type and field offset
	if(FICA_get_field_addr(imgtype, fieldoffset, pfieldaddr)!=SLN_FLASH_NO_ERROR)
		return SLN_FLASH_ERROR;

	if(FICA_write_buf(fieldaddr, FICA_FIELD_SIZE, (void *)pvalbuf)!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	return(SLN_FLASH_NO_ERROR);
}

//! @brief Read any field given the passed image type and field offset
__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE")))
int32_t FICA_read_field(uint32_t imgtype, uint32_t field_offset, uint32_t *pfieldval)
{
	uint32_t fieldaddr = 0;
	uint32_t *pfieldaddr = &fieldaddr;

	// Check if its a valid image type
	if(imgtype>FICA_NUM_IMG_TYPES) return(SLN_FLASH_ERROR);

	// Get the field address based on the image type and field offset
	if(FICA_get_field_addr(imgtype, field_offset, pfieldaddr)!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	// Read the field value from the SPI Flash
	if(SLN_Read_Flash_At_Address(fieldaddr, (uint8_t *)pfieldval, FICA_FIELD_SIZE)!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	return(SLN_FLASH_NO_ERROR);
}

//! @brief Get the application external flash start address, given the passed image type
__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE")))
int32_t FICA_get_app_img_start_addr(uint32_t imgtype, uint32_t *startaddr)
{
	*startaddr = 0;
	switch(imgtype)
	{
		case FICA_IMG_TYPE_APP_A: *startaddr = FICA_IMG_APP_A_ADDR; break;
		case FICA_IMG_TYPE_APP_B: *startaddr = FICA_IMG_APP_B_ADDR; break;
		case FICA_IMG_TYPE_APP_FAC: *startaddr = FICA_IMG_APP_FAC_ADDR; break;
		default: break;
	}
	if(*startaddr==0)
		return(SLN_FLASH_ERROR);
	return(SLN_FLASH_NO_ERROR);
}

//! @brief Get the application external flash start address, given the passed image type
__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE")))
int32_t FICA_get_app_img_max_size(uint32_t imgtype, uint32_t *maximgsize)
{
	*maximgsize = 0;
	switch(imgtype)
	{
		case FICA_IMG_TYPE_APP_A: *maximgsize = FICA_IMG_APP_A_SIZE; break;
		case FICA_IMG_TYPE_APP_B: *maximgsize = FICA_IMG_APP_B_SIZE; break;
		case FICA_IMG_TYPE_APP_FAC: *maximgsize = FICA_IMG_APP_FAC_SIZE; break;
		default: break;
	}
	if(*maximgsize==0)
		return(SLN_FLASH_ERROR);
	return(SLN_FLASH_NO_ERROR);
}


//! @brief Get the Application Image Length
__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE")))
int32_t FICA_get_app_img_len(uint32_t imgtype, uint32_t *plen)
{
	*plen=0;

	if(FICA_read_field(imgtype, FICA_OFFSET_IMG_SIZE, plen)!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	return(SLN_FLASH_NO_ERROR);
}


//! @brief Write the image to external memory
__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE")))
int32_t FICA_write_image_info(uint32_t imgtype, uint32_t imgfmt, uint32_t len)
{
	uint32_t startaddr = 0;

	// Check if its a valid image type
	if(imgtype>FICA_NUM_IMG_TYPES)
		return(SLN_FLASH_ERROR);

	// Set the start address of the image based on the image type
	if(FICA_get_app_img_start_addr(imgtype, &startaddr)!= SLN_FLASH_NO_ERROR)
			return(SLN_FLASH_ERROR);

	if(FICA_read_db() != SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	// Write the rest of the record values for this image types ICA (Image Config Area)
	if(FICA_write_field_no_save(imgtype, FICA_OFFSET_IMG_DESC, FICA_IMG_DESC_ID)!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	if(FICA_write_field_no_save(imgtype, FICA_OFFSET_IMG_TYPE, imgtype)!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	// Write Image Start Address into FICA Record for this image type
	if(FICA_write_field_no_save(imgtype, FICA_OFFSET_IMG_START, startaddr)!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	// Write Image Size into FICA Record for this image type
	if(FICA_write_field_no_save(imgtype, FICA_OFFSET_IMG_SIZE, len)!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	// Write Image Type into FICA Record for this image type
	if(FICA_write_field_no_save(imgtype, FICA_OFFSET_IMG_FMT, imgfmt)!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	// Write the info to the flash
	if(FICA_write_db()!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

#ifdef UART_DEBUG
    sprintf(pmsg, "Flash ICA record written successfully\r\n");
    PrintUart(TERMINAL_UART, (uint8_t *)pmsg, strlen(pmsg));
#endif

    return (SLN_FLASH_NO_ERROR);
}

//! @brief Read ICA record from the buffer
__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE")))
int32_t FICA_read_record(uint32_t imgtype, FICA_Record *pimgrec)
{
	// Make sure pointer to image record is not NULL
	if(pimgrec==NULL) return(SLN_FLASH_ERROR);

	// Check if its a valid image type
	if(imgtype==0 || imgtype>ProgExtAppImgMaxSize)
		return(SLN_FLASH_ERROR);

	// Calculate the offset in the ICA buffer
	uint32_t bufaddr = FICA_ICA_DEFINITION_SIZE + ((imgtype-1) * sizeof(FICA_Record));

	// Read the buffer data into the FICA record variable
	if(FICA_read_buf(bufaddr, sizeof(FICA_Record), pimgrec)!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	return(SLN_FLASH_NO_ERROR);
}

//! @brief Write an ICA record to the FICA buffer and flash
__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE")))
int32_t FICA_write_record(uint32_t imgtype, FICA_Record *pimgrec)
{
	// Make sure pointer to image record is not NULL
	if(pimgrec==NULL) return(SLN_FLASH_ERROR);

	// Check if its a valid image type
	if(imgtype==0 || imgtype>ProgExtAppImgMaxSize)
		return(SLN_FLASH_ERROR);

	// Calculate the offset in the ICA buffer
	uint32_t bufaddr = FICA_ICA_DEFINITION_SIZE + ((imgtype-1) * sizeof(FICA_Record));

	if(FICA_read_db()!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	// Write FICA record variable data to the buffer
	if(FICA_write_buf(bufaddr, sizeof(FICA_Record), pimgrec)!=SLN_FLASH_NO_ERROR)
			return(SLN_FLASH_ERROR);

	// Write the info to the flash
	if(FICA_write_db()!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

#ifdef UART_DEBUG
    sprintf(pmsg, "Flash ICA record written successfully\r\n");
    PrintUart(TERMINAL_UART, (uint8_t *)pmsg, strlen(pmsg));
#endif

    return (SLN_FLASH_NO_ERROR);
}

//! @brief Is Flash Image Config Area Initialized ?
__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE")))
bool is_FICA_initialized(void)
{

	uint32_t fieldval=0;
	uint32_t *pfieldval = &fieldval;
	uint32_t verval=0;
	uint32_t *pverval = &verval;
	uint32_t fieldaddr = 0;


#ifdef FORCE_FICA_TABLE_INIT
	static bool firsttime = true;
	if(firsttime) {	firsttime = false; return(false); }
#endif

    // Get the field address based on the image type and field offset
    if (FICA_get_field_addr(FICA_IMG_TYPE_NONE, FICA_OFFSET_ICA_DESC, &fieldaddr) != SLN_FLASH_NO_ERROR)
        return (false);

    // See if the Start ICA Identifier is there
    if (SLN_Read_Flash_At_Address(fieldaddr, (uint8_t *)pfieldval, FICA_FIELD_SIZE) != SLN_FLASH_NO_ERROR)
        return (false);

    // Get the field version based on the image type and field offset
    if (FICA_get_field_addr(FICA_IMG_TYPE_NONE, FICA_OFFSET_ICA_VER, &fieldaddr) != SLN_FLASH_NO_ERROR)
        return (false);

    // See if the Start ICA Identifier is there
    if (SLN_Read_Flash_At_Address(fieldaddr, (uint8_t *)pverval, FICA_FIELD_SIZE) != SLN_FLASH_NO_ERROR)
        return (false);

    // Check ICA Start Identifier, value should be 0x5A5A5A5A
    if (fieldval == FICA_ICA_DESC && verval == FICA_VER)
    {
#ifdef UART_DEBUG
        sprintf(pmsg, "Flash ICA already initialized\r\n");
        PrintUart(TERMINAL_UART, (uint8_t *)pmsg, strlen(pmsg));
#endif
        return (true);
    }
#ifdef UART_DEBUG
    sprintf(pmsg, "Flash ICA needs initialization\r\n");
    PrintUart(TERMINAL_UART, (uint8_t *)pmsg, strlen(pmsg));
#endif
    return (false);
}

//! @brief Check if FICA is initialized, initialize it if needed
__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE"))) int32_t FICA_initialize(void)
{
    if (FICA_verify_ext_flash() != SLN_FLASH_NO_ERROR)
        return (SLN_FLASH_ERROR);

#ifdef UART_DEBUG
    sprintf(pmsg, "\r\nChecking Image Config Area (ICA) initialization\r\n");
    PrintUart(TERMINAL_UART, (uint8_t *)pmsg, strlen(pmsg));
#endif

    FICA_RECORD_SIZE = sizeof(FICA_Record);

    // If its already initialized, return no error
    if (is_FICA_initialized())
    {
        if (FICA_read_db() != SLN_FLASH_NO_ERROR)
            return (SLN_FLASH_ERROR);

        return (SLN_FLASH_NO_ERROR);
    }

#ifdef UART_DEBUG
    sprintf(msg, "Flash ICA initialization started\r\n");
    PrintUart(TERMINAL_UART, (uint8_t *)msg, strlen(msg));
#endif

	FICA_clear_buf(ProgExtAppImgBuf, 0);

//	if(SLN_Erase_Sector(FICA_START_ADDR)!=SLN_FLASH_NO_ERROR)
//					return(SLN_FLASH_ERROR);

	if(FICA_write_db()!=SLN_FLASH_NO_ERROR)
			return(SLN_FLASH_ERROR);

	// ICA is not initialized, so initialize it, what are you waiting for, xmas, what the...
	// Write the ICA Start ID 0x5A5A5A5A
	if(FICA_write_field(FICA_IMG_TYPE_NONE, FICA_OFFSET_ICA_DESC, FICA_ICA_DESC)!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	// Write the ICA Version Number
	if(FICA_write_field(FICA_IMG_TYPE_NONE, FICA_OFFSET_ICA_VER, FICA_VER)!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	// Write the Factory Application type as the current application type
	if(FICA_write_field(FICA_IMG_TYPE_NONE, FICA_OFFSET_ICA_CUR_TYPE, FICA_IMG_TYPE_APP_FAC)!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	// Set OTA Bit so OTA update shows not pending
	if(FICA_write_field(FICA_IMG_TYPE_NONE, FICA_OFFSET_ICA_COMM, ICA_COMM_AIS_OTA_BIT)!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	// Initialize all the records
	for(int imgtype = 1; imgtype <= FICA_NUM_IMG_TYPES; imgtype++)
	{
		// Write defaults to all Image Records
		if(FICA_write_image_info(imgtype, FICA_IMG_FMT_NONE, 0)!=SLN_FLASH_NO_ERROR)
			return(SLN_FLASH_ERROR);
	}

#ifdef UART_DEBUG
    sprintf(msg, "Flash ICA initialization complete\r\n");
    PrintUart(TERMINAL_UART, (uint8_t *)msg, strlen(msg));
#endif

    return (SLN_FLASH_NO_ERROR);
}

//! @brief Initialize External Flash driver
__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE"))) int32_t FICA_verify_ext_flash(void)
{
    uint8_t id = 0x7E;
    uint8_t *pid = (uint8_t *)&id;

#ifdef UART_DEBUG
    sprintf(pmsg, "**** VERSION %s       ****\n\n", TEST_VERSION);
    PrintUart(TERMINAL_UART, (uint8_t *)pmsg, strlen(pmsg));

    strcpy(pmsg, "RPK SPI FLASH Init\n");
    PrintUart(TERMINAL_UART, (uint8_t *)pmsg, strlen(pmsg));
#endif

    // Read flash vendor ID
    if (FICA_read_hyper_flash_id(pid) != SLN_FLASH_NO_ERROR)
    {
#ifdef UART_DEBUG
        strcpy(pmsg, "   Can not find any SPI Flash device!\r\n");
        PrintUart(TERMINAL_UART, (uint8_t *)pmsg, strlen(pmsg));
#endif
        return (SLN_FLASH_ERROR);
    }
#ifdef UART_DEBUG
    else
    {
        strcpy(pmsg, "   found SPI FLASH\n");
        PrintUart(TERMINAL_UART, (uint8_t *)pmsg, strlen(pmsg));
        sprintf(pmsg, "   device ID : 0x%X.0x%X.0x%X.0x%X\n------------------------------\n", pid[0], pid[1], pid[2],
                pid[3]);
        PrintUart(TERMINAL_UART, (uint8_t *)pmsg, strlen(pmsg));
    }
    if (*pid == 0x12345678)
#endif

        return (SLN_FLASH_NO_ERROR);
}

__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE"))) int32_t FICA_read_hyper_flash_id(uint8_t *pid)
{
    uint8_t id;
    int32_t ret = SLN_FLASH_NO_ERROR;
    uint32_t irqState;

    irqState = DisableGlobalIRQ();

    SCB_DisableDCache();

    // Read hyperflash ID
    ret = flexspi_nor_hyperflash_id(FLEXSPI, &id);

	if(SLN_FLASH_NO_ERROR != ret)
	{
		ret = SLN_FLASH_ERROR;
	}

	if (SLN_FLASH_NO_ERROR == ret)
	{
		// Check if it matches the intended device
		if(*pid != id)
		{
			ret = SLN_FLASH_ERROR;
		}
	}

    SCB_EnableDCache();

    EnableGlobalIRQ(irqState);
    /* Flush pipeline to allow pending interrupts take place
     * before starting next loop */
    __ISB();

	return ret;
}

//! @brief Resets this MCU
__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE"))) void reset_mcu(void)
{
    NVIC_SystemReset();
    while (1)
        ;
}

/************************************************************************************
 *  Updates the CRC based on the received data to process.
 *  Updates the global CRC value. This was determined to be optimal from a resource
 *  consumption POV.
 *
 ************************************************************************************/
__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE"))) uint16_t FICA_compute_chunk_CRC(uint8_t *pData,
                                                                           uint16_t lenData,
                                                                           uint16_t crcValueOld)
{
    uint8_t i;

    while (lenData--)
    {
        crcValueOld ^= (uint16_t)((uint16_t)*pData++ << 8);
        for (i = 0; i < 8; ++i)
        {
            if (crcValueOld & 0x8000)
            {
                crcValueOld = (crcValueOld << 1) ^ 0x1021U;
            }
            else
            {
                crcValueOld = crcValueOld << 1;
            }
        }
    }
    return crcValueOld;
}


//! @brief Gets the Processor Communication Flag
__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE")))
int32_t FICA_get_comm_flag(uint32_t comflag, bool *flagstate)
{
	uint32_t retval = 0;
	if(FICA_read_field(FICA_IMG_TYPE_NONE, FICA_OFFSET_ICA_COMM, &retval)!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	if((retval & comflag)>0)
		*flagstate = true;
	else
		*flagstate = false;

	return(SLN_FLASH_NO_ERROR);
}


//! @brief Sets the Processor Communication Flag
__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE")))
int32_t FICA_set_comm_flag(uint32_t comflag, bool flagstate)
{
	uint32_t readflags = 0;
	uint32_t cmpval = 0;

	if(FICA_read_field(FICA_IMG_TYPE_NONE, FICA_OFFSET_ICA_COMM, &readflags)!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	cmpval = comflag & readflags;
	if(!flagstate && cmpval>0)
	{
		readflags &= ~comflag;
		if(FICA_write_field(FICA_IMG_TYPE_NONE, FICA_OFFSET_ICA_COMM, readflags)!=SLN_FLASH_NO_ERROR)
			return(SLN_FLASH_ERROR);
	}
	else if(flagstate && cmpval==0)
	{
		readflags |= comflag;
		if(FICA_write_field(FICA_IMG_TYPE_NONE, FICA_OFFSET_ICA_COMM, readflags)!=SLN_FLASH_NO_ERROR)
			return(SLN_FLASH_ERROR);
	}
	return(SLN_FLASH_NO_ERROR);
}


__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE")))
int32_t FICA_read_for_debug(uint32_t addr, uint32_t len)
{

#ifdef DEBUG_FICA

	FICA_clear_buf(tdata,0xFF);

	if(SLN_Read_Flash_At_Address(addr, (uint8_t *)tdata, EXT_FLASH_ERASE_PAGE)!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	for(uint32_t i=0; i<EXT_FLASH_ERASE_PAGE; i++)
	{
		if(pProgExtAppImgBuf[i]!=tdata[i])
			ErrorCnt+=1;
	}
#endif

    if (ErrorCnt)
        return (SLN_FLASH_ERROR);

    return (SLN_FLASH_NO_ERROR);
}

__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE"))) int32_t FICA_GetCurBootStartAddr(uint32_t *paddr)
{
    *paddr = FICA_IMG_BL_FAC_ADDR;
    return (SLN_FLASH_NO_ERROR);
}


__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE")))
int32_t FICA_GetCurAppStartType(uint32_t *pimgtype)
{
	*pimgtype = FICA_IMG_TYPE_APP_FAC;

	FICA_initialize();

	// Read the address of the current application programmed in flash
	FICA_read_field(FICA_IMG_TYPE_NONE, FICA_OFFSET_ICA_CUR_TYPE, pimgtype);

	if ((*pimgtype!=FICA_IMG_TYPE_APP_A) && (*pimgtype!=FICA_IMG_TYPE_APP_B) && (*pimgtype!=FICA_IMG_TYPE_APP_FAC))
	{
		// No app found in flash, write the default address

		*pimgtype = FICA_IMG_TYPE_APP_FAC;

		// No Application set, write it to flash
		if(FICA_write_field(FICA_IMG_TYPE_NONE, FICA_OFFSET_ICA_CUR_TYPE, *pimgtype)!=SLN_FLASH_NO_ERROR)
			return(SLN_FLASH_ERROR);
	}
	return(SLN_FLASH_NO_ERROR);
}


__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE")))
int32_t FICA_GetNewAppStartAddr(uint32_t *paddr)
{
	*paddr = ProgExtAppImgStartAddr;
	return(SLN_FLASH_NO_ERROR);
}


__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE")))
int32_t FICA_SetCurAppStartType(uint32_t imgtype)
{
	// write it to flash
	if(FICA_write_field(FICA_IMG_TYPE_NONE, FICA_OFFSET_ICA_CUR_TYPE, imgtype)!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	return(SLN_FLASH_NO_ERROR);
}


__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE")))
int32_t FICA_GetImgTypeFromAddr(uint32_t appaddr, uint32_t *imgtype)
{
	// Find the img type given the img address
	if(appaddr == FICA_IMG_APP_A_ADDR)
		*imgtype = FICA_IMG_TYPE_APP_A;
	else if(appaddr == FICA_IMG_APP_B_ADDR)
		*imgtype = FICA_IMG_TYPE_APP_B;
	else if(appaddr == FICA_IMG_APP_FAC_ADDR)
		*imgtype = FICA_IMG_TYPE_APP_FAC;
	else
		return(SLN_FLASH_ERROR);

	return(SLN_FLASH_NO_ERROR);
}


//! @brief Check if the OTA bit is set in the flash, main app will set if OTA is desired
bool FICA_is_OTA_FlashBitCleared()
{
	bool otaflag = false;

	FICA_initialize();

#ifdef TESTING_OTA
	// if testing, clear the bit so it will go into AWS OTA mode
	Clear_OTA_FlashBit();
#endif

	// Read the OTA bit from the flash
	if(FICA_get_comm_flag(ICA_COMM_AIS_OTA_BIT, &otaflag)!=SLN_FLASH_NO_ERROR)
			return(false);

	// if the bit is cleared, return true
	if(otaflag==false)
		return(true);
	return(false);
}

//! @brief Set OTA bit in the flash, not used in bootloader, here for reference
int32_t FICA_Set_OTA_FlashBit()
{
	bool otaflag = true;

	FICA_initialize();

	// Set the OTA bit in the flash
	if(FICA_set_comm_flag(ICA_COMM_AIS_OTA_BIT, otaflag)!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	return(SLN_FLASH_NO_ERROR);
}


//! @brief Set OTA bit in the flash, not used in bootloader, here for reference
int32_t FICA_Clear_OTA_FlashBit()
{
	bool otaflag = false;

	FICA_initialize();

	// Set the OTA bit in the flash
	if(FICA_set_comm_flag(ICA_COMM_AIS_OTA_BIT, otaflag)!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	return(SLN_FLASH_NO_ERROR);
}


//! @}

////////////////////////////////////////////////////////////////////////////////
// EOF
////////////////////////////////////////////////////////////////////////////////
