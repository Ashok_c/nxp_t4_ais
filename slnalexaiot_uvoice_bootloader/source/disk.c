/*
 * Copyright (c) 2015 - 2016, Freescale Semiconductor, Inc.
 * Copyright 2016 - 2017 NXP
 * All rights reserved.
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include "usb_device_config.h"
#include "usb.h"
#include "usb_device.h"

#include "usb_device_class.h"
#include "usb_device_msc.h"
#include "usb_device_ch9.h"
#include "usb_device_descriptor.h"
#include "disk.h"

#include "fsl_device_registers.h"
#include "clock_config.h"
#include "board.h"
#include "fsl_debug_console.h"

#include <stdio.h>
#include <stdlib.h>
#if (defined(FSL_FEATURE_SOC_SYSMPU_COUNT) && (FSL_FEATURE_SOC_SYSMPU_COUNT > 0U))
#include "fsl_sysmpu.h"
#endif /* FSL_FEATURE_SOC_SYSMPU_COUNT */

#if defined(USB_DEVICE_CONFIG_EHCI) && (USB_DEVICE_CONFIG_EHCI > 0)
#include "usb_phy.h"
#endif

#include "pin_mux.h"
#include "bootloader.h"
#include "flash_ica_driver.h"
#include "sln_RT10xx_RGB_LED_driver.h"
/*******************************************************************************
 * Definitions
 ******************************************************************************/
/*******************************************************************************
  * Prototypes
  ******************************************************************************/
void BOARD_InitHardware(void);
void USB_DeviceClockInit(void);
void USB_DeviceIsrEnable(void);
#if USB_DEVICE_CONFIG_USE_TASK
void USB_DeviceTaskFn(void *deviceHandle);
#endif

/*******************************************************************************
 * Variables
 ******************************************************************************/

volatile bool MSD_Done = false;
volatile uint64_t CurProgTime = 0;


USB_DMA_INIT_DATA_ALIGN(USB_DATA_ALIGN_SIZE)
usb_device_inquiry_data_fromat_struct_t g_InquiryInfo = {
    (USB_DEVICE_MSC_UFI_PERIPHERAL_QUALIFIER << USB_DEVICE_MSC_UFI_PERIPHERAL_QUALIFIER_SHIFT) |
        USB_DEVICE_MSC_UFI_PERIPHERAL_DEVICE_TYPE,
    (uint8_t)(USB_DEVICE_MSC_UFI_REMOVABLE_MEDIUM_BIT << USB_DEVICE_MSC_UFI_REMOVABLE_MEDIUM_BIT_SHIFT),
    USB_DEVICE_MSC_UFI_VERSIONS,
    0x02,
    USB_DEVICE_MSC_UFI_ADDITIONAL_LENGTH,
    {0x00, 0x00, 0x00},
    {'N', 'X', 'P', ' ', 'S', 'E', 'M', 'I'},
    {'N', 'X', 'P', ' ', 'M', 'A', 'S', 'S', ' ', 'S', 'T', 'O', 'R', 'A', 'G', 'E'},
    {'0', '0', '0', '1'}};
USB_DMA_INIT_DATA_ALIGN(USB_DATA_ALIGN_SIZE)
usb_device_mode_parameters_header_struct_t g_ModeParametersHeader = {
    /*refer to ufi spec mode parameter header*/
    0x0000, /*!< Mode Data Length*/
    0x00,   /*!<Default medium type (current mounted medium type)*/
    0x00,   /*!MODE SENSE command, a Write Protected bit of zero indicates the medium is write enabled*/
    {0x00, 0x00, 0x00, 0x00} /*!<This bit should be set to zero*/
};
/* Data structure of msc device, store the information ,such as class handle */
USB_DMA_NONINIT_DATA_ALIGN(USB_DATA_ALIGN_SIZE) static uint8_t s_StorageDisk[DISK_PHYSICAL_SIZE_NORMAL]
= {
		0xEB,0x3C,0x90,0x4D,	0x53,0x44,0x4F,0x53,	0x35,0x2E,0x30,0x00,	0x02,0x08,0x08,0x00,	0x02,0x00,0x02,0x00,	0x50,0xF8,0x08,0x00,	0x3F,0x00,0xFF,0x00,	0x00,0x00,0x00,0x00,	0x00,0x00,0x00,0x00,	0x80,0x00,0x29,0xE2,	0xA2,0x3F,0xF6,0x4E,	0x4F,0x20,0x4E,0x41,	0x4D,0x45,0x20,0x20,	0x20,0x20,0x46,0x41,
		0x54,0x31,0x32,0x20,	0x20,0x20,0x33,0xC9,	0x8E,0xD1,0xBC,0xF0,	0x7B,0x8E,0xD9,0xB8,	0x00,0x20,0x8E,0xC0,	0xFC,0xBD,0x00,0x7C,	0x38,0x4E,0x24,0x7D,	0x24,0x8B,0xC1,0x99,	0xE8,0x3C,0x01,0x72,	0x1C,0x83,0xEB,0x3A,	0x66,0xA1,0x1C,0x7C,	0x26,0x66,0x3B,0x07,	0x26,0x8A,0x57,0xFC,	0x75,0x06,0x80,0xCA,
		0x02,0x88,0x56,0x02,	0x80,0xC3,0x10,0x73,	0xEB,0x33,0xC9,0x8A,	0x46,0x10,0x98,0xF7,	0x66,0x16,0x03,0x46,	0x1C,0x13,0x56,0x1E,	0x03,0x46,0x0E,0x13,	0xD1,0x8B,0x76,0x11,	0x60,0x89,0x46,0xFC,	0x89,0x56,0xFE,0xB8,	0x20,0x00,0xF7,0xE6,	0x8B,0x5E,0x0B,0x03,	0xC3,0x48,0xF7,0xF3,	0x01,0x46,0xFC,0x11,
		0x4E,0xFE,0x61,0xBF,	0x00,0x00,0xE8,0xE6,	0x00,0x72,0x39,0x26,	0x38,0x2D,0x74,0x17,	0x60,0xB1,0x0B,0xBE,	0xA1,0x7D,0xF3,0xA6,	0x61,0x74,0x32,0x4E,	0x74,0x09,0x83,0xC7,	0x20,0x3B,0xFB,0x72,	0xE6,0xEB,0xDC,0xA0,	0xFB,0x7D,0xB4,0x7D,	0x8B,0xF0,0xAC,0x98,	0x40,0x74,0x0C,0x48,	0x74,0x13,0xB4,0x0E,
		0xBB,0x07,0x00,0xCD,	0x10,0xEB,0xEF,0xA0,	0xFD,0x7D,0xEB,0xE6,	0xA0,0xFC,0x7D,0xEB,	0xE1,0xCD,0x16,0xCD,	0x19,0x26,0x8B,0x55,	0x1A,0x52,0xB0,0x01,	0xBB,0x00,0x00,0xE8,	0x3B,0x00,0x72,0xE8,	0x5B,0x8A,0x56,0x24,	0xBE,0x0B,0x7C,0x8B,	0xFC,0xC7,0x46,0xF0,	0x3D,0x7D,0xC7,0x46,	0xF4,0x29,0x7D,0x8C,
		0xD9,0x89,0x4E,0xF2,	0x89,0x4E,0xF6,0xC6,	0x06,0x96,0x7D,0xCB,	0xEA,0x03,0x00,0x00,	0x20,0x0F,0xB6,0xC8,	0x66,0x8B,0x46,0xF8,	0x66,0x03,0x46,0x1C,	0x66,0x8B,0xD0,0x66,	0xC1,0xEA,0x10,0xEB,	0x5E,0x0F,0xB6,0xC8,	0x4A,0x4A,0x8A,0x46,	0x0D,0x32,0xE4,0xF7,	0xE2,0x03,0x46,0xFC,	0x13,0x56,0xFE,0xEB,
		0x4A,0x52,0x50,0x06,	0x53,0x6A,0x01,0x6A,	0x10,0x91,0x8B,0x46,	0x18,0x96,0x92,0x33,	0xD2,0xF7,0xF6,0x91,	0xF7,0xF6,0x42,0x87,	0xCA,0xF7,0x76,0x1A,	0x8A,0xF2,0x8A,0xE8,	0xC0,0xCC,0x02,0x0A,	0xCC,0xB8,0x01,0x02,	0x80,0x7E,0x02,0x0E,	0x75,0x04,0xB4,0x42,	0x8B,0xF4,0x8A,0x56,	0x24,0xCD,0x13,0x61,
		0x61,0x72,0x0B,0x40,	0x75,0x01,0x42,0x03,	0x5E,0x0B,0x49,0x75,	0x06,0xF8,0xC3,0x41,	0xBB,0x00,0x00,0x60,	0x66,0x6A,0x00,0xEB,	0xB0,0x42,0x4F,0x4F,	0x54,0x4D,0x47,0x52,	0x20,0x20,0x20,0x20,	0x0D,0x0A,0x52,0x65,	0x6D,0x6F,0x76,0x65,	0x20,0x64,0x69,0x73,	0x6B,0x73,0x20,0x6F,	0x72,0x20,0x6F,0x74,
		0x68,0x65,0x72,0x20,	0x6D,0x65,0x64,0x69,	0x61,0x2E,0xFF,0x0D,	0x0A,0x44,0x69,0x73,	0x6B,0x20,0x65,0x72,	0x72,0x6F,0x72,0xFF,	0x0D,0x0A,0x50,0x72,	0x65,0x73,0x73,0x20,	0x61,0x6E,0x79,0x20,	0x6B,0x65,0x79,0x20,	0x74,0x6F,0x20,0x72,	0x65,0x73,0x74,0x61,	0x72,0x74,0x0D,0x0A,	0x00,0x00,0x00,0x00,
		0x00,0x00,0x00,0xAC,	0xCB,0xD8,0x55,0xAA,	0x00,0x00,0x00,0x00,	0x00,0x00,0x00,0x00,	0x00,0x00,0x00,0x00,	0x00,0x00,0x00,0x00,	0x00,0x00,0x00,0x00,	0x00,0x00,0x00,0x00,	0x00,0x00,0x00,0x00,	0x00,0x00,0x00,0x00,	0x00,0x00,0x00,0x00,	0x00,0x00,0x00,0x00,	0x00,0x00,0x00,0x00,	0x00,0x00,0x00,0x00
};


usb_msc_struct_t g_msc;

typedef struct {
    uint8_t boot_sector[11];
    /* DOS 2.0 BPB - Bios Parameter Block, 11 bytes */
    uint16_t bytes_per_sector;
    uint8_t  sectors_per_cluster;
    uint16_t reserved_logical_sectors;
    uint8_t  num_fats;
    uint16_t max_root_dir_entries;
    uint16_t total_logical_sectors;
    uint8_t  media_descriptor;
    uint16_t logical_sectors_per_fat;
    /* DOS 3.31 BPB - Bios Parameter Block, 12 bytes */
    uint16_t physical_sectors_per_track;
    uint16_t heads;
    uint32_t hidden_sectors;
    uint32_t big_sectors_on_drive;
    /* Extended BIOS Parameter Block, 26 bytes */
    uint8_t  physical_drive_number;
    uint8_t  not_used;
    uint8_t  boot_record_signature;
    uint32_t volume_id;
    char     volume_label[11];
    char     file_system_type[8];
    /* bootstrap data in bytes 62-509 */
    uint8_t  bootstrap[448];
    /* These entries in place of bootstrap code are the *nix partitions */
    //uint8_t  partition_one[16];
    //uint8_t  partition_two[16];
    //uint8_t  partition_three[16];
    //uint8_t  partition_four[16];
    /* Mandatory value at bytes 510-511, must be 0xaa55 */
    uint16_t signature;
} __attribute__((packed)) mbr_t;

static const mbr_t mbr_init = {
    /*uint8_t[11]*/.boot_sector = {
        0xEB, 0x3C, 0x90,
        'M', 'S', 'D', '0', 'S', '4', '.', '1' // OEM Name in text (8 chars max)
    },
    /*uint16_t*/.bytes_per_sector           = 0x0200,       // 512 bytes per sector
    /*uint8_t */.sectors_per_cluster        = 0x08,         // 4k cluster
    /*uint16_t*/.reserved_logical_sectors   = 0x0001,       // mbr is 1 sector
    /*uint8_t */.num_fats                   = 0x01,         // 1 FAT
    /*uint16_t*/.max_root_dir_entries       = 0x0020,       // 32 dir entries (max)
    /*uint16_t*/.total_logical_sectors      = 0x5000,       // sector size * # of sectors = drive size
    /*uint8_t */.media_descriptor           = 0xf0,         // fixed disc = F8, removable = F0
    /*uint16_t*/.logical_sectors_per_fat    = 0x0001,       // FAT is 1k - ToDO:need to edit this
    /*uint16_t*/.physical_sectors_per_track = 0x0001,       // flat
    /*uint16_t*/.heads                      = 0x0001,       // flat
    /*uint32_t*/.hidden_sectors             = 0x00000000,   // before mbt, 0
    /*uint32_t*/.big_sectors_on_drive       = 0x00000000,   // 4k sector. not using large clusters
    /*uint8_t */.physical_drive_number      = 0x00,
    /*uint8_t */.not_used                   = 0x00,         // Current head. Linux tries to set this to 0x1
    /*uint8_t */.boot_record_signature      = 0x29,         // signature is present
    /*uint32_t*/.volume_id                  = 0x27021974,   // serial number
    // needs to match the root dir label
    /*char[11]*/.volume_label               = {'U','V','O','I','C','E',0,0,0,0,0},
    // unused by msft - just a label (FAT, FAT12, FAT16)
    /*char[8] */.file_system_type           = {'F', 'A', 'T', '3', '2', ' ', ' ', ' '},
    /* Executable boot code that starts the operating system */
    /*uint8_t[448]*/.bootstrap = {0},
    // Set signature to 0xAA55 to make drive bootable
    /*uint16_t*/.signature = 0x55AA
};




/*******************************************************************************
 * Code
 ******************************************************************************/

void USB_OTG1_IRQHandler(void)
{
    USB_DeviceEhciIsrFunction(g_msc.deviceHandle);
    /* Add for ARM errata 838869, affects Cortex-M4, Cortex-M4F Store immediate overlapping
    exception return operation might vector to incorrect interrupt */
    __DSB();
}

void USB_OTG2_IRQHandler(void)
{
    USB_DeviceEhciIsrFunction(g_msc.deviceHandle);
    /* Add for ARM errata 838869, affects Cortex-M4, Cortex-M4F Store immediate overlapping
    exception return operation might vector to incorrect interrupt */
    __DSB();
}

void USB_DeviceClockInit(void)
{
    usb_phy_config_struct_t phyConfig = {
        BOARD_USB_PHY_D_CAL, BOARD_USB_PHY_TXCAL45DP, BOARD_USB_PHY_TXCAL45DM,
    };

    if (CONTROLLER_ID == kUSB_ControllerEhci0)
    {
        CLOCK_EnableUsbhs0PhyPllClock(kCLOCK_Usbphy480M, 480000000U);
        CLOCK_EnableUsbhs0Clock(kCLOCK_Usb480M, 480000000U);
    }
    else
    {
        CLOCK_EnableUsbhs1PhyPllClock(kCLOCK_Usbphy480M, 480000000U);
        CLOCK_EnableUsbhs1Clock(kCLOCK_Usb480M, 480000000U);
    }
    USB_EhciPhyInit(CONTROLLER_ID, BOARD_XTAL0_CLK_HZ, &phyConfig);
}
void USB_DeviceIsrEnable(void)
{
    uint8_t irqNumber;

    uint8_t usbDeviceEhciIrq[] = USBHS_IRQS;
    irqNumber = usbDeviceEhciIrq[CONTROLLER_ID - kUSB_ControllerEhci0];

/* Install isr, set priority, and enable IRQ. */
    NVIC_SetPriority((IRQn_Type)irqNumber, USB_DEVICE_INTERRUPT_PRIORITY);
    EnableIRQ((IRQn_Type)irqNumber);
}
#if USB_DEVICE_CONFIG_USE_TASK
void USB_DeviceTaskFn(void *deviceHandle)
{
    USB_DeviceEhciTaskFunction(deviceHandle);
}
#endif

static uint32_t highestval = 0;
/*!
 * @brief device msc callback function.
 *
 * This function handle the disk class specified event.
 * @param handle          The USB class  handle.
 * @param event           The USB device event type.
 * @param param           The parameter of the class specific event.
 * @return kStatus_USB_Success or error.
 */
usb_status_t USB_DeviceMscCallback(class_handle_t handle, uint32_t event, void *param)
{
    usb_status_t error = kStatus_USB_Error;
    usb_device_lba_information_struct_t *lbaInformationStructure;
    usb_device_lba_app_struct_t *lbaData;
    usb_device_ufi_app_struct_t *ufi;

    switch (event)
    {
        case kUSB_DeviceMscEventReadResponse:
            lbaData = (usb_device_lba_app_struct_t *)param;
            break;
        case kUSB_DeviceMscEventWriteResponse:
            lbaData = (usb_device_lba_app_struct_t *)param;
            break;
        case kUSB_DeviceMscEventWriteRequest:
            lbaData = (usb_device_lba_app_struct_t *)param;
            /*offset is the write start address get from write command, refer to class driver*/
            if(lbaData->offset<20)
            	lbaData->buffer = g_msc.storageDisk + lbaData->offset * LENGTH_OF_EACH_LBA;
            else
            {
            	lbaData->buffer = g_msc.storageDisk + 21 * LENGTH_OF_EACH_LBA;
            	if(lbaData->offset>highestval)
            		highestval = lbaData->offset;
            }
            break;
        case kUSB_DeviceMscEventReadRequest:
            lbaData = (usb_device_lba_app_struct_t *)param;
            /*offset is the read start address get from read command, refer to class driver*/
            if(lbaData->offset<20)
            	lbaData->buffer = g_msc.storageDisk + lbaData->offset * LENGTH_OF_EACH_LBA;
            else
            	lbaData->buffer = g_msc.storageDisk + 22 * LENGTH_OF_EACH_LBA;
				if(lbaData->offset>highestval)
					highestval = lbaData->offset;
            break;
        case kUSB_DeviceMscEventGetLbaInformation:
            lbaInformationStructure = (usb_device_lba_information_struct_t *)param;
            lbaInformationStructure->lengthOfEachLba = LENGTH_OF_EACH_LBA;
            lbaInformationStructure->totalLbaNumberSupports = TOTAL_LOGICAL_ADDRESS_BLOCKS_NORMAL;
            lbaInformationStructure->logicalUnitNumberSupported = LOGICAL_UNIT_SUPPORTED;
            lbaInformationStructure->bulkInBufferSize = DISK_LOGICAL_SIZE_NORMAL;
            lbaInformationStructure->bulkOutBufferSize = DISK_LOGICAL_SIZE_NORMAL;
            break;
        case kUSB_DeviceMscEventTestUnitReady:
            /*change the test unit ready command's sense data if need, be careful to modify*/
            ufi = (usb_device_ufi_app_struct_t *)param;
            break;
        case kUSB_DeviceMscEventInquiry:
            ufi = (usb_device_ufi_app_struct_t *)param;
            ufi->size = sizeof(usb_device_inquiry_data_fromat_struct_t);
            ufi->buffer = (uint8_t *)&g_InquiryInfo;
            break;
        case kUSB_DeviceMscEventModeSense:
            ufi = (usb_device_ufi_app_struct_t *)param;
            ufi->size = sizeof(usb_device_mode_parameters_header_struct_t);
            ufi->buffer = (uint8_t *)&g_ModeParametersHeader;
            break;
        case kUSB_DeviceMscEventModeSelect:
            break;
        case kUSB_DeviceMscEventModeSelectResponse:
            ufi = (usb_device_ufi_app_struct_t *)param;
            break;
        case kUSB_DeviceMscEventFormatComplete:
        	highestval += 1;
            break;
        case kUSB_DeviceMscEventRemovalRequest:
            break;
        default:
            break;
    }
    return error;
}
/*!
 * @brief device callback function.
 *
 * This function handle the usb standard event. more information, please refer to usb spec chapter 9.
 * @param handle          The USB device handle.
 * @param event           The USB device event type.
 * @param param           The parameter of the device specific request.
 * @return kStatus_USB_Success or error.
 */
usb_status_t USB_DeviceCallback(usb_device_handle handle, uint32_t event, void *param)
{
    usb_status_t error = kStatus_USB_Error;
    uint16_t *temp16 = (uint16_t *)param;
    uint8_t *temp8 = (uint8_t *)param;
    switch (event)
    {
        case kUSB_DeviceEventBusReset:
        {
            g_msc.attach = 0;
            g_msc.currentConfiguration = 0U;
            error = kStatus_USB_Success;
#if (defined(USB_DEVICE_CONFIG_EHCI) && (USB_DEVICE_CONFIG_EHCI > 0U)) || \
    (defined(USB_DEVICE_CONFIG_LPCIP3511HS) && (USB_DEVICE_CONFIG_LPCIP3511HS > 0U))
            /* Get USB speed to configure the device, including max packet size and interval of the endpoints. */
            if (kStatus_USB_Success == USB_DeviceClassGetSpeed(CONTROLLER_ID, &g_msc.speed))
            {
                USB_DeviceSetSpeed(handle, g_msc.speed);
            }
#endif
        }
        break;
        case kUSB_DeviceEventSetConfiguration:
            if (0U ==(*temp8))
            {
                g_msc.attach = 0;
                g_msc.currentConfiguration = 0U;
            }
            else if (USB_MSC_CONFIGURE_INDEX == (*temp8))
            {
                g_msc.attach = 1;
                g_msc.currentConfiguration = *temp8;
            }
            else
            {
                error = kStatus_USB_InvalidRequest;
            }
            break;
        case kUSB_DeviceEventSetInterface:
            if (g_msc.attach)
            {
                uint8_t interface = (uint8_t)((*temp16 & 0xFF00U) >> 0x08U);
                uint8_t alternateSetting = (uint8_t)(*temp16 & 0x00FFU);
                if (interface < USB_MSC_INTERFACE_COUNT)
                {
                    g_msc.currentInterfaceAlternateSetting[interface] = alternateSetting;
                }
            }
            break;
        case kUSB_DeviceEventGetConfiguration:
            if (param)
            {
                *temp8 = g_msc.currentConfiguration;
                error = kStatus_USB_Success;
            }
            break;
        case kUSB_DeviceEventGetInterface:
            if (param)
            {
                uint8_t interface = (uint8_t)((*temp16 & 0xFF00U) >> 0x08U);
                if (interface < USB_INTERFACE_COUNT)
                {
                    *temp16 = (*temp16 & 0xFF00U) | g_msc.currentInterfaceAlternateSetting[interface];
                    error = kStatus_USB_Success;
                }
                else
                {
                    error = kStatus_USB_InvalidRequest;
                }
            }
            break;
        case kUSB_DeviceEventGetDeviceDescriptor:
            if (param)
            {
                error = USB_DeviceGetDeviceDescriptor(handle, (usb_device_get_device_descriptor_struct_t *)param);
            }
            break;
        case kUSB_DeviceEventGetConfigurationDescriptor:
            if (param)
            {
                error = USB_DeviceGetConfigurationDescriptor(handle,
                                                             (usb_device_get_configuration_descriptor_struct_t *)param);
            }
            break;
#if (defined(USB_DEVICE_CONFIG_CV_TEST) && (USB_DEVICE_CONFIG_CV_TEST > 0U))
        case kUSB_DeviceEventGetDeviceQualifierDescriptor:
            if (param)
            {
                /* Get Qualifier descriptor request */
                error = USB_DeviceGetDeviceQualifierDescriptor(
                    handle, (usb_device_get_device_qualifier_descriptor_struct_t *)param);
            }
            break;
#endif
        case kUSB_DeviceEventGetStringDescriptor:
            if (param)
            {
                error = USB_DeviceGetStringDescriptor(handle, (usb_device_get_string_descriptor_struct_t *)param);
            }
            break;
        default:
            break;
    }
    return error;
}
/* USB device class information */
usb_device_class_config_struct_t msc_config[1] = {{
    USB_DeviceMscCallback, 0, &g_UsbDeviceMscConfig,
}};
/* USB device class configuration information */
usb_device_class_config_list_struct_t msc_config_list = {
    msc_config, USB_DeviceCallback, 1,
};

/*!
 * @brief device application init function.
 *
 * This function init the usb stack and sdhc driver.
 *
 * @return None.
 */
int32_t USB_DeviceApplicationInit()
{

    USB_DeviceClockInit();
#if (defined(FSL_FEATURE_SOC_SYSMPU_COUNT) && (FSL_FEATURE_SOC_SYSMPU_COUNT > 0U))
    SYSMPU_Enable(SYSMPU, 0);
#endif /* FSL_FEATURE_SOC_SYSMPU_COUNT */

    g_msc.speed = USB_SPEED_FULL;
    g_msc.attach = 0;
    g_msc.mscHandle = (class_handle_t)NULL;
    g_msc.deviceHandle = NULL;
    g_msc.storageDisk = &s_StorageDisk[0];

    if (kStatus_USB_Success != USB_DeviceClassInit(CONTROLLER_ID, &msc_config_list, &g_msc.deviceHandle))
    {
        usb_echo("USB device init failed\r\n");
    }
    else
    {
        usb_echo("USB device mass storage demo\r\n");
        g_msc.mscHandle = msc_config_list.config->classHandle;
    }

    USB_DeviceIsrEnable();
    USB_DeviceRun(g_msc.deviceHandle);
}

uint32_t blinkrate = 10;
uint32_t blinkcount = 0;
static bool blinktoggle = true;

int32_t USB_Finalize_MSC_Transfer()
{

	if(TransferStarted && !MSD_Done)
	{
		MSD_LastProgTime++;
		if(MSD_LastProgTime>MSD_TIMEOUT)
		{
			MSD_Done = true;
			if(!BadBinaryFile)
			{
				// Finalize the new application by storing app specific info into FICA
				if(FICA_app_program_ext_finalize(NULL)!=SLN_FLASH_NO_ERROR)
				{
					RGB_LED_Blink(LED_BRIGHT_LOW, LED_COLOR_RED, blinkrate, &blinkcount, &blinktoggle);
				}
				// Make the new app active by setting the reset vector
				FICA_app_program_ext_set_reset_vector();
				vTaskDelay(1000);
			}
			ReRunBootloader();
		}
		RGB_LED_Blink(LED_BRIGHT_LOW, LED_COLOR_BLUE, blinkrate, &blinkcount, &blinktoggle);
	}
	else
	{
		MSD_LastProgTime=0;
		RGB_LED_Blink(LED_BRIGHT_LOW, LED_COLOR_PURPLE, blinkrate/2, &blinkcount, &blinktoggle);
	}
	return(SLN_FLASH_NO_ERROR);
}


void USB_DeviceTask(void *handle)
{
    while (1)
    {
//        USB_DeviceCdcVcomTask();
    	USB_Finalize_MSC_Transfer();

#if USB_DEVICE_CONFIG_USE_TASK
        USB_DeviceTaskFn(g_msc.deviceHandle);
#endif
    }
}


void USB_MSC_TASK(void *handle)
{
    USB_DeviceApplicationInit();

#if USB_DEVICE_CONFIG_USE_TASK
    if (g_msc.deviceHandle)
    {
        if (xTaskCreate(USB_DeviceTask,                  /* pointer to the task */
                        (char const *)"usb device task", /* task name for kernel awareness debugging */
                        5000L / sizeof(portSTACK_TYPE),  /* task stack size */
                        g_msc.deviceHandle,              /* optional task startup argument */
                        5,                               /* initial priority */
                        &g_msc.device_task_handle        /* optional task handle to create */
                        ) != pdPASS)
        {
            usb_echo("usb msc task create failed!\r\n");
            return;
        }
    }
#endif

    while (1)
    {
        vTaskDelay(100);
    }
}


int USB_MSC_Init(void)
{
    if (xTaskCreate(USB_MSC_TASK,                       /* pointer to the task */
                    "USB MSC TASK",                     /* task name for kernel awareness debugging */
                    5000L / sizeof(portSTACK_TYPE), /* task stack size */
                    &g_msc,                         /* optional task startup argument */
                    4,                              /* initial priority */
                    &g_msc.application_task_handle  /* optional task handle to create */
                    ) != pdPASS)
    {
        usb_echo("usb msc task create failed!\r\n");
        return 1;
    }

    vTaskStartScheduler();

    while(1)
    {
        vTaskDelay(100);
    };

    return 1;
}
