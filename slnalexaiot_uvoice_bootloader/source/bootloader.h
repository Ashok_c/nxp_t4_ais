/*
 * Copyright 2018 NXP.
 * This software is owned or controlled by NXP and may only be used strictly in accordance with the
 * license terms that accompany it. By expressly accepting such terms or by downloading, installing,
 * activating and/or otherwise using the software, you are agreeing that you have read, and that you
 * agree to comply with and are bound by, such license terms. If you do not agree to be bound by the
 * applicable license terms, then you may not retain, install, activate or otherwise use the software.d
 */

/**
 * @file bootloader.h
 * @brief Bootloader Main
 */

#ifndef _BOOTLOADER_H_
#define _BOOTLOADER_H_

/* Type definitions for Bootloader Main */
#include "aws_ota_types.h"

/* Includes required by the FreeRTOS timers structure. */
#include "FreeRTOS.h"
#include "timers.h"

extern uint8_t *pAppPageData;
extern uint8_t *AppVerifyPEM;
extern uint8_t *AppAWSKey;

/*---------------------------------------------------------------------------*/
/*								Public API									 */
/*---------------------------------------------------------------------------*/

/**
 * @brief Trap device execution if fatal issue is detected.
 *
 * Traps execution when there is no valid Reset Vector detected in flash.
 * 
 */
extern void the_should_never_get_here_catch(void);

/**
 * @brief Rapidly blinks LEDs to indicate the Bootloader is running
 */
extern void RapidBlinkLED(void);

/**
 * @brief Jump Execution to Address function.
 *
 * Causes bootloader execution to jump to the passed address (Interrupt Vector Table Address).  The
 * function calculates where the reset vector is from the passed IVT address and jumps there.
 *
 * @param[in] appaddr IVT address that holds the Reset Vector to jump to
 */
extern void JumpToAddr(uint32_t appaddr);

/**
 * @brief Jump to Main Application task
 *
 * RTOS task used by the Bootloader to jump to the current main application. The current application IVT
 * address is read from the flash.
 *
 * @param[in] appaddr IVT address that holds the Reset Vector to jump to.
 */
extern void jumpToMainAppTask(void *arg);

//
/**
 * @brief Check if Main App Cleared the OTA bit function.
 *
 * Check if the OTA bit is cleared in the flash, main app will clear the bit if OTA was requested from AVS.
 *
 * @return True if OTA was requested (bit is cleared), False if not
 */
extern bool is_OTA_FlashBitCleared();

// Set OTA bit in the flash
/**
 * @brief Set OTA Request Flag function.
 *
 * Set OTA Request Flag bit in flash. This indicates the OTA is no longer needed.
 *
 * @return Returns SLN_FLASH_NO_ERROR or SLN_FLASH_ERROR accordingly
 */
extern int32_t Set_OTA_FlashBit();

// Clear OTA bit in the flash, not used in bootloader, here for reference
/**
 * @brief Clear OTA Request Flag function.
 *
 * Clear OTA Request Flag bit in flash. When this is cleared, the bootloader will know an AVS OTA
 * application download is needed.
 *
 * @return Returns SLN_FLASH_NO_ERROR or SLN_FLASH_ERROR accordingly
 */
extern int32_t Clear_OTA_FlashBit();

/**
 * @brief Test Program Hyper Flash Application function.
 */
extern int32_t TestHyperFlashApplication();

/**
 * @brief ReRun Bootloader function.
 *
 * Restarts the Bootloader, soft reset.
 */
extern void ReRunBootloader();

/**
 * @brief Bootloader Main function.
 *
 * Execute the primary Bootloader main functions. Cycles LEDs to alert user it is running,
 * detects if the user is pressing a button for MSD, to start Factory or Main Application OTA, or
 * do a factory reset.
 */
extern void BootloaderMain();


/* _BOOTLOADER_H_ */
#endif
