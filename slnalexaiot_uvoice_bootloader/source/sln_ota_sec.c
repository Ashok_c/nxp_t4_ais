/*
 * Copyright 2019 NXP
 * All rights reserved.
 *
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "pin_mux.h"
#include "clock_config.h"

#include "mbedtls/base64.h"
#include "mbedtls/md.h"
#include "mbedtls/md_internal.h"
#include "mbedtls/x509_crt.h"

#include "sln_flash.h"
#include "sln_ota_sec.h"

static mbedtls_x509_crt s_caChain;
static mbedtls_x509_crt s_verifCert;

int32_t SLN_OTA_SEC_Verify_Cert(uint8_t *caPem, uint8_t *vfPem)
{
    int32_t ret = SLN_OTA_OK;   // SLN_OTA_SEC return code
    int32_t status = 0;         // mbedTLS status code
    uint32_t flags = 0;         // mbedTLS verification flags

    mbedtls_x509_crt_init(&s_caChain);
    mbedtls_x509_crt_init(&s_verifCert);

    if ((NULL == caPem) || (NULL == vfPem))
    {
        ret = SLN_OTA_NULL_PTR;
    }

    if (SLN_OTA_OK == ret)
    {
        // Parse CA Cert
        status = mbedtls_x509_crt_parse(&s_caChain, caPem, safe_strlen((char *)caPem, MAX_CERT_LEN) + 1);

        if (status)
        {
            configPRINTF(("ERROR: Could not parse CA certificate, -0x%X.\r\n", -status));
            ret = SLN_OTA_BAD_CERT;
        }
    }

    if (SLN_OTA_OK == ret)
    {
        // Parse Verification/Signing Cert
        status = mbedtls_x509_crt_parse(&s_verifCert, vfPem, safe_strlen((char *)vfPem, MAX_CERT_LEN) + 1);

        if (status)
        {
            configPRINTF(("ERROR: Could not parse verification certificate, -0x%X.\r\n", -status));
            ret = SLN_OTA_BAD_CERT2;
        }
    }

    if (SLN_OTA_OK == ret)
    {
        // Verify Verification/Signing Certificate
        status = mbedtls_x509_crt_verify(&s_verifCert, &s_caChain, NULL, NULL, &flags, NULL, NULL);

        if (status)
        {
            configPRINTF(("ERROR: Could not verify verification certificate, -0x%X.\r\n", -status));
            configPRINTF(("Flags: 0x%X\r\n", flags));
            ret = SLN_OTA_INVALID_CERT;
        }
    }

    mbedtls_x509_crt_free(&s_caChain);
    mbedtls_x509_crt_free(&s_verifCert);

    return ret;
}

int32_t SLN_OTA_SEC_Verify_Signature(uint8_t *vfPem, uint8_t *msg, size_t msglen, uint8_t *msgsig)
{
    int32_t ret = SLN_OTA_OK;   // SLN_OTA_SEC return code
    int32_t status = 0;         // mbedTLS status code

    mbedtls_x509_crt_init(&s_verifCert);

    if ((NULL == vfPem) || (NULL == msg) || (NULL == msgsig))
    {
        ret = SLN_OTA_NULL_PTR;
    }

    if (SLN_OTA_OK == ret)
    {
        // Parse Verification Cert
        status = mbedtls_x509_crt_parse(&s_verifCert, vfPem, safe_strlen((char *)vfPem, MAX_CERT_LEN) + 1);

        if (status)
        {
            configPRINTF(("ERROR: Could not parse verification certificate, -0x%X.\r\n", -status));
            ret = SLN_OTA_BAD_CERT;
        }
    }

    if (SLN_OTA_OK == ret)
    {
        // SHA256 hash of data
        mbedtls_md_context_t mdCtx;
        mbedtls_md_type_t mdType = MBEDTLS_MD_SHA256;
        mbedtls_md_info_t *mdInfo = mbedtls_md_info_from_type(mdType);
        char *hash = NULL;

        mbedtls_md_init(&mdCtx);
        mbedtls_md_setup(&mdCtx, mdInfo, 1);

        // Allocate hash memory
        hash = (char *)pvPortMalloc(mdCtx.md_info->size);

        if (NULL == hash)
        {
            configPRINTF(("ERROR: Could not allocate memory for hash.\r\n"));
            ret = SLN_OTA_NO_MEM;
        }

        if (SLN_OTA_OK == ret)
        {
            // Hash message
            mbedtls_md(mdCtx.md_info, msg, msglen, (uint8_t *)hash);

            // Now verify the signature for the given hash of the data
            status = mbedtls_pk_verify(&(s_verifCert.pk), mdCtx.md_info->type, (uint8_t *)hash, mdCtx.md_info->size, msgsig, RSA_SIG_LEN);

            if (status)
            {
                configPRINTF(("ERROR: Could not authenticate message, -0x%X.\r\n", -status));
                ret = SLN_OTA_INVALID_SIG;
            }
        }

        if (NULL != hash)
        {
            for (uint32_t idx = 0; idx < mdCtx.md_info->size; idx++)
            {
                hash[idx] = 0x00; // Wipe hash data
            }
        }
        vPortFree(hash);
        hash = NULL;

        mbedtls_md_free(&mdCtx);
    }

    mbedtls_x509_crt_free(&s_verifCert);

    return ret;
}
