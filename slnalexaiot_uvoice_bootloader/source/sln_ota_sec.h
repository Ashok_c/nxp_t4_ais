/*
 * Copyright 2019 NXP
 * All rights reserved.
 *
 *
 * SPDX-License-Identifier: BSD-3-Clause
 */

#ifndef _SLN_OTA_SEC_H_
#define _SLN_OTA_SEC_H_ 1

#define ROOT_CA_CERT		"ca_root.dat"
#define APP_A_SIGNING_CERT  "app_a_sign_cert.dat"
#define APP_B_SIGNING_CERT  "app_b_sign_cert.dat"
#define CRED_SIGNING_CERT   "cred_sign_cert.dat"

#define MAX_CERT_LEN    2048
#define RSA_SIG_LEN     256

typedef enum _sln_ota_status
{
    SLN_OTA_OK           =  0,
    SLN_OTA_NULL_PTR     = -1,
    SLN_OTA_BAD_CERT     = -2,
    SLN_OTA_BAD_CERT2    = -3,
    SLN_OTA_INVALID_CERT = -4,
    SLN_OTA_INVALID_SIG  = -5,
    SLN_OTA_NO_MEM       = -6,
}sln_ota_status_t;

int32_t SLN_OTA_SEC_Verify_Cert(uint8_t *caPem, uint8_t *vfPem);

int32_t SLN_OTA_SEC_Verify_Signature(uint8_t *vfPem, uint8_t *msg, size_t msglen, uint8_t *msgsig);

#endif /* _SLN_OTA_SEC_H_ */
