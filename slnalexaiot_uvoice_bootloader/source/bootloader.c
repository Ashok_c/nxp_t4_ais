/*
 * Copyright 2019 NXP.
 * This software is owned or controlled by NXP and may only be used strictly in accordance with the
 * license terms that accompany it. By expressly accepting such terms or by downloading, installing,
 * activating and/or otherwise using the software, you are agreeing that you have read, and that you
 * agree to comply with and are bound by, such license terms. If you do not agree to be bound by the
 * applicable license terms, then you may not retain, install, activate or otherwise use the software.
 */

/* Board includes */
#include "board.h"
#include "pin_mux.h"

/* FreeRTOS kernel includes */
#include "FreeRTOS.h"
#include "task.h"

#include "usb_device_config.h"
#include "usb.h"
#include "usb_device.h"

#include "usb_device_class.h"
#include "usb_device_msc.h"
#include "usb_device_ch9.h"
#include "usb_device_descriptor.h"
#include "disk.h"

#include "network_connection.h"
#include "sln_flash_mgmt.h"
#include "wifi_credentials.h"

#include "bootloader.h"
#include "composite.h"
#include "aws_logging_task.h"
#include "sln_ota.h"

#include  "sln_RT10xx_RGB_LED_driver.h"

/*******************************************************************************
* Definitions
******************************************************************************/

#define PRESSED 0

#define SET_THUMB_ADDRESS(x)  (x | 0x1)

#define LOGGING_STACK_SIZE 256
#define LOGGING_QUEUE_LENGTH 64


/*******************************************************************************
* Variables
******************************************************************************/

// Variables used by AWS OTA for to pass Application data to FICA
__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE"))) uint8_t AppPageData[EXT_FLASH_PROGRAM_PAGE*2]={0};
__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE"))) uint8_t *AppVerifyPEM = NULL;
__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE"))) uint8_t *AppAWSKey = NULL;

// Set Temporary Stack Top to end of first block of OC RAM
extern void __base_SRAM_OC_NON_CACHEABLE(void);
#define TEMP_STACK_TOP (__base_SRAM_OC_NON_CACHEABLE + 0x8000)

extern void appInit(void *arg);

// typedef for function used to do the jump to the application
typedef void (*app_entry_t)(void);

// variable used as a function to do the jump to the application
app_entry_t appEntry = 0;

__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE"))) uint8_t ProgExtAppImgBuf[EXT_FLASH_ERASE_PAGE];
__attribute__((section(".data.$SRAM_OC_NON_CACHEABLE")))
#ifdef DEBUG_IMG
	uint8_t tdata[EXT_FLASH_ERASE_PAGE + 1]={0};
#else
	uint8_t tdata[EXT_FLASH_PROGRAM_PAGE + 1]={0};
#endif

/*******************************************************************************
* Code
******************************************************************************/

//! @brief Hard Fault Handler breakpoint to control execution if tripped
__attribute__((naked)) void HardFault_Handler(void)
{
    __asm("BKPT #0");
}

//! @brief Software Trap for when no valid application exists
void the_should_never_get_here_catch(void)
{
    volatile uint32_t delay = 1000000;

    while(1)
    {
        while(delay--);
    }

}


//! @brief Run the passed Application
void JumpToAddr(uint32_t appaddr)
{
	appaddr += FlexSPI_AMBA_BASE;

	RGB_LED_SetBrightnessColor(LED_BRIGHT_MEDIUM, LED_COLOR_PURPLE);

	// Point entry point address to entry point call function
	appEntry = (app_entry_t)(SET_THUMB_ADDRESS((*(uint32_t *)(appaddr + 4))));

	// Set the VTOR to the application vector table address.
	SCB->VTOR = (uint32_t)appaddr;

	// Set stack pointers to the application stack pointer.
	__set_MSP((uint32_t)TEMP_STACK_TOP);
	__set_PSP((uint32_t)TEMP_STACK_TOP);

	// Jump to main app entry point
	appEntry();
}

//! @brief run the current Application
void jumpToMainAppTask(void *arg)
{

#ifdef DEBUG_BOOTLOADER
	the_should_never_get_here_catch();
#endif

	uint32_t imgtype = FICA_IMG_TYPE_APP_FAC;
	uint32_t appaddr = FICA_IMG_APP_FAC_ADDR;

    // Get Current Application Vector
    FICA_GetCurAppStartType(&imgtype);

	FICA_get_app_img_start_addr(imgtype, &appaddr);

	/* main app does not start correctly without this. why? */
	DbgConsole_Deinit();

	NVIC_DisableIRQ(LPUART6_IRQn);

    JumpToAddr(appaddr);

    // Uh oh...
    the_should_never_get_here_catch();

    vTaskDelete(NULL);
}


//! @brief Test Programming Application to Hyper Flash
int32_t TestHyperFlashApplication(uint32_t desttype)
{
	uint32_t destaddr = 0;
	uint8_t runval = 0;
	uint8_t *prunaddr = 0;

	// Init FICA to be ready for the new application
	if(FICA_app_program_ext_init(desttype)!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	prunaddr = &AppPageData;
	for(uint32_t i=0; i<EXT_FLASH_PROGRAM_PAGE*2;i++)
		*prunaddr++=runval++;

	// For this test we will just move the current application to the new application location
	// Uses worst case Max App Size, actual OTA will only write what comes in from WiFi
	for(uint32_t runaddr = 0; runaddr < EXT_FLASH_PROGRAM_PAGE*10; runaddr+=EXT_FLASH_PROGRAM_PAGE*2)
	{
		// Write the data to Flash a PAGE size or less at a time, contiguously
		if(FICA_app_program_ext_abs(runaddr, (void *)&AppPageData, EXT_FLASH_PROGRAM_PAGE*2)!=SLN_FLASH_NO_ERROR)
	    		return(SLN_FLASH_ERROR);
	}

	prunaddr = &AppPageData;
	for(uint32_t i=0; i<EXT_FLASH_PROGRAM_PAGE*2;i++)
		*prunaddr++=0x55;

	// Write the data to Flash w less than a PAGE at offset 0x00001000
	if(FICA_app_program_ext_abs(0x00002000, (void *)&AppPageData, 35)!=SLN_FLASH_NO_ERROR)
    		return(SLN_FLASH_ERROR);

	// Verifies passed Signature with passed key
	if(FICA_app_program_ext_finalize(AppAWSKey)!=SLN_FLASH_NO_ERROR)
	    		return(SLN_FLASH_ERROR);

	// Program the reset vector for next reset
	if(FICA_app_program_ext_set_reset_vector()!=SLN_FLASH_NO_ERROR)
		return(SLN_FLASH_ERROR);

	return(SLN_FLASH_NO_ERROR);
}

//! @brief Rerun the bootloader, soft reset
void ReRunBootloader()
{
#ifdef DEBUG_BOOTLOADER
	the_should_never_get_here_catch();
#endif

	uint32_t appaddr = FICA_IMG_BL_FAC_ADDR;

    // Get Current Application Vector
	FICA_GetCurBootStartAddr(&appaddr);

	// Run the Bootloader
    JumpToAddr(appaddr);
}

//! @brief Check if a Factory OTA was requested
void CheckForFactoryOTA()
{
	// Not implemented
}

//! @brief Check and process if Factory Reset button was pressed
void CheckForFactoryReset()
{
	uint32_t i = 0;

#ifdef BOOT_TEST_FACTORY_RESET
	FICA_SetCurAppStartType(FICA_IMG_TYPE_APP_FAC);
	ReRunBootloader();
#else

	// Check if Factory button pushed for > 20 secs, set factory app vector
	if(GPIO_PinRead(SW1_GPIO, SW1_GPIO_PIN)==PRESSED)
	{
		for(i = 0; i<20; i++)
		{
			vTaskDelay(1000); // delay 1s
			// Check if button is still pushed, if not, jump out
			if(GPIO_PinRead(SW1_GPIO, SW1_GPIO_PIN)!=PRESSED)
				break;
		}
		// if button was pushed for 20s, set Factory App as the default, re-run bootloader
		if(i==20)
		{
			// Set Factory App as default
			FICA_SetCurAppStartType(FICA_IMG_TYPE_APP_FAC);
			ReRunBootloader();
		}
	}
#endif
}

//! @brief Check and process if MSD Mode button is pushed
void CheckForMSDMode()
{
#ifdef BOOT_TEST_MSD
	USB_MSC_Init();
#else
	// Check if USB MSD Mode button is pushed
	if(GPIO_PinRead(SW2_GPIO, SW2_GPIO_PIN)==PRESSED)
		USB_MSC_Init();
#endif
}


void Run_OTA_Task()
{
	xLoggingTaskInitialize(LOGGING_STACK_SIZE, tskIDLE_PRIORITY + 1, LOGGING_QUEUE_LENGTH);

	// Run AWS OTA Task WiFi AVS Application OTA Function
	xTaskCreate(otaAppInitTask, "AWS_OTA_Task", 2048, NULL, configMAX_PRIORITIES - 1, NULL);

	/* Run RTOS */
	vTaskStartScheduler();

    /* Must not return from this function, otherwise BootloaderMain will jump to mainTask */
	while (1)
	{

	}
}


//! @brief Check and process if Application OTA is pending
void CheckForAppOTA()
{
#ifdef BOOT_TEST_APP_OTA
	xTaskCreate(AWS_OTA_Main, "AWS_OTA_Task", 1024, NULL, configMAX_PRIORITIES - 1, NULL);
#else
	// Check if App Detected WiFi OTA Image is Available
	if(FICA_is_OTA_FlashBitCleared())
	{
		Run_OTA_Task();
	}
#endif
}

//! @brief Bootloader Main control function
void BootloaderMain()
{
	RapidBlinkLED();

	CheckForFactoryOTA();
	CheckForFactoryReset();
	CheckForMSDMode();
	CheckForAppOTA();

	jumpToMainAppTask(NULL);
}

